<!--
BIG FAT WARNING ================================================================

  Hej! This file is public! beware with what you write here ^^
  The rest of this directory is not though, so feel free to add whatever you
  see fit!

================================================================================
-->

This directory contains data private to the development group such as passwords,
Secure access tokens and what not.
The contents of this directory are encrypted to each contributor's
[PGP key][PGP] using [git-crypt][git-crypt].

This is the only file in this directory unencrypted.

[PGP]: https://en.wikipedia.org/wiki/Pretty_Good_Privacy
[git-crypt]: https://github.com/AGWA/git-crypt/
