###  Ripoff of the HHCC Xiaomi Flower Care plant database (https://github.com/vrachieru/plant-database)

4539 plants, each with in its json file

### Infos

Quantized : Name, alias (simple name), max and min : light, temperature, humidity, soil's moist.
Not quantized : instructions on care (size, type of soile, sunlight, watering, fertilization, pruning), origin, category, blooming period, color, picture of plant

Usage example on https://github.com/vrachieru/huahuacaocao-api

Data downloadable 

Data can be retrieved easily

Given that it's Xiaomi's database, one can assume it's validation.

Infos per plant variety

No mention of a license whatsoever.



### OpenPlantbook (https://open.plantbook.io/)

#### Infos :

Name, alias (simple name), max and min : light, temperature, humidity, soil's moist, picture

Basically the same parameters as Xiaomi's but less.

#### Features

Possibility to create a new plant or modify an existing one

Free service

Needs a connection for a query. 



### MiFloraDB (https://github.com/khronimo/MiFloraDB )

Also a ripoff of the Xiaomi database but more plants (5335) in .csv format

Data downloadable 

Data can be retrieved easily

License : GLP-3.0



### Garden (https://garden.org/plants/)

#### Infos

Plant habit, life cycle, sun requirements, minimum temperature, flower time, container.
Not much quantized but nice pictures 



