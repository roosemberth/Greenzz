# Project de semestre à l'HEIG-VD

Ce dossier contient les documents de travails utilisés par le groupe le long du
semestre.

```console
├── 00-selection_de_projet : Propositions initiales de projet.
├── 10-design_brief        : Cahier de charges.
│   ├── 10-Cahier-de-charges.md   : Version texte du cahier de charges.
│   ├── Makefile           : Script de construction du cahier de charges.
│   └── ...
├── datasources.md         : Détails sur la base de données de plantes.
├── media                  : Fichiers media utilisés sur les READMEs.
├── pvs                    : Procès verbales des réunions du groupe.
├── README.md
├── research
│   ├── application_mockup : Ébauches de l'aspet graphique de l'app Android.
│   └── datasource.md      : Sources de données de plantes trouvées.
├── slides                 : Slides utilisées lors des présentations.
└── tools                  : Outils utilisées pour la mise en page de texte.
```

## Vidéo présentation des fonctionnalités utilisateur du projet

![Vidéo présentation des fonctionnalités du projet](./media/Présentation-des-fonctionnalités-utilisateur.mp4)

## Génération des rapports

Les rapports sont automatiquement générés par l'intégration continue GitLab.
Vous pouvez accéder à la dernière version du Cahier de charges en cliquant sur
[ce lien][ci-design-brief].
Vous pouvez accéder au rapport final en cliquant sur [ce lien][ci-final-report].

[ci-design-brief]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/raw/artifacts/reports/10-Cahier-des-charges.pdf?job=Render%20reports
[ci-final-report]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/raw/artifacts/reports/Rapport-final.pdf?job=Render%20reports

Vous pouvez également consulter tous les rapports en cliquant [ici][ci-reports].

[ci-reports]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/browse/artifacts/reports?job=Render%20reports
