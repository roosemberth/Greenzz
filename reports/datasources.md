### Database selection

The database we will use is MiFloraDB (https://github.com/khronimo/MiFloraDB). It is the same one the Xiaomi application used, which means it will give us all the information we need. It has a GLP-3.0 license.

The data is stored in a .csv format, so we will need to parse the file.

The data is the following : 

- Quantized : Name, alias (simple name), max and min : light, temperature, humidity, soil's moist.
- Not quantized : instructions on care (size, type of soile, sunlight, watering, fertilization, pruning), origin, category, blooming period, color, picture of plant