---
title: "Propositions de projet de semestre Groupe B03"
author: Roosembert PALACIOS
date: Mon 01 Mar 2021
geometry: margin=1.8cm
output: pdf_document
---
Composition de l'équipe:

- DERDER Hakim
- GAZZETTA Florian (remplacant chef de projet)
- HOANG ANH Mai
- PALACIOS Roosembert (chef de projet)
- PENALVA Carl

# Propositions

## Assistant pour la culture de plantes

Aimez-vous les plantes mais vous êtes trop maladroit pour en prendre soin ? Ou bien vous êtes quelqu'un de très occupé et vous oubliez souvent d'être attentif à ses besoins ? Nous proposons une solution moderne connecté sur votre smartphone et / ou ordinateur qui vous permet de non seulement de monitorer l'environment et la croissance de vos plantes, mais aussi vous informer de ses besoins et vous advertir lorsque son bien-être est à risque. Nous aimerions proposer une platforme vous permettant de suivre l'état de vos plantes à l'aide des capteurs d'humidité, lumière, pH et temperature et vous donner des statistiques sur les conditions de vos plantes.

Comme fonctionnalité supplémentaires, si le temps du projet le permet, on aimerait fournir aussi des conseils pour mieux entretenir les plantes en fonction de l'espèce de plante : Par exemple, indiquer si la plante reçoit trop ou peu d'eau à chaque arrossage, ou que votre plante de tomate est prête à la récolte.


## Outil de plannification de voyages pittoresques

Le voyage est une solution facile pour la prévention des maladies psychiques telles que la dépression. Néanmoins, dans notre vie de tous les jours, nous avons des horaires établies et des responsabilités qui nous empêchent de prendre le temps pour les trajets. On aimerait concevoir une application qui nous permette de travailler lors des trajet pour ensuite pouvoir profiter de la nature, la montagne et la culture, une fois arrivés à destination. Pour ce faire, nous prévoyons une application qui nous permet de choisir une destination et un temps de parcours et vous proposer des itinéraires en fonction de l'occupation des train, le nombre minimum de transbordements afin de vous permettre de travailler dans le confort du train et profiter d'un moment de détente. Il y aurait aussi la possibilité de choisir de passer par des endroits ou le paysage est le plus joli.

Exemple : Je voudrais aller de Lausanne a Basel en passant par Andermatt, avec une halte de deux heures a Göschenen et une halte de deux heures a Winterthur, en évitant les trains bondés.

## Outil pour le coaching en sport et activités physiques

Les restrictions sanitaires liées au Covid-19 ont rendu très difficile et/ou peu conseillé de pratiquer du sport ou de l'activité physique dans des structures tels que les clubs sportifs, les gymnases ou les salles de sport. Néanmoins, beaucoup de personnes ont besoin d'accompagnement, du soutien et du conseil (dans d'autres mots du coaching) pour pouvoir avancer dans leur processus de conditionnement physique. On aimerait donc avoir une application permettant aux deux types d'acteurs, les coachs et leurs clients, d'échanger au sujet des activités physiques. Idéalement, l'application devrait permettre d'effectuer la plupart des interactions qui sont possibles dans des circonstances normales, exception faite des interactions physiques plus proches de la consultation médicale que du coaching.
