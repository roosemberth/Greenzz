---
toc: yes
date: 10 Juin 2021
title: Greenzz
lang: fr
subtitle: Rapport final - Groupe B-03
author:
- Roosembert Palacios
- Mai Anh Hoang
- Hakim Derder
- Florian Gazzetta
- Carl Penalva
---

<!-- markdownlint-disable MD025 -->

# Introduction

Au cours du semestre, nous avons développé Greenzz ; un système conçu pour
assister ses utilisateurs à suivre les besoins de leurs plantes et fournir des
conseils pour mieux les entretenir.

Ce système est composé de trois composants :

- Une application Android que les utilisateurs peuvent installer sur leurs
  appareils mobiles.
- Une carte à capteurs optionnelle qui peut être associé à une plante, afin de
  pouvoir fournir des recommandations en temps réel, suivre l'évolution de
  la plante et permet à l'application d'offrir des rappels à l'utilisateur.
- Un service en ligne avec diverses informations sur les plantes, consultable
  à travers l'application.
  Ce service est aussi responsable de coordonner l'application Android avec
  la carte à capteurs, si celle-ci est installée.

Lors de la proposition initiale du projet[^1], nous avions également envisagé
un aspect communautaire, mais à cause des contraintes de temps, cet objectif
n'a pas été achevé.
Néanmoins, la fonctionnalité d'informations sur la plante a été plus développé
que ce qui a été initialement prévu.

[^1]: https://gitlab.com/roosemberth/Greenzz/-/tree/master/reports#user-content-génération-des-rapports

# Package de test et installation

Un paquet d'installation de l'application Android, ainsi que le _firmware_
(micrologiciel) de la carte à capteurs, les composants logiciel du service
en ligne, ainsi que d'autres documents sur les projets sont générés
et publiés par notre système d'intégration continue.

L'application Android peut être téléchargée à partir de la section _User
application_[^2] dans le README du projet.

Le firmware (micrologiciel), ainsi que les instructions sur comment téléverser
le micrologiciel sur la carte à capteurs peuvent être téléchargés à partir de
la section _Sensor board_[^3] dans le README du projet.

Les composants logiciel du service en ligne peuvent être téléchargés à partir
de la section _Application server_[^4] dans le README du projet.

[^2]: https://gitlab.com/roosemberth/Greenzz#user-application
[^3]: https://gitlab.com/roosemberth/Greenzz#sensor-board
[^4]: https://gitlab.com/roosemberth/Greenzz#application-server

Pour installer un fichier _.apk_ sur un téléphone Android, il faut tout
d'abord le copier sur la mémoire du téléphone.
Ensuite, dans les paramètres de l'appareil, section _applications_, menu
_autorisation des applications_, il est nécessaire d'activer le téléchargement
d'applications depuis des sources inconnues.
Selon les paramètres de votre système d'exploitation, il se peut que ces options
soient dans le menu _Sécurité_.
Finalement, il faut trouver le fichier _.apk_ précédemment copié dans les
fichiers du téléphone et simplement appuyer dessus pour l'installer.
Une fois l'opération terminée, il est recommandé de désactiver le
téléchargement depuis des sources inconnues.

Des instructions détaillées installation, utilisation, exploitation, exécution
des tests ainsi que les liens vers la documentation du code sont disponibles
sur les READMEs dédiés à chaque composant.
Des liens à ceux-ci peuvent être trouvés sur les sections précédemment
référencées.
Pour votre commodité, vous trouverez ces documentations en annexe à la fin de
ce document.

## Information pour le support

Notre équipe est évidemment disponible et joignable sur nos adresses mails
respectives.
En cas de remarques techniques, problèmes ou informations manquantes sur la
documentation, nous vous prions de bien vouloir ouvrir un ticket sur notre
_gestionnaire des tickets_[^5].

[^5]: https://gitlab.com/roosemberth/Greenzz/-/issues

# Démonstration du logiciel

## Rappel des objectifs de l'itération finale

Les fonctionnalités suivantes ont été envisagés lors de la dernière
itération[^7]

[^7]: https://gitlab.com/roosemberth/Greenzz/-/boards/2577877

- Synchronisation des photos journalières des profils de plantes.
- L'application Android récupère les informations nécessaires pour afficher
  des recommandations à l'utilisateur.
- L'application Android affiche l'état et connectivité de la carte à capteurs.
- La carte à capteurs doit pouvoir envoyer la température au serveur.
- L'application Android doit être capable d'enregistrer une carte à capteurs.

## Démo scénario nominal itération finale

Une vidéo de présentation des fonctionnalités utilisateur de l'application
Android et de la carte à capteurs est disponible sur le README du dossier
_reports_[^6] dans notre dépôt de projet.

[^6]: https://gitlab.com/roosemberth/Greenzz/-/tree/master/reports

# Documentation du logiciel

## Documentation utilisateur

La documentation de l'application utilisateur peut être trouvé dans le dossier
`docs`[^8] situé dans le répertoire du sous projet de l'application utilisateur.

[^8]: https://gitlab.com/roosemberth/Greenzz/-/tree/master/src/client/docs

La documentation de la carte à capteur peut être trouvé dans le dossier
`docs`[^8] situé dans le répertoire du sous projet de la carte à capteur.

[^8]: https://gitlab.com/roosemberth/Greenzz/-/tree/master/src/sensor-board/docs

Vous trouverez une copie de ces documentations en annexe à la fin de ce document.

## Documentation conception technique

Le schéma suivant présente de manière visuel les interactions entre les
composants du système.

```plantuml
@startuml

package client {
  :Utilisateur: as usr
  (Application Téléphone) as app
}

package serveur {
  (Serveur applicatif) as serv
  (Database Influx) as influx
  (Database PostgreSQL) as sql
}

package embarque {
  :Capteurs sur la plante: as plante
  (Carte a capteur) as carte
}

usr -> app : Utilise
plante -l-> carte : Génère des données
app <-u-> serv : Envoie et reçoit des données
carte -u-> influx : Envoie les données des capteurs
serv -> influx : Gère les données
serv <-u-> sql : Envoie et reçoit des données
app -> influx : Lis données des capteurs
app -> carte : Configure

@enduml
```

Ce schéma illustre les différents composants du système ainsi que les
interactions entre ceux-ci.

Le serveur applicatif expose la plupart de fonctionnalités sur une interface
_websocket_ mais aussi sur _HTTP REST_ (sauf pour le téléchargement de photos,
disponible uniquement en REST).

La database influx expose également ses fonctionnalités

Chacun de ces composants est documenté en détail dans le README du dossier de
chacun de ces composants.
Pour votre commodité, vous trouverez ces documentations en annexe à la fin de
ce document.

### Schéma de séquence de l'exemple type d'utilisation de la carte à capteurs

```plantuml
@startuml
hide footbox

participant Serveur as serv
participant Application as app
participant Carte
participant "Database Influx" as db

== Ajout d'une carte à capteurs ==

note over app : L'utilisateur clique sur 'Add a sensor board'.

group Bluetooth
  app <-> Carte : Vérification de compatibilité du protocole
  app -> Carte : Demande des réseaux WiFi à portée de la carte
  Carte -> app : Liste des réseaux WiFi détectés
  app -> Carte : Envoi de données de connexion au réseau WiFi
  Carte -> app : Confirmation de la connectivité internet
end

app -> serv : Enregistrement de la carte\nRéception des identifiants\n InfluxDB pour la carte

group Bluetooth
  app -> Carte : Envoi des données de connexion InfluxDB
end

group WiFi
  Carte -> db : Vérification des identifiants de connexion
end

group Bluetooth
  Carte -> app : Confirmation de la connexion à InfluxDB
end

== Fonctionnement de la carte à capteurs ==

group WiFi [périodiquement]
  Carte -> db : Envoi des données des capteurs
end

== L'utilisateur accède aux données en temps réel ==

app -> serv : Récupération des identifiants\n  InfluxDB pour l'application

app -> db : Demande des flux des données des capteurs

group (de manière asynchrone) [périodiquement]
  db -> app : Données des capteurs
end

@enduml
```

Ce schéma décrit les interactions qui ont lieu lorsqu’un utilisateur associe
une carte à capteurs à une plante et lors qu'il consulte les informations en
temps réel.

Dans ce schéma, toutes les interactions avec le serveur se passent à travers
d'un _websocket_, alors que les interactions de la carte avec la base de données
_InfluxDB_ se font à travers _HTTP_.

Le protocole de communication entre la carte à capteurs et l'application
Android est basé sur du _JSON-RPC 2.0_[^9] ;
Cette communication a lieu à travers un lien _Bluetooth low energy (Bluetooth
LE)_ qui réutilise un le service _Nordic UART_ avec UUID
_6e400001-b5a3-f393-e0a9-e50e24dcca9e_.

[^9]: https://www.jsonrpc.org/specification

# Organisation du code

## Arborescence & Contenus

```console
├── README.md : Fichier présentation du projet
├── private   : Fichiers chiffrés à accès restreint aux membres du projet
├── reports   : Contenu lié à l'HEIG-VD, organisation du projet et service fourni
│   ├── README.md   : Présentation du projet dans le cadre de l'HEIG-VD
└── src
    ├── client            : Sous-projet application utilisateur
    │   ├── lib             : Sources de l'application utilisateur
    │   ├── pubspec.yaml    : Fichier de déclaration de dépendances Dart
    │   ├── test            : Tests de l'application utilisateur
    │   ├── README.md       : Présentation du projet application utilisateur
    │   └── ...
    ├── sensor-board      : Sous-projet carte à capteurs
    │   ├── COPYING         : License des sources du micrologiciel
    │   ├── docs            : Documentation utilisateur et technique
    │   ├── lib             : Sources du micrologiciel de la carte à capteurs
    │   ├── src             : Point d'entrée du micrologiciel
    │   ├── test            : Code harnais pour les tests du micrologiciel
    │   ├── tools           : Outils utilisés pour le débogage du micrologiciel
    │   ├── platformio.ini  : Fichier de configuration du projet embarqué
    │   └── README.md       : Présentation du projet du micrologiciel
    └── server            : Sous-projet serveur applicatif
        ├── pom.xml         : Fichier de configuration du projet Maven
        ├── src             : Sources du projet serveur applicatif
        └── README.md       : Présentation du projet serveur applicatif
```

# Suivi qualité

## Principe de supervision

Lorsqu'un contributeur veut apporter des modifications au code, il doit créer
une _merge request_ sur la forge logiciel du dépôt du projet.

Lorsqu'une _merge request_ est créé, un autre contributeur (le vérificateur ou
_reviewer_) vérifiera le code afin de s'assurer de propriétés suivantes :

- Les modifications apportés par la _merge request_ sont nécessaires et vont
  dans le même sens que l'idée de développement du groupe.
- Le code implémente ce que la _merge request_ déclare de faire.
- Les standards du code du projet sont respectés (e.g. Tout le code est en
  anglais, le code est testé, le code ne génère pas des alertes sur les
  analyseurs statiques).
- Le code ne casse pas de fonctionnalités non testables de manière automatique
  (comme par exemple, la communication Bluetooth LE).

Ainsi, le _reviewer_ pourra faire des remarques sur le code et le contributeur
aura la possibilité de répondre aux remarques ou faire des changements sur le
code.
Ce processus se repère jusqu'à ce que toutes les personnes concernées soient
en accord.
À ce moment, le _reviewer_ peut approuver la _merge request_ et accepter la
contribution en la fusionnant au dépôt (_merge_).

Il faut mentionner que le système refuse que la _merge request_ soit accepté
si:

- Toutes les discussions n'ont pas été résolues.
- Les tests ou autres étapes de vérification et génération de l'intégration
  continue ont des erreurs.
- La _merge request_ n'a pas été approuvé.

## Liste de contrôles effectués

L'intégration continue (CI) réalise les vérifications suivantes.

- Les rapports et autres documents du projet (e.g. Documentation auto généré sur
  le code) compilent sur la CI est ont été générés correctement.
- Les tests de chaque composant (serveur applicatif, client utilisateur,
  micrologiciel de la carte à capteurs) s'exécutent sans erreurs.
- Des livrables pour le serveur applicatif, client utilisateur et micrologiciel
  ont été générés sur la CI sans erreurs.

En plus des étapes précédemment décrites, lorsque la _merge request_ est accepté
et fusionné avec le code du projet, l'intégration continue s'exécute à nouveau
mais cette fois ci contre la version du code principal (branche _master_) afin
de fournir une preuve sur la validité de celui-ci.

Lorsque la CI s'exécute sur le code du projet principal (branche _master_), elle
publie aussi les objets suivants :

- Documents et autres rapports au format PDF.
- Fichier d'installation de l'application utilisateur.
- Documentation auto généré sur le code (e.g. javadoc, dartdoc).
- Fichier binaire prêt à utiliser pour le serveur applicatif.
- Micrologiciel prêt à utiliser pour la carte à capteurs.
- Instructions pour la mise à jour du micrologiciel.

# Conclusion

Notre groupe a appris énormément des choses en travaillant sur ce projet.
Le partage de connaissances a été une partie clé afin de pouvoir construire un
projet aussi grand.

Avec un peu de rétrospective, nous pensons que ce projet a été trop ambitieux
dès le début.
Si bien, il ne le semblait pas quand nous avons commencé à travailler, nous
avons passé énormément de temps sur le suivi qualité (e.g. Faire des
aller-retours sur le discussions des _merge requests_, écrire des tests,
débugger des problèmes détectés par l'intégration continue).
Nous sommes un peu tristes de ne pas avoir atteint tous les objectifs que nous
nous étions fixés, mais l'espoir de continuer le développement du projet est
dans nos horizons.

Notre logiciel utilise des services en ligne (et par conséquence tels services
ont été mis à disposition).
Étant donné que le déploient des services en ligne ne fait pas partie de ce
projet, ce code ne fait pas partie du dépôt.
Néanmoins, il est disponible en ligne sur le dépôt personnel de l'un de nos
membres du projet, à qui nous remercions d'avoir mis à disposition
l'infrastructure et ressources informatiques nécessaires pour l'exploitation
des services en ligne du projet :
<https://gitlab.com/roosemberth/dotfiles/-/blob/master/nix/machines/containers/greenzz-prod.nix>
Vous trouverez les politiques de confidentialité et vie privée de ce service en
annexe à la fin de ce document.

Notre projet est très aligné avec les méthodologies et processus de
développement des logiciels libres.
En effet, il était très important pour nous de développer des logiciels libres
afin que n'importe qui sur internet puisse reprendre notre projet, l'étudier, le
modifier et distribuer ses modifications à son gré.
Cet esprit nous a mené à prendre du temps pour tout documenter de manière
détaillée, même si les connaissances étaient déjà au sein du groupe, afin que
n'importe qui puisse reproduire nos résultats.

# Annexes
