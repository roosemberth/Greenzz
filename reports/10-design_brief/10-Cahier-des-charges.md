---
toc: yes
date: 3 Mar 2021
title: Greenzz
subtitle: Cahier de charges - Groupe B-03
author:
- Roosembert Palacios
- Mai Anh Hoang
- Hakim Derder
- Florian Gazzetta
- Carl Penalva
---
# Presentation du projet

## Contexte

Dans le monde actuel, l'horticulture est un passe-temps qui commence à
intéresser une grande quantité de gens.
Cependant, il peut être difficile de savoir comment prendre soin de ses plantes
vertes correctement et encore plus démotivant de les voir mourir suite à des
soins inadaptés ou des erreurs commises par manque d'expérience ou expertise.
Nous proposons une application qui accompagne ses utilisateur pour mieux vous
aider à combler les besoins des plantes.

## Objectifs

Nous envisageons de développer une application Android qui permet de suivre des
besoins d'une plante et assister l'utilisateur par moyen des rappels, des
notification et des conseils d'autres utilisateurs à mieux les entretenir.
Nous envisageons également de développer une carte à capteur optionnelle que les
utilisateurs pourront acheter séparément qui permettra à l’utilisateur de faire
un suivi et recevoir des alertes personnalisées sur l'état de chaque plante.
L'aspect communautaire de l'application permettra de mettre en vedette ses
plantes et réagir aux profil d'autres plantes, ainsi que permettre aux
utilisateurs plus expérimentés de venir au secours d'autres plantes en détresse.

L'application aura plusieurs fonctionnalités modulaires qui permettront de
fournir des conseils plus spécialisés si la carte à capteurs est disponible.
Mais il sera également possible de l'utiliser sur la base des informations
rentrées manuellement par l'utilisateur sur l'état de la plante.
L'application enverra par la suite des notification et rappels sur les
différentes opérations d'entretien de la plante (arrosage, rempotage, taille,
...).

On envisage aussi plusieurs autres fonctionnalités qui seront implémentées si
le temps le permet :

- Complémenter les informations disponibles sur la base de données par des
  utilisateurs expérimentés.
- Redirection vers des boutiques en ligne (Ex. lien pour vers un site pour
  acheter le terreau suggéré par l’application).
- Lors d’une utilisation de la version sans capteurs, utilisation du capteur de
  lumière du téléphone afin de mesurer l’ensoleillement de la plante.
- Événements et d'autres conseils suggéré par nos partenaires.

## Périmètre

Grâce à l'importance de la problématique ciblée par notre projet, les
opportunités dans le domaine sont nombreuses. Afin d'incrémenter nos chances de
réussite, il est fondamental de cadrer notre projet et limiter sa portée.
En particulier, nous avons décidé d'exclure les aspects suivants :

- Établissement d'une communauté et modération des interactions entre les
  utilisateurs : Dans le cadre de cette proposition de projet, on aimerait
  limiter la main d’œuvre nécessaire pour l'exploitation de l'application.
- Analyse automatisée des images téléchargées par l'utilisateur.
- Technologies d'automatisation complète de l'élevage de plantes
  (e.g. Arrosage automatique).
- Négociation de partenariats.
- Promotion de l'application.
- Reconnaissance de l'espèce de la plante à l'aide d’une photo prise par
  l'utilisateur.
- Analyse des images téléchargées par l'utilisateur.

La liste qui précède est bien évidement non exhaustive et pourra être remise en
question pour des éventuelles extensions du projet.

## Description de l'existant

- Indicateurs pour l'évaluation automatisée de l'état et la santé d'une plante :
  À déterminer lesquels par l’équipe de recherche métier et étude de marché.
- Technologies pour l'application Android : Java, AndroidX, Material design.
- Technologies pour le côté serveur de l'application :
  Maven, Java, Spring boot, MVC, HTTP Rest.
- Bases de données utilisés pour stocker les données générées par
  les utilisateurs : PostgreSQL, InfluxDB.
- Technologies pour la programmation de la carte à capteurs :
  PlatformIO, Arduino, ESP8266.
- Bases de données avec des informations sur l'entretien des plantes :
  À déterminer exactement lesquelles par l'équipe de recherche métier et étude
  de marché.

## Objectifs visés par le projet

### Besoins fonctionnels

Possibilité de créer un profil par plante intégrant les fonctionnalités
suivantes :

- Questions qualitatives sur le bien-être de la plante en question.
- Téléchargement d'une photo journalière par plante.
- Affichage de l'évolution d'une plante à l'aide des photos journalières.
- Informations calculées par notre système sur l'état de la plante.
- Réglages des notifications des tâches à effectuer selon les informations
  une base de données spécialisée et l'état calculé par notre application.
- Conseils d'ordre générale en fonction du type de plante.
- Si l'utilisateur possède la carte à capteurs, un suivi intelligent plus précis
  est à disposition.
- Possibilité de publier le profil de la plante avec ses photos et son
  historique dans la communauté.
- Possibilité de demander de l'aide aux membres de la communauté.

Possibilité de naviguer dans les galeries des profils de plantes publiés par
des membres de la communauté :

- Laisser des réactions (style émoji) sur les profils et sur chaque photo de la
  gallérie de la plante.
- Parcourir les profils des plantes par libellés (e.g. Roses, jardin,
  chambre, ...).

Possibilité d’interaction entre utilisateurs dans l'espace communautaire :

- Émettre des commentaires sur les plantes ayant besoin d'aide.

Le suivi intelligent permet d'émettre des alertes et notifications lorsque les
capteurs détectent des niveaux peu adaptés d'humidité, ensoleillement ou
autres paramètres pour le type de plante en question.

### Besoins non fonctionnels

- Créer un écosystème communautaire qui engage les interactions des utilisateurs
  à travers de la publication des profils de plantes et des réactions d'autres
  utilisateurs aux profils et photos partagés par les utilisateurs.
- Limiter les interactions nocives dans la communauté afin de créer un
  environnement sécurisé et inclusif pour tous les utilisateurs.
- Fournir une interface intuitive et des conseils pertinents sans encombrer
  l'utilisateur.

## Déroulement du projet

Organisation du projet en 3 phases :

```plantuml
@startgantt
Project starts the 17th of march 2021
saturday are closed
sunday are closed
2021/04/05 to 2021/04/11 is closed
[Viabilite du produit] lasts 13 days
then [Prototypage] lasts 15 days
then [Validation par des tiers] lasts 15 days
@endgantt
```

### Phase 1 : Attester la viabilité du produit

#### Embarqué (phase 1)

- Obtenir les capteurs.
- Capturer des données d'un capteur et les envoyer dans une base de données.
- Mettre en place la communication bluetooth entre l'application et la carte.

#### Métier (phase 1)

- Choisir les capteurs que l'on veut avoir dans la carte.
- Rechercher les besoins du marché (voir ce dont les gens ont besoin).
- Apprendre a faire des Mockups.
- Choisir une base de données existante dont nous pourrons tirer les données.

#### UX (phase 1)

- Choisir le framework graphic que l'on va utiliser.
- Mettre en place une application Android.
- Définir les interactions de la carte avec l'utilisateur (pattern des leds
  de couleur).

#### Architecture et infrastructure (phase 1)

- Configuration de l'intégration continue et assurance de la qualité du code.

### Phase 2 : Création d'un prototype

#### Embarqué (phase 2)

- Ecrire le manuel utilisateur de mise en place de la carte.
- Etablir le protocole et les mécanismes de communication bluetooth entre
  l'application et la carte pour permettre la connexion au serveur.
- Gerer les erreurs matériels.

#### Métier (phase 2)

- Etablir la priorité des fonctionalitées a livrer.

#### UX (phase 2)

- Implémentation des Mockups.
- Concevoir l'usabilité de l'interface graphique par des utilisateurs non
  techniques qui sera validé en fin de la phase (e.g. message d'erreurs
  détaillées).
- Implémentation des fonctions de notification et d'alarme.

#### Architecture et infrastructure (phase 2)

- Mettre en place la génération du manuel utilisateur en pdf depuis le markdown.
- Génération des données fictives de test pour les fonctionalitées
  communautaires.

### Phase 3 : Validation du produit

#### Embarqué (phase 3)

- Certifier la stabilité de l'application et la sécurité des connexions.

#### Métier (phase 3)

- Mise en place du support pour les fonctionnalitées communautaires.

#### UX (phase 3)

- Corriger les bugs visuels.
- Adapter l'application en fonction des retours des utilisateurs.

#### Architecture et infrastructure (phase 3)

- Validation artistique et validation conforme au cahier des charges.

#### Toutes les équipes

- Rapport final du projet.

## Plan d'assurance qualité

- Tests à blanc par des utilisateurs potentielles de l'application qui ne font
  pas partie de l'équipe.

- Intégration continue sur le code du serveur ainsi que des métriques sur la
  qualité du code à chaque contribution.

- Intégration continue:
  - Génération automatisée des fichiers d'installation de l'application
    Android au format APK.
  - Génération automatisée du code serveur sous format jar.
  - Génération des pages d'aide sous format HTML.

## Documentation

- Documentation sur la construction du serveur java et de l'application Android
  ainsi que du déploiement du serveur.
- Documentation du logiciel sous forme de Javadoc.
