---
title: Privacy policy
author: Roosembert Palacios
toc: yes
---

## English, not legalese

Privacy is important, and we want you to understand the issues involved.
In the following document we try to be as clear and concise as possible.

Where you read 'the app', 'the android application' or 'the user client', it
refers to the [user application][app] provided in this repository.

[app]: ../src/client

Where you read 'the server' it refers to the [application server][server]
provided in this repository.

[server]: ../src/server

Where you read 'the service' in this document, it refers to the services
provided through the application server, including the application server
itself and any other software component critical to its operations on the
open internet.

Where you read 'the sensor board' it refers to any [Greenzz sensor board][sb]
configured through the app using the service.

[sb]: ../src/sensor-board/

Where you read 'Roos' or 'We', it refers to _Roosembert Palacios_ as the
individual operator of the service.

By using the app provided for download by the CI or compiling it yourself but
reusing the APIs provided in the source files, _Roosembert Palacios_ is the
[Data Controller][data-controller] and [Data Protection Officer][dpo] (DPO)
for your data.
He can be reached at the email address 'roosembert' DOT 'palacios' AT 'posteo'
DOT 'ch' (without single quotes, joined with the corresponding characters).

[data-controller]: https://ec.europa.eu/info/law/law-topic/data-protection/reform/rules-business-and-organisations/obligations/controller-processor/what-data-controller-or-data-processor_en
[dpo]: https://www.gdpreu.org/the-regulation/key-concepts/data-protection-officer/

Should you have other questions or concerns about this document, please open
an issue in this repository or send an email at the aforementioned address.

## Scope of This Document

This document explains what data is collected and how it is processed as it
relates to the app and eventual sensor boards accessing the service.

## Access to your data

### Legal basis for processing data

The service collects your IP address when the app accesses to the service.
The service collects the sensor board IP address, when it accesses the service.
This data is collected under [Legitimate Interest][Legitimate Interest] to
support operational maintenance and protect against malicious actions against
the service or other services collocated in Roos' infrastructure.

[Legitimate Interest]: https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/legitimate-interests/when-can-we-rely-on-legitimate-interests/

## What information is collected and why

### Information provided by you

We collect your username when you register an account.
This information is kept since it's essential to the functioning of the app
as means to identify you when you log in from a different device.

We collect the plant profiles and the daily photos uploaded by the app along
with their creation and upload timestamp.
This information is kept since it's an integral part of the functioning of
the app when you log in from a different device.

### Information collected automatically when you use the service

#### Connect information

We log the IP addresses of everyone who accesses the service.
This data is used in order to mitigate abuse, debug operational issues,
and monitor traffic patterns.
Our logs are kept for 30 days.

#### Usage information

We log the IP addresses used to create user accounts.
This data is used in order to mitigate abuse.
This IP address is permanently associated with your username.

When you register a sensor board, its MAC address is used as a unique
identifier and stored along side your username.
When the sensor board publishes data, it's associated to its MAC address,
which in turn is associated with your username.
Both of these can be retrieved by the app in order to display to you live
information from your sensor board(s).
Your sensor board's MAC address is permanently associated with your username.
The data published by the sensor board is stored indefinitely but may be
deleted at any time.

### Sharing Data in Compliance with Enforcement Requests and Applicable Laws

In exceptional circumstances, we may share information about you with a third
party if we believe that sharing is reasonably necessary to comply with any
applicable law, regulation, legal process or governmental request.

### How Do You Handle Passwords

We do not handle password nor plan to.

### How can I access my information?

You can request a copy of the information associated to your username by
emailing the Data Controller at the aforementioned email address (see section
_English, not legalese_).

### Who can access my information?

The only person capable of reading your information is your Data Controller
(see section _English, not legalese_).

We store your data in Lausanne, Switzerland encrypted at rest under
`aes-xts-plain64`.
You data may temporally be in transit through Amsterdam, Zurich, Geneva or
Stockholm encrypted under `ChaCha20` while it reaches our processing servers
in Lausanne or Zurich, Switzerland.
We reserve the right to change the physical location of the processing servers
within Switzerland.

## Making a complaint

We try to meet the highest standards when collecting and using personal
information.
For this reason, we take any complaints we receive about this very seriously.
Don't hesitate to contact your Data Protection Officer if needed.
Please note however, the service is provided in a best-effort voluntary basis
and it may take a while to hear back from us.

## Validity of this document

This document (and any eventual revisions to it) are valid for as long as the
document (or a git commit applying a change on it) is signed with the
following [PGP key][pgp]:

[pgp]: https://en.wikipedia.org/wiki/Pretty_Good_Privacy

```console
pub   rsa2048/CAAAECE5C2242BB7 2015-02-21 [SC]
      Key fingerprint = 78D9 1871 D059 663B 6117  7532 CAAA ECE5 C224 2BB7
```

In case of discrepancy or conflict, the signed commit with the latest commit
date shall prevail.
