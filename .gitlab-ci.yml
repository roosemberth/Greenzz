---
workflow:
  rules:
    # Do not run merge request pipelines (we use branch pipelines instead)
    - if: $CI_MERGE_REQUEST_IID
      when: never
    # Run branch pipeline if already associated to a merge request
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
    # If a tag has been created
    - if: $CI_COMMIT_TAG
    # Unconditionally upon merging to the default branch
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Unconditionally if explicitly requested
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version"
  POSTGRES_DB: ci-pinocchio
  POSTGRES_USER: pinocchio
  POSTGRES_PASSWORD: placeholder-password
  POSTGRES_HOST_AUTH_METHOD: trust

.mavencache: &mavencache
  key: "$CI_BUILD_REF_NAME-cache-maven"
  paths:
    - .m2/repository

.fluttercache: &fluttercache
  key: "$CI_BUILD_REF_NAME-cache-flutter"
  paths:
    - .pub-cache
    - src/client/.dart_tool
    - src/client/android/.gradle
    - src/client/build

stages:
  - Build
  - Docs
  - Test
  - Artifacts

Render reports:
  image: registry.gitlab.com/islandoftex/images/context:beta
  stage: Build
  before_script:
    - apt update
    - apt install --no-install-recommends -y
      make pandoc vim
      inkscape pandoc-plantuml-filter
      graphviz gsfonts
      lmodern texlive-latex-base texlive-latex-recommended
  script:
    - for f in reports/*/Makefile; do pushd "$(dirname "$f")"; make; popd; done
    - mkdir -p artifacts/reports
    - find reports -maxdepth 3 -name '*.pdf' -exec mv '{}' artifacts/reports/ \;
  artifacts:
    name: reports
    paths:
      - artifacts/reports/*
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - reports/*
        - reports/**/*

Application server:
  image: maven:3-openjdk-11-slim
  stage: Build
  cache: *mavencache
  script:
    - mkdir -p artifacts
    - cd src/server
    - "mvn package -Dmaven.test.skip=true $MAVEN_CLI_OPTS"
    - cp target/*.jar ../../artifacts
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/server/*
        - src/server/**/*
  artifacts:
    name: Application server jar
    paths:
      - artifacts/

Test server:
  image: maven:3-openjdk-11-slim
  services:
    - influxdb:1.8.5
    - postgres:12.2-alpine
  stage: Test
  needs:
    - Application server
  cache:
    <<: *mavencache
    policy: pull
  script:
    - cd src/server
    - >-
      mvn verify $MAVEN_CLI_OPTS
      "-Dgreenzz.datasource.influx.url=http://influxdb:8086"
      "-Dspring.datasource.url=jdbc:postgresql://postgres:5432/$POSTGRES_DB"
      "-Dspring.datasource.username=$POSTGRES_USER"
      "-Dspring.datasource.password=$POSTGRES_PASSWORD"
      "-Dspring.jpa.properties.hibernate.hbm2ddl.auto=none"
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/server/*
        - src/server/**/*

Sensor board firmware:
  image: ubuntu:rolling
  stage: Build
  before_script:
    - apt update
    - apt install --no-install-recommends -y make g++ python3-pip
    - pip3 install platformio
  script:
    - make sensor-board-artifacts
  artifacts:
    name: sensor board firmware
    paths:
      - artifacts/
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/sensor-board/*
        - src/sensor-board/**/*

Sensor board tests:
  image: ubuntu:rolling
  stage: Test
  needs: []
  before_script:
    - apt update
    - apt install --no-install-recommends -y g++ python3-pip
    - pip3 install platformio
  script:
    # Platform needs to be manually installed, an upstream bug exists.
    # https://github.com/platformio/platformio-core/issues/3901
    - pio platform install native
    - cd src/sensor-board; pio test -v -e native
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/sensor-board/*
        - src/sensor-board/**/*

Flutter client:
  image: cirrusci/flutter
  stage: Build
  cache: *fluttercache
  before_script:
    - apt update
    - apt install rsync -y --no-install-recommends
  script:
    - mkdir -p .pub-cache
    - rsync -a .pub-cache ~  # Recursive merge, use our copy on conflict.
    - mkdir -p artifacts
    - cd src/client
    - flutter build apk
    - cp build/app/outputs/flutter-apk/*-*.apk ../../artifacts
    - cd ../..
    - rm -fr .pub-cache
    - mv ~/.pub-cache .
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/client/*
        - src/client/**/*
  artifacts:
    name: Android application
    paths:
      - artifacts/*.apk

Flutter client dart docs:
  image: cirrusci/flutter
  stage: Docs
  needs:
    - job: Flutter client
  cache:
    <<: *fluttercache
    policy: pull
  script:
    - rm -fr ~/.pub-cache
    - mkdir -p .pub-cache
    - cp -r .pub-cache ~
    - mkdir -p artifacts
    - cd src/client
    - dartdoc
    - cp -R doc ../../artifacts/client-dartdoc
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/client/*
        - src/client/**/*
  artifacts:
    name: Flutter client documentation
    paths:
      - artifacts/client-dartdoc

Test Flutter client:
  image: cirrusci/flutter
  stage: Test
  needs:
    - Flutter client
  cache:
    <<: *fluttercache
    policy: pull
  before_script:
    - apt update
    - apt install rsync -y --no-install-recommends
  script:
    - mkdir -p .pub-cache
    - rsync -a .pub-cache ~  # Recursive merge, use our copy on conflict.
    - cd src/client
    - flutter test
    - flutter analyze
  rules:
    - if: '$CI_FORCE_RUN_ALL_STAGES == "true"'
    - if: '$CI_COMMIT_BRANCH == "master"'
    - changes:
        - src/client/*
        - src/client/**/*

Package distributables:
  stage: Artifacts
  needs:
    - job: Render reports
      optional: true
    - job: Flutter client
      optional: true
    - job: Flutter client dart docs
      optional: true
    - job: Application server
      optional: true
    - job: Sensor board firmware
      optional: true
  script: "true" # Do nothing
  artifacts:
    name: distributables
    expose_as: distributables
    paths:
      - artifacts/

# Static page generation. The job name must match 'pages' as described in the
# Gitlab documentation: https://docs.gitlab.com/ee/ci/yaml/README.html#pages
pages:
  stage: Artifacts
  needs:
    - job: Flutter client dart docs
  script:
    - mkdir -p public/apidocs/
    - cp -r artifacts/client-dartdoc/api public/apidocs/client
  artifacts:
    paths:
      - public
  only:
    - master
