# This file contains automation for common tasks, it is not meant to be portable
# but merely support properly configured platforms.
# In particular, this is used by the CI to render release artifacts.

SENSOR_FW = artifacts/nodemcu-32s/greenzz.firmware.bin

# Virtual targets not generating files
.PHONY = all sensor-board-artifacts

all: sensor-board-artifacts

sensor-board-artifacts: $(SENSOR_FW)

$(SENSOR_FW):
	cd src/sensor-board/; platformio run
	mkdir -p "$(dir $@)"
	cp "$$HOME/.platformio/packages/framework-arduinoespressif32/tools/sdk/bin/bootloader_dio_40m.bin" "$(dir $@)"
	cp "src/sensor-board/.pio/build/nodemcu-32s/partitions.bin" "$(dir $@)"
	cp "$$HOME/.platformio/packages/framework-arduinoespressif32/tools/partitions/boot_app0.bin" "$(dir $@)"
	cp "src/sensor-board/.pio/build/nodemcu-32s/firmware.bin" "$@"
