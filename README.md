# Green Buzz (Greenzz)

This project originated as a semester project at [HEIG-VD][heig].
If you're interested in that, make sure to check the [reports
directory](./reports/) (in french).

[heig]: https://heig-vd.ch/

This repository contains the multiple software components of _Greenzz_, a
system designed to assist people on the care and maintenance of their plants.

## Project components

### User Application

The [user application][app] allows users to record the evolution of their
plants and take daily photos to see them grow.
You can **download** the latest version [here][app-download]
([privacy policy][privacy-policy] regarding the usage of this application).

[app]: ./src/client/
[app-download]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/raw/artifacts/app-release.apk?job=Package%20distributables
[privacy-policy]: ./reports/10-privacy-policy.md

| ![Welcome screen](./reports/media/entry.gif) |
  ![Creating a new profile](./reports/media/new-profile.gif) |
  ![Retrieving plant information](./reports/media/plant-info.gif)
|--|--|--|

### Sensor board

The [sensor board][sb] provides real-time data and enables notifications to
help you tend your plant needs.
You can **download** the latest firmware [here][sb-download].
Instructions on how to update the firmware can be found [here][sb-flashing].

[sb]: ./src/sensor-board/
[sb-download]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/browse/artifacts/nodemcu-32s?job=Package%20distributables
[sb-flashing]: ./src/sensor-board/docs/

| ![Sensor board](./reports/media/sensor-board-1.gif) |
  ![Help is on the way](./reports/media/installed-sensor-board.gif) |
  ![Pairing the sensor board](./reports/media/add-sensor-board-explained.gif)
|--|--|--|

### Application Server

A core component providing the user application with an extensive plant care
database of 5500+ plant species.
The [application server][server] also mediates the interactions between the
sensor board and the user application.

You can **download** the latest version [here][server-download].

[server]: ./src/server/
[server-download]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/raw/artifacts/server-0.0-SNAPSHOT.jar?job=Package%20distributables

## Continuous integration

The multiple software components and the many reports are rendered, built,
tested and released using GitLab's [CI][CI].

[CI]: https://docs.gitlab.com/ee/ci/

You can access the latest CI distributables [here][ci-distributables].

[ci-distributables]: https://gitlab.com/roosemberth/Greenzz/-/jobs/artifacts/master/browse/artifacts?job=Package%20distributables

### How to use the CI when contributing to this project

The CI is configured to build only the affected components when a MR is opened
and *all* components upon merging into master.
An additional job collects the different artifacts produced by every stage into
a single artifact that may be independently distributed.

A contributor may force all CI stages to be ran for a given **pipeline** by
setting the [CI variable][ci-vars] `CI_FORCE_RUN_ALL_STAGES` to value `true`.
This can be performed by either launching a manual pipeline in the GitLab UI or
pushing with option `ci.variable=CI_FORCE_RUN_ALL_STAGES=true`. e.g.:

```console
git push origin HEAD:new-feature -o ci.variable=CI_FORCE_RUN_ALL_STAGES=true
```

[ci-vars]: https://docs.gitlab.com/ee/ci/variables/README.html

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
