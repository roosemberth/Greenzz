# Greenzz client application

This directory contains the Greenzz client application (“the app”).

If you're looking for _ready-to-download_ packages, please check the [dedicated
section on the project README][app-on-project-readme].

[app-on-project-readme]: https://gitlab.com/roosemberth/Greenzz#user-application

This project is build using [flutter][flutter].
The client application does not require an internet connection to provide basic
functionnalities, but a connection to the Greenzz server is necessary to get the
best experience and provide most advanced functionnalities.
The client application may also be used to register and configure the [Greenzz
sensor board][sb] by connecting to it via [Bluetooth LE][BLE].

[flutter]: https://flutter.dev/
[sb]: https://gitlab.com/roosemberth/Greenzz#sensor-board
[BLE]: https://en.wikipedia.org/wiki/Bluetooth_Low_Energy

You can find the detailed documentation about flutter on their website.
A reference workflow is provided in this document for convenience.
The reference workflow assumes you have flutter and its Android component
installed in your system.
Please refer to [their installation guide][instal-flutter] if needed.

[install-flutter]: https://flutter.dev/docs/get-started/install

## Vendor settings

The client application uses services over the internet provided by the [Greenzz
application server][server].
These customizations may be changed by adding the following flags to your
flutter commands: `--dart-define=VARNAME=value`.

[server]: https://gitlab.com/roosemberth/Greenzz#application-server

At the time of writing, the following vendor flags may be used to customize the
build; we recommend nonetheless to check
the [`AppPreferences` class][./lib/Services/AppPreferences.dart]:

```console
$ flutter run \
  --dart-define=SERVER_WS_API=wss://greenzz.orbstheorem.ch/ws \
  --dart-define=SERVER_REST_API=https://greenzz.orbstheorem.ch/ \
  --dart-define=SERVER_PRIVACY_POLICY_URL=https://gitlab.com/roosemberth/Greenzz/-/blob/master/reports/10-privacy-policy.md
```

If left unspecified, the build will use the infrastructure provided by one of
our project members (see it's [privacy policy][pp]).

[pp]: https://gitlab.com/roosemberth/Greenzz/-/blob/master/reports/10-privacy-policy.md

## API documentation

The CI publishes the client application API documentation (generated using
`dartdoc`) for the master branch.
They can be accessed [here][apidoc].

[apidoc]: https://roosemberth.gitlab.io/Greenzz/apidocs/client/

## Reference workflow

You can make sure the necessary flutter components are correctly installed in
your system by running `flutter doctor`.
If you'd like to use your own device instead of the android emulator, make sure
your device is available to flutter by running `flutter devices`.

You may run the Android application by running `flutter run` if an Android
device is not connected, an emulator will be spawn.
