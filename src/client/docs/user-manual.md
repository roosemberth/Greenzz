# User application usage manual

## First start

After starting the application, the first step is to create the profile of your
first plant. To do this, you can click on the green button in the center of the
screen or on the button at the top right. You will arrive on a screen that will
ask you for information about your first plant (name, state of the plant, age
of the plant and the species of the plant). After entering the information, you
arrive on the page of the plant. If this is your first time using the
application, a short tutorial explains the role of the different buttons. By
using the back button, you can return to the home page where you can view the
plant you have created or create a new plant by using the button at the top
right.

## Account creation and login

It is necessary to create an account to synchronize your plants between several
devices or to retrieve data from a sensor card. To do this, click on the button
at the top left and select the option 'login or register an account'. You will
then have the possibility to create an account or to connect to an existing
account. For the moment, passwords are not supported so you just need to enter
the username to create an account or login. When you create your account,
you can consult the privacy policy by clicking on the link.

## Taking pictures

The app allows you to take one photo of your plants per day. There are two ways
to take a picture : either you use the button available on each plant in the
plant list (this button is only available if you don't have any pictures for
this plant today), or you use the green button at the bottom right of the plant
profile. If you take a second photo of the plant on the same day, the first
photo will be deleted.

## Add, delete or modify information about the plant

If you want to change the information about the plant, you can click on the
button at the top right of the plant profile and select the option Rename plant
or Edit details. You can also add or remove tags from the plant profile to
categorize your plants. You can also delete the plant from the top right menu.

## Using a sensor board

For more information on the use of a sensor board, you can look at [the
documentation of the sensor board](
https://gitlab.com/roosemberth/Greenzz/-/blob/master/src/sensor-board/docs/20-Getting-started_nodemcu-32s-B.md).
