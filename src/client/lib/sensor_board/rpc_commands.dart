import 'dart:async';

import '../Services/SensorService.dart';

enum GreenzzRPCError {
  /// Request requires no active WiFi AP, but connected to a WiFi AP.
  ERR_CONNECTED,

  /// The device was unable to disassociate from the WiFi AP.
  ERR_COULD_NOT_DISCONNECT,

  /// Device occupied or otherwise unable to handle the specified request.
  ERR_BUSY,

  /// Connection to the requested access point could not complete.
  ERR_AP_CONNECTION_FAILED,

  /// Device associated to an AP but no IP configuration yet.
  ERR_IN_PROGRESS,

  /// Device associated to an AP but received no IP configuration.
  ERR_NO_IP,

  /// Device has IP connectivity but was unable to reach the server.
  ERR_UNREACHABLE,

  /// Device is not associated to a WiFi AP.
  ERR_NOT_CONNECTED_TO_AP,

  /// The requested method has not been implemented.
  ERR_NOT_IMPLEMENTED,

  // JSONRPC 2.0 pre-defined errors. Refer to the specification.
  ERR_PARSE_ERROR,
  ERR_INVALID_REQUEST,
  ERR_METHOD_NOT_FOUND,
  ERR_INVALID_PARAMS,
  ERR_INTERNAL_ERROR,
}

const _errorCodes = {
  -32099: GreenzzRPCError.ERR_CONNECTED,
  -32098: GreenzzRPCError.ERR_COULD_NOT_DISCONNECT,
  -32097: GreenzzRPCError.ERR_BUSY,
  -32096: GreenzzRPCError.ERR_AP_CONNECTION_FAILED,
  -32095: GreenzzRPCError.ERR_IN_PROGRESS,
  -32094: GreenzzRPCError.ERR_NO_IP,
  -32093: GreenzzRPCError.ERR_UNREACHABLE,
  -32092: GreenzzRPCError.ERR_NOT_CONNECTED_TO_AP,
  -32091: GreenzzRPCError.ERR_NOT_IMPLEMENTED,
  -32700: GreenzzRPCError.ERR_PARSE_ERROR,
  -32600: GreenzzRPCError.ERR_INVALID_REQUEST,
  -32601: GreenzzRPCError.ERR_METHOD_NOT_FOUND,
  -32602: GreenzzRPCError.ERR_INVALID_REQUEST,
  -32603: GreenzzRPCError.ERR_INTERNAL_ERROR,
};

class JsonRpcResponse {
  final int id;
  final dynamic result;

  /// Build from a json map
  JsonRpcResponse.fromJSON(Map<String, dynamic> resp)
      : id = resp["id"] as int,
        result = resp["result"] {
    if (resp["jsonrpc"] as String != "2.0")
      throw Exception("Could not decode message from sensor board");
  }

  JsonRpcResponse(this.id, this.result);
}

class Tuple<T1, T2> {
  final T1 e1;
  final T2 e2;

  const Tuple(this.e1, this.e2);
}

enum _Request {
  Echo,
  Reset,
  WiFiQuery,
  WiFiList,
  WiFiDisconnect,
  WiFiConnect,
  WiFiMac,
  NetworkLocal,
  NetworkServer,
  ConfigureInflux,
}

const _methods = {
  _Request.Echo: "echo",
  _Request.Reset: "reset",
  _Request.WiFiQuery: "wifi_query",
  _Request.WiFiList: "wifi_list",
  _Request.WiFiDisconnect: "wifi_disconnect",
  _Request.WiFiConnect: "wifi_connect",
  _Request.WiFiMac: "wifi_mac",
  _Request.NetworkLocal: "network_local",
  _Request.NetworkServer: "network_server",
  _Request.ConfigureInflux: "configure_influxdb",
};

/// A WiFi network connection information
class WiFiNetworkEntry {
  final String ssid;
  final int rssi;
  final String bssid;
  const WiFiNetworkEntry(this.ssid, this.rssi, this.bssid);
  WiFiNetworkEntry.fromJSON(Map<String, dynamic> obj)
      : ssid = obj["ssid"] as String,
        rssi = obj["rssi"] as int,
        bssid = obj["bssid"] as String;
}

/// Information on the local network
class LocalNetworkData {
  final String ip;
  final String gateway;
  final String netmask;
  const LocalNetworkData(this.ip, this.gateway, this.netmask);
  LocalNetworkData.fromJSON(Map<String, dynamic> obj)
      : ip = obj["ip"] as String,
        gateway = obj["gateway"] as String,
        netmask = obj["netmask"] as String;
}

/// An API to communicate with a Greenzz sensor board
class GreenzzAPI {
  StreamSink<Map<String, dynamic>> _sink;
  final Map<String, Tuple<_Request, Completer>> continuations = {};
  int _lastId = 0;

  GreenzzAPI(this._sink);

  /// Processes a response to a request
  ///
  /// Throws [Exception] if the response is not JSONRPC 2.0, the response has no
  /// id, its a response to an unknown request or the error code couldn't be decode
  void accept(Map<String, dynamic> response) {
    if (response["jsonrpc"] != "2.0")
      throw Exception("Received object is not a valid JSONRPC 2.0 response");
    if (response["id"] == null)
      throw Exception("Received RPC error cannot be routed: $response");
    var continuation = continuations[response["id"]];
    if (continuation == null)
      throw Exception("Received a response we didn't _request: $response.");
    final c = continuation.e2;

    if (response["error"] != null) {
      final e = response["error"]!;
      final error = _errorCodes[e["code"] as int];
      if (error == null)
        throw Exception("Received RPC error but could not decode it: $e");

      print("Received RPC Error: ${e["message"]}");
      return c.completeError(error);
    }

    var r = response["result"];
    switch (continuation.e1) {
      case _Request.Reset:
      case _Request.WiFiDisconnect:
      case _Request.WiFiConnect:
        return c.complete(null);
      case _Request.Echo:
      case _Request.WiFiMac:
        return c.complete(r as String);
      case _Request.WiFiQuery:
        return c.complete(WiFiNetworkEntry.fromJSON(r));
      case _Request.WiFiList:
        return c.complete((r as List).map((e) => WiFiNetworkEntry.fromJSON(e)));
      case _Request.NetworkLocal:
        return c.complete(LocalNetworkData.fromJSON(r));
      case _Request.NetworkServer:
        return c.complete(response["response"] as int);
      case _Request.ConfigureInflux:
        return c.complete(null);
      default:
        throw Exception("Programming error: Unhandled call ${continuation.e1}");
    }
  }

  Future _dispatch(_Request method, Map<String, String> params) {
    Completer c = Completer();
    final id = (_lastId++).toString();
    continuations[id] = Tuple(method, c);
    _sink.add({
      "id": id,
      "jsonrpc": "2.0",
      "method": _methods[method],
      "params": params
    });
    return c.future;
  }

  /// Returns the message echoed.
  ///
  /// Requests to echo the [msg]
  Future<String> echo(String msg) {
    return _dispatch(_Request.Echo, {"msg": msg}).then((r) => r as String);
  }

  Future<Null> reset() {
    return _dispatch(_Request.Reset, {}) as Future<Null>;
  }

  /// Returns a WifiNetworkEntry corresponding to the network of a sensor board
  ///
  /// Requests the information of the wifi network used by a sensor board
  Future<WiFiNetworkEntry> wifiQuery() {
    return _dispatch(_Request.WiFiQuery, {}).then((r) => r as WiFiNetworkEntry);
  }

  /// Returns a List<WifiNetworkEntry> corresponding to the networks
  /// available to a sensor board.
  ///
  /// Requests the list of wifi network a sensor board can see
  Future<List<WiFiNetworkEntry>> wifiList() {
    return _dispatch(_Request.WiFiList, {})
        .then((r) => r.toList() as List<WiFiNetworkEntry>);
  }

  /// Requests that a sensor board disconnects from the wifi
  Future<Null> wifiDisconnect() {
    return _dispatch(_Request.WiFiDisconnect, {}).then((r) => r as Null);
  }

  /// Requests that a sensor board connects to a wifi using [ssid] and [password]
  Future<Null> wifiConnect(String ssid, String password) {
    final args = {"ssid": ssid, "password": password};
    return _dispatch(_Request.WiFiConnect, args).then((r) => r as Null);
  }

  /// Returns the mac address of the wifi a sensor board is connected to
  Future<String> wifiMac() {
    return _dispatch(_Request.WiFiMac, {}).then((r) => r as String);
  }

  /// Returns a LocalNetworkData corresponding to the local network of a sensor board
  ///
  /// Requests the information on the local network of a sensor board
  Future<LocalNetworkData> networkLocal() {
    return _dispatch(_Request.NetworkLocal, {})
        .then((r) => r as LocalNetworkData);
  }

  Future<int> networkServer(String ip) {
    return _dispatch(_Request.NetworkServer, {"ipv4": ip})
        .then((r) => r as int);
  }

  /// Requests a sensor board to configure an influxDb connection using the
  /// data in [registration]
  Future<Null> configureInflux(SensorInfluxRegistration registration) {
    return _dispatch(_Request.ConfigureInflux, {
      "influxUsername": registration.influxUser,
      "influxPass": registration.influxPass,
      "influxURL": registration.influxApiUrl,
      "influxDatabase": registration.influxDatabase
    }).then((r) => r as Null);
  }
}
