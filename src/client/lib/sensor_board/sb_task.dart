import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:permission_handler/permission_handler.dart';

import './location_permission_observer.dart';
import './protocols.dart';
import '../Services/PlantService.dart';

class ConfigureSensorBoardTask extends StatelessWidget {
  final PlantID plantUuid;
  final FlutterBlue bt;
  final LocationPermissionObserver locationPermission;

  ConfigureSensorBoardTask(this.plantUuid,
      {Key? key,
      FlutterBlue? bt,
      LocationPermissionObserver? locationPermission})
      : bt = bt ?? FlutterBlue.instance,
        locationPermission =
            locationPermission ?? LocationPermissionObserver.instance,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: BluetoothSelectDeviceScreen.routeName,
      onGenerateRoute: (RouteSettings newRouteSettings) {
        WidgetBuilder builder;
        switch (newRouteSettings.name) {
          case BluetoothSelectDeviceScreen.routeName:
            builder = (BuildContext _) => BluetoothSelectDeviceScreen(
                bt: bt, locationPermission: locationPermission);
            break;
          case SbConfigScreen.routeName:
            final device = newRouteSettings.arguments! as BluetoothDevice;
            builder = (BuildContext _) {
              return SbConfigScreen(
                plantUuid,
                device,
                onComplete: Navigator.of(context).pop,
              );
            };
            break;
          default:
            throw Exception("Invalid route: ${newRouteSettings.name}");
        }
        return MaterialPageRoute(builder: builder, settings: newRouteSettings);
      },
    );
  }
}

/// A screen for the selection of a bluetooth device
class BluetoothSelectDeviceScreen extends StatelessWidget {
  static const routeName = "bluetooth-connect";
  final FlutterBlue bt;
  final LocationPermissionObserver locationPermission;

  BluetoothSelectDeviceScreen(
      {FlutterBlue? bt, LocationPermissionObserver? locationPermission})
      : bt = bt ?? FlutterBlue.instance,
        locationPermission =
            locationPermission ?? LocationPermissionObserver.instance;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<BluetoothState>(
        stream: bt.state,
        initialData: BluetoothState.unknown,
        builder: (_, snapshot) {
          final btState = snapshot.data;
          return StreamBuilder<PermissionStatus>(
            stream: locationPermission.stream,
            initialData: PermissionStatus.denied,
            builder: (_, snapshot) {
              final locationState = snapshot.data!;
              if (btState != BluetoothState.on)
                return BluetoothUnavailableScreen(state: btState);
              if (locationState != PermissionStatus.granted)
                return LocationAccessNotGrantedScreen(locationState);
              return ScanForSensorBoardScreen(bt: bt);
            },
          );
        });
  }
}

/// A screen displayed if the application doesn't have access to the location
class LocationAccessNotGrantedScreen extends StatelessWidget {
  const LocationAccessNotGrantedScreen(this.state, {Key? key})
      : super(key: key);
  final PermissionStatus state;

  bool get permanentlyDenied => state == PermissionStatus.permanentlyDenied;

  /// Returns the message to display according to the property [permanentlyDenied]
  String getMsg() {
    if (permanentlyDenied)
      return "The app will never use your location, but we need to request access "
          "to your location in order to access your Bluetooth adapter.\n\n"
          "Location access has been permanently denied.\n"
          "You can change this in the settings.";

    return "The app will never use your location, but we need to request "
        "access to your location in order to access your Bluetooth adapter.";
  }

  Widget getButton() {
    if (permanentlyDenied)
      return ElevatedButton(
        child: Text("Go to settings."),
        onPressed: () => openAppSettings(),
      );
    return ElevatedButton(
      child: Text("Request access."),
      onPressed: () => Permission.locationWhenInUse.request(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.location_on,
              size: 200.0,
              color: Colors.white54,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                getMsg(),
                style: Theme.of(context)
                    .primaryTextTheme
                    .subtitle1
                    ?.copyWith(color: Colors.white),
                softWrap: true,
                textAlign: TextAlign.justify,
              ),
            ),
            getButton(),
          ],
        ),
      ),
    );
  }
}

/// A screen displayed if the bluetooth is unavailable
class BluetoothUnavailableScreen extends StatelessWidget {
  const BluetoothUnavailableScreen({Key? key, this.state}) : super(key: key);
  final BluetoothState? state;

  String getMsg() {
    if (state == BluetoothState.turningOn)
      return "Bluetooth adapter turning on...";
    if (state == BluetoothState.unavailable)
      return "A Bluetooth adapter is required to connect to the sensor board "
          "but none available in your device.";
    if (state == BluetoothState.unauthorized)
      return "The application needs access to your Bluetooth adapter to "
          "connect to the sensor board.";
    return "Please turn on your Bluetooth adapter.";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.bluetooth_disabled,
              size: 200.0,
              color: Colors.white54,
            ),
            Text(
              getMsg(),
              style: Theme.of(context)
                  .primaryTextTheme
                  .subtitle1
                  ?.copyWith(color: Colors.white),
              softWrap: true,
              textAlign: TextAlign.center,
            ),
            Visibility(
                visible: state == BluetoothState.unauthorized,
                child: ElevatedButton(
                  child: Text("Request access."),
                  onPressed: () {/* FIXME: Request permissions. */},
                ))
          ],
        ),
      ),
    );
  }
}

/// A screen displayed while the application is scanning for sensor board
class ScanForSensorBoardScreen extends StatelessWidget {
  final _scanInProgressIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  final FlutterBlue bt;

  ScanForSensorBoardScreen({FlutterBlue? bt}) : bt = bt ?? FlutterBlue.instance;

  @override
  Widget build(BuildContext context) {
    // Immediately start scanning when screen is first displayed.
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _scanInProgressIndicatorKey.currentState?.show();
      // Disconnect from any previously bound devices.
      bt.connectedDevices.then((devices) {
        Future.wait(devices.map((d) => d.disconnect()));
      });
    });
    return Scaffold(
      appBar: AppBar(title: Text("Select your sensor board")),
      body: RefreshIndicator(
        key: _scanInProgressIndicatorKey,
        onRefresh: () => bt.startScan(timeout: Duration(seconds: 3)),
        child: StreamBuilder<List<ScanResult>>(
          stream: bt.scanResults.map((scanResults) => scanResults
              .where(
                  (result) => result.device.type != BluetoothDeviceType.unknown)
              .toList()),
          initialData: [],
          builder: (context, snapshot) => ListView(
            children: snapshot.data!
                .map((r) => BluetoothDeviceListTile(
                      r.device,
                      onChoose: (device) => Navigator.pushNamed(
                          context, SbConfigScreen.routeName,
                          arguments: device),
                    ))
                .toList(),
          ),
        ),
      ),
      floatingActionButton: StreamBuilder<bool>(
        stream: bt.isScanning,
        initialData: false,
        builder: (c, snapshot) {
          if (snapshot.data!) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: () => bt.stopScan(),
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
                child: Icon(Icons.search),
                onPressed: () =>
                    _scanInProgressIndicatorKey.currentState?.show());
          }
        },
      ),
    );
  }
}

/// A screen for the configuration of the sensor board
class SbConfigScreen extends StatefulWidget {
  static const routeName = "/bluetooth-config";
  final PlantID plantUuid;
  final BluetoothDevice device;
  final void Function(SensorBoardConfigurationResult)? onComplete;
  late final SensorBoardConnectionController _sb;

  SbConfigScreen(this.plantUuid, this.device,
      {Key? key, this.onComplete, SensorBoardConnectionController? sb})
      : super(key: key) {
    _sb = sb ?? SensorBoardConnectionController(this.plantUuid, this.device);
  }

  // Proxy SB Controller to the internal state for testing this screen.
  State<SbConfigScreen> createState() => _SbConfigScreenState();
}

class _SbConfigScreenState extends State<SbConfigScreen> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    widget._sb.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SensorBoardState>(
      stream: widget._sb.status,
      initialData: SensorBoardActionInProgress("Getting things ready..."),
      builder: (_, snapshot) {
        final state = snapshot.data!;
        if (state is SensorBoardActionInProgress)
          return SensorBoardConnectingScreen(state);
        if (state is SensorBoardConnectionFailed)
          return SensorBoardConnectionFailedScreen(state);
        if (state is SensorBoardConnectionSuccessful) {
          WidgetsBinding.instance?.addPostFrameCallback((_) {
            widget.onComplete?.call(state.result);
          });
          return SensorBoardConnectingScreen(
            SensorBoardActionInProgress("Getting things ready"),
          );
        }
        if (state is SensorBoardWiFiResults)
          return SensorBoardDisplayWiFiResultsScreen(state);
        if (state is SensorBoardHandshakingFailed)
          return SensorBoardHandshakingFailedScreen(state);
        if (state is SensorBoardConfigurationFailed)
          return SensorBoardConfigurationFailedScreen(state);
        throw Exception("Unrecognised device state: $state");
      },
    );
  }
}

typedef OnPickBluetoothDevice = void Function(BluetoothDevice device);

class BluetoothDeviceListTile extends ListTile {
  const BluetoothDeviceListTile(this.device, {Key? key, this.onChoose})
      : super(key: key);
  final BluetoothDevice device;
  final OnPickBluetoothDevice? onChoose;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: ListTile(
          title: Text(device.name),
          subtitle: Text(device.id.toString()),
          onTap: () => onChoose?.call(device),
        ),
      ),
    );
  }
}

/// A waiting screen for the connection to a sensor board
class SensorBoardConnectingScreen extends StatelessWidget {
  final SensorBoardActionInProgress state;
  const SensorBoardConnectingScreen(this.state);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Configuring your sensor board")),
      body: Center(
        child: Container(
          child: ListTile(
            leading: CircularProgressIndicator(),
            title: Text(state.title),
            subtitle:
                state.userMessage == null ? null : Text(state.userMessage!),
          ),
          padding: EdgeInsets.symmetric(horizontal: 10),
        ),
      ),
    );
  }
}

/// A screen displayed when an error occurred while connecting to a sensor board
class SensorBoardConnectionFailedScreen extends StatelessWidget {
  final SensorBoardConnectionFailed state;
  const SensorBoardConnectionFailedScreen(this.state);

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context)
        .primaryTextTheme
        .subtitle1
        ?.copyWith(color: Colors.white);
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Center(
        child: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.bluetooth_disabled),
                    title:
                        Text("Bluetooth connection failed.", style: textStyle),
                    subtitle: state.userMessage == null
                        ? null
                        : Text(state.userMessage!, style: textStyle),
                  ),
                  ListTile(
                    title: Text(
                      "The operating system in your device produced an error "
                      "that our application can do nothing about.\n\n "
                      "You can try turning your Bluetooth off and on again or "
                      "restarting your device.\n"
                      "If the problem persists, please contact your sensor "
                      "board vendor.",
                      style: textStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                ] +
                (state.technicalDetails == null
                    ? []
                    : [
                        ExpansionTile(
                            title: Text("Technical details", style: textStyle),
                            children: <Widget>[
                              Text(state.technicalDetails!, style: textStyle)
                            ]),
                      ]),
          ),
          padding: EdgeInsets.symmetric(horizontal: 20),
        ),
      ),
    );
  }
}

/// A screen for selecting and entering the information of a wifi network to
/// connect to
class SensorBoardDisplayWiFiResultsScreen extends StatelessWidget {
  final SensorBoardWiFiResults state;
  const SensorBoardDisplayWiFiResultsScreen(this.state);
  @override
  Widget build(BuildContext context) {
    StreamController<bool> isScanning = StreamController();
    final scanInProgressIndicatorKey = GlobalKey<RefreshIndicatorState>();
    final loginDialog = (ssid, ctx) {
      String password = "";
      return AlertDialog(
        title: Text("Connect to " + ssid),
        content: TextField(
          autofocus: true,
          decoration: InputDecoration(
              labelText: "Please enter password for WiFi network."),
          onChanged: (value) => password = value,
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(ctx).pop(),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
              state.onSelect(WiFiNetworkCredentials(ssid, password));
            },
            child: const Text('Connect'),
            key: const Key('Connect button'),
          ),
        ],
      );
    };
    return Scaffold(
        appBar: AppBar(title: Text("WiFi network selection")),
        body: Column(children: <Widget>[
          Container(
              decoration: BoxDecoration(boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.15),
                  spreadRadius: 1,
                  blurRadius: 1,
                )
              ]),
              child: MaterialBanner(
                  actions: [SizedBox()],
                  leading: Icon(Icons.wifi),
                  content: Text(
                    "The Sensor Board requires an internet connection. "
                    "Please select the WiFi network it will connect to.",
                    softWrap: true,
                    textAlign: TextAlign.justify,
                  ))),
          Expanded(
              flex: 1,
              child: RefreshIndicator(
                key: scanInProgressIndicatorKey,
                onRefresh: state.onRefresh,
                child: ListView(
                    children: state.networks
                        .map((e) => ListTile(
                              title: Text(e.ssid),
                              subtitle: Text(e.bssid),
                              trailing: Text(e.rssi.toString() + " dBm"),
                              onTap: () => showDialog(
                                  context: context,
                                  builder: (ctx) => (loginDialog(e.ssid, ctx))),
                            ))
                        .toList()),
              ))
        ]),
        floatingActionButton: StreamBuilder<bool>(
          stream: isScanning.stream,
          initialData: true,
          builder: (c, snapshot) {
            return Visibility(
              visible: snapshot.data ?? false,
              child: FloatingActionButton(
                  child: Icon(Icons.search),
                  onPressed: () {
                    scanInProgressIndicatorKey.currentState?.show();
                    isScanning.close();
                  }),
            );
          },
        ));
  }
}

/// A screen displayed if an error occurred during the initial talk between
/// the app and a bluetooth device
class SensorBoardHandshakingFailedScreen extends StatelessWidget {
  final SensorBoardHandshakingFailed state;
  const SensorBoardHandshakingFailedScreen(this.state);

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context)
        .primaryTextTheme
        .subtitle1
        ?.copyWith(color: Colors.white);
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
                  Icon(
                    Icons.sync_problem,
                    size: 200.0,
                    color: Colors.white54,
                  ),
                  Text(
                    "A Bluetooth connection has been successfully established "
                    "but the device does not appear to be a Greenzz-compatible "
                    "Sensor Board.",
                    softWrap: true,
                    textAlign: TextAlign.justify,
                    style: textStyle,
                  ),
                  Divider(),
                  Text(
                    "If you are certain to have selected the correct device, "
                    "We suspect your device may require a firmware upgrade.\n"
                    "Please contact your Sensor Board vendor.",
                    softWrap: true,
                    textAlign: TextAlign.justify,
                    style: textStyle,
                  ),
                  Divider(),
                ] +
                (state.userMessage == null
                    ? []
                    : [
                        Text(
                          "\nAdditional information: " + state.userMessage!,
                          style: textStyle,
                        )
                      ]) +
                (state.technicalDetails == null
                    ? []
                    : [
                        ExpansionTile(
                            title: Text("Technical details", style: textStyle),
                            children: <Widget>[
                              Text(state.technicalDetails!, style: textStyle)
                            ]),
                      ]),
          ),
        ),
      ),
    );
  }
}

/// An error screen displayed if the sensor board couldn't be configured
class SensorBoardConfigurationFailedScreen extends StatelessWidget {
  final SensorBoardConfigurationFailed state;
  const SensorBoardConfigurationFailedScreen(this.state);

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context)
        .primaryTextTheme
        .subtitle1
        ?.copyWith(color: Colors.white);
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 30),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
                  Icon(
                    Icons.do_not_disturb_on,
                    size: 200.0,
                    color: Colors.white54,
                  ),
                  Text(
                    "A Bluetooth connection has been successfully established "
                    "but we were unable to configure the Sensor Board.",
                    softWrap: true,
                    textAlign: TextAlign.justify,
                    style: textStyle,
                  ),
                  Text(
                    "We suspect your device may require a firmware upgrade.\n"
                    "Please contact your Sensor Board vendor.",
                    softWrap: true,
                    textAlign: TextAlign.justify,
                    style: textStyle,
                  ),
                ] +
                (state.userMessage == null
                    ? []
                    : [
                        Text(
                          "\nAdditional information: " + state.userMessage!,
                          style: textStyle,
                        )
                      ]) +
                (state.technicalDetails == null
                    ? []
                    : [
                        ExpansionTile(
                            title: Text("Technical details", style: textStyle),
                            children: <Widget>[
                              Text(state.technicalDetails!, style: textStyle)
                            ]),
                      ]),
          ),
        ),
      ),
    );
  }
}
