import 'dart:async';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class LocationPermissionObserver with WidgetsBindingObserver {
  static final _streamController =
      StreamController<PermissionStatus>.broadcast(onListen: feedStream);
  static Future<PermissionStatus> get _hasLocationAccess async =>
      await Permission.locationWhenInUse.status;
  Stream<PermissionStatus> get stream => _streamController.stream;

  LocationPermissionObserver._() {
    _hasLocationAccess.then((v) => _streamController.add(v));
    WidgetsBinding.instance!.addObserver(this);
  }

  static final _instance = LocationPermissionObserver._();
  static LocationPermissionObserver get instance => _instance;

  static void feedStream() =>
      _hasLocationAccess.then((v) => _streamController.add(v));

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) feedStream();
  }
}
