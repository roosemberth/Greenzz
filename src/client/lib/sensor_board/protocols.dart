import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter_blue/flutter_blue.dart';

import './rpc_commands.dart';
import '../Services/PlantService.dart';
import '../Services/RemoteServicesProvider.dart';

/// A result of a sensor board configuration
class SensorBoardConfigurationResult {
  /// Name of a sensor
  final String name;
  /// Mac address of s sensor
  final String mac;
  /// Id of a sensor
  final String sensorId;

  SensorBoardConfigurationResult(this.name, this.mac, this.sensorId);
}

/// A controller for the connection with a sensor board
class SensorBoardConnectionController {
  SensorBoardConnectionController(this._plantUuid, this._device) {
    _connectTransport();
  }

  SensorBoardConfigurationProtocol? _protocol;
  StreamSubscription? _rawSbStateSubscription;
  final PlantID _plantUuid;
  final BluetoothDevice _device;
  final _sbStateController = StreamController<SensorBoardState>();
  StreamController<BluetoothDeviceState>? _rawSbStateController;

  /// The status of the sensor board state
  Stream<SensorBoardState> get status => _sbStateController.stream;

  void _connectTransport() {
    _sbStateController.add(SensorBoardActionInProgress(
      "Connecting to sensor board",
      userMessage: "Opening an Bluetooth-LE connection with the sensor board.",
    ));
    _device.connect(autoConnect: false).onError((e, _) {
      _sbStateController
          .add(SensorBoardConnectionFailed(technicalDetails: e.toString()));
    });

    _rawSbStateController = StreamController<BluetoothDeviceState>();
    _rawSbStateSubscription = _device.state
        .distinct()
        .skipWhile((state) => state == BluetoothDeviceState.disconnected)
        .listen(_rawSbStateController?.add);
    _rawSbStateController!.stream.listen(sbStateCb);
  }

  /// Stops the controller
  void stop() {
    print("Stopping sb controller.");
    _rawSbStateSubscription?.cancel();
    _rawSbStateSubscription = null;
    _protocol?.abort();
    _protocol = null;
    _rawSbStateController?.close();
    _rawSbStateController = null;
  }

  /*
   * This performs a very precise workaround of a bug in flutter_blue and MUST
   * be called immediately after connect and before service discovery. Kudos to
   * https://github.com/pauldemarco/flutter_blue/pull/579#issuecomment-685448833
   */
  Future<int> _negociateMtu() async {
    print("Negociating MTU");
    await _device.requestMtu(251);
    final linkMtu = await _device.mtu.elementAt(1); // BLE max payload
    final payloadMtu = linkMtu - 3; // ATT header is 3 bytes.
    print("MTU: $payloadMtu");
    return payloadMtu;
  }

  /// Process the different state of the bluetooth device.
  ///
  /// If [state] is connected, search for services and configures them. If
  /// [state] is connecting or disconnecting, do nothing. If [state] is
  /// disconnected, stops the controller.
  void sbStateCb(BluetoothDeviceState state) async {
    print("BTLE: Sensor board link: $state");
    switch (state) {
      case BluetoothDeviceState.connecting:
      case BluetoothDeviceState.disconnecting:
        // These states seem to be unreliable across platforms, thus unusable...
        break;
      case BluetoothDeviceState.connected:
        final mtu = await _negociateMtu();

        final services;
        try {
          services = await _device
              .discoverServices()
              .timeout(const Duration(seconds: 3));
        } on TimeoutException {
          return disconnectAndFail("Could not discover services.");
        }

        print("Found ${services.length} services.");
        BluetoothService? uartService = findUartService(services);
        if (uartService == null)
          return disconnectAndFail("Missing UART service.");
        var mosi = findRxChar(uartService);
        if (mosi == null)
          return disconnectAndFail("Missing UART RX characteristic.");
        var miso = findTxChar(uartService);
        if (miso == null)
          return disconnectAndFail("Missing UART TX characteristic.");
        if (!await miso.setNotifyValue(true))
          return disconnectAndFail("Could not subscribe notifications.");

        print("Dispatching SensorBoardProtocolController.");
        _protocol = runZonedGuarded(
            () => SensorBoardConfigurationProtocol(
                _plantUuid,
                _device.name,
                SensorBoardProtocolAdapter.createRecvStream(miso, mtu),
                SensorBoardProtocolAdapter.createSendStream(mosi, mtu),
                _sbStateController), (e, _) {
          print("Unexpected error in protocol controller: $e, aborting...");
          _protocol?.abort();
          _sbStateController.add(SensorBoardConfigurationFailed(
              userMessage: "An error occurred communicating with the Sensor " +
                  "Board and the connection was terminated.",
              technicalDetails:
                  "Unexpected exception on transport response: $e"));
        });
        break;
      case BluetoothDeviceState.disconnected:
        print("Sb protocol closed prematurely.");
        stop();
        break;
    }
  }

  /// Disconnects the bluetooth device
  ///
  /// [msg] is a message to print to inform on the source of the disconnection
  void disconnectAndFail(String msg) {
    print("Fatal error in the SensorBoardConnectionController: $msg");
    _device.disconnect();
    _sbStateController.add(SensorBoardHandshakingFailed());
  }

  /// Returns the UART service if available, null otherwise
  ///
  /// Searches for the UART service uuid of the sensor board
  /// in the list [services].
  static BluetoothService? findUartService(List<BluetoothService> services) {
    const SERVICE_UUID = "{6e400001-b5a3-f393-e0a9-e50e24dcca9e}";
    var candidates = services.where((s) => s.uuid == Guid(SERVICE_UUID));
    return candidates.length > 0 ? candidates.first : null;
  }

  /// Returns the rx characteristic of a bluetooth service if possible,
  /// null otherwise.
  ///
  /// Searches for the rx characteristic in [service]
  static BluetoothCharacteristic? findRxChar(BluetoothService service) {
    const CHAR_RX_UUID = "{6e400002-b5a3-f393-e0a9-e50e24dcca9e}";
    var candidates = service.characteristics.where(
      (characteristic) => characteristic.uuid == Guid(CHAR_RX_UUID),
    );
    return candidates.length > 0 ? candidates.first : null;
  }

  /// Returns the rx characteristic of a bluetooth service if possible,
  /// null otherwise.
  ///
  /// Searches for the rx characteristic in [service]
  static BluetoothCharacteristic? findTxChar(BluetoothService service) {
    const CHAR_TX_UUID = "{6e400003-b5a3-f393-e0a9-e50e24dcca9e}";
    var candidates = service.characteristics.where(
      (characteristic) => characteristic.uuid == Guid(CHAR_TX_UUID),
    );
    return candidates.length > 0 ? candidates.first : null;
  }
}

/// An adapter for the sensor board protocol
class SensorBoardProtocolAdapter {
  SensorBoardProtocolAdapter._(); // Prevent instances

  /// Returns a stream to read data receive from bluetooth
  static Stream<Map<String, dynamic>> createRecvStream(
          BluetoothCharacteristic rx, final int mtu) =>
      rx.value
          .transform(StreamTransformer.fromBind((s) => unChunkStream(s, mtu)))
          .transform(utf8.decoder)
          // Cannot use json.decoder since it rechunks at random sizes.
          .transform(StreamTransformer.fromHandlers(
              handleData: (String value, EventSink<Map<String, dynamic>> sink) {
        sink.add(json.decode(value) as Map<String, dynamic>);
      }));

  /// Returns a stream to write data over bluetooth
  static StreamSink<Map<String, dynamic>> createSendStream(
      BluetoothCharacteristic tx, final int mtu) {
    StreamController<Map<String, dynamic>> sc = StreamController();
    sc.stream
        // Cannot use json.encoder since it rechunks at random sizes.
        .transform(StreamTransformer.fromHandlers(
            handleData: (value, EventSink<String> sink) {
          sink.add(json.encode(value));
        }))
        .transform(utf8.encoder)
        .listen((buf) {
          final sendAction = () => tx.write(buf);
          int retryCount = 0;
          final int maxRetryCount = 50; // Yes... 50...
          late final retry;

          retry = (e, _) {
            if (retryCount == 0)
              print("Error while sending message over BLE link " +
                  "[${buf.length} bytes]. Retrying...");

            if (retryCount > maxRetryCount) {
              print("Retry failed. Giving up...");
              throw e!;
            } else if (retryCount % 5 == 1) // Sad...
              print("Retry failed, retrying again...");

            retryCount++;

            final delayedRetry = (Future<Null> Function() action, onError) =>
                Future.delayed(Duration(milliseconds: 15), () {
                  return action().onError(onError).whenComplete(() => print(
                      "Completed BTLE TX on $retryCount/$maxRetryCount retry"));
                });
            return delayedRetry(sendAction, retry);
          };

          sendAction().onError(retry);
        });
    return sc;
  }

  static Stream<List<int>> unChunkStream(Stream<List<int>> s, int mtu) async* {
    const chunkDeadline = const Duration(seconds: 1);
    final StreamController<List<int>> sc = StreamController<List<int>>();
    s.listen(sc.add);

    Completer<Null> yieldFallbackTimer = new Completer();
    List<int> data = [];

    await for (List<int> tmp in sc.stream) {
      var recvLen = tmp.length;
      data.addAll(tmp);

      if (data.isEmpty) continue;

      yieldFallbackTimer.complete(null); // Snooze. See bellow.
      yieldFallbackTimer = new Completer();

      if (recvLen == mtu) {
        /*
         * If the transmission was the size of the payload MTU, the response
         * has likely been chunked.
         * In the unlikely event that a response was exactly the same size as
         * the payload MTU (and thus no more transmissions are expected), we
         * start a timer to force an event and yield the data received so far.
         */
        print("Received chunked response, buffering...");
        yieldFallbackTimer.future.timeout(chunkDeadline, onTimeout: () {
          print("Message chunking deadline expired. Flushing aligned buffer");
          sc.add([]);
        });
      } else {
        print("Received message of ${data.length} bytes.");
        yield data;
        data = [];
      }
    }
    sc.close(); // Unreachable, but makes compiler happy.
  }
}

/// A configuration protocol for the sensor board
class SensorBoardConfigurationProtocol {
  /// Id of the plant linked to the sensor board
  final PlantID plantUuid;
  /// Name of the sensor board
  final String deviceName;
  final StreamController<SensorBoardState> _sbStateController;
  final StreamSink<Map<String, dynamic>> _sink;
  StreamSubscription? _deviceSubscription;
  /// Api to communicate with the sensor board
  final GreenzzAPI api;

  SensorBoardConfigurationProtocol(this.plantUuid, this.deviceName,
      Stream<Map<String, dynamic>> source, this._sink, this._sbStateController)
      : api = GreenzzAPI(_sink) {
    _deviceSubscription = source.listen(api.accept);
    _start();
  }

  void _start() {
    Future<T> awaitWiFi<T>(FutureOr<T> Function(WiFiNetworkEntry) contd) {
      final timer = Timer(Duration(seconds: 10), () {});
      final completer = Completer<T>();
      late final waitWiFi;
      waitWiFi = () => Future.delayed(Duration(seconds: 1)).then((_) {
            api.wifiQuery().then((results) {
              completer.complete(contd(results));
            }, onError: (_, __) {
              if (!timer.isActive) {
                final msg = "Timeout connecting to the WiFi network.";
                print(msg);
                _sbStateController.add(
                  SensorBoardConnectionFailed(userMessage: msg),
                );
                return completer.completeError(StateError(msg));
              }
              print("Error... Retrying...");
              _sbStateController.add(SensorBoardActionInProgress(
                  "Connecting to sensor board.",
                  userMessage: "Connection is taking a little longer..."));
              return waitWiFi();
            });
          });
      return (waitWiFi() as Future<Null>).then((_) => completer.future);
    }

    final nonce = _randomAlphaNumStr(32);

    final sendInflxConfig = (String mac) async {
      _sbStateController.add(SensorBoardActionInProgress(
          "Configuring sensor board.",
          userMessage: "Linking sensor board to the database."));
      final sApi = await (await RemoteServicesProvider.instance).sensorApi;
      final registration = await sApi.registerSensor(this.plantUuid, mac);
      print("Successful registration: $registration");
      api.configureInflux(registration).then((_) {
        _sbStateController.add(SensorBoardConnectionSuccessful(
          SensorBoardConfigurationResult(
              deviceName, mac, registration.sensorId),
        ));
      });
    };
    late final chooseWiFi;
    chooseWiFi = (networks) {
      _sbStateController.add(SensorBoardWiFiResults(networks, (creds) {
        _sbStateController
             .add(SensorBoardActionInProgress("Connecting to WiFi network."));
        api.wifiConnect(creds.ssid, creds.password).then((_) async {
          final mac = await awaitWiFi((_) => api.wifiMac()).onError((e, _) {
            print("Could not connect to WiFi network.");
            abort(
                userMessage: "The sensor board could not connect to the "
                    "specified WiFi network. Verify the chosen WiFi network "
                    "and password combination and try again.");
            throw e!;
          });
          await sendInflxConfig(mac).onError((e, _) {
            print("Could not configure InfluxDB");
            abort(
                technicalDetails: "A WiFi connection was succesfully "
                    "established, but we were unable to establish a "
                    "connection to the database.");
          });
        });
      }, () {
        _sbStateController.add(SensorBoardActionInProgress(
          "Awaiting sensor board response",
          userMessage: "Sensor is scanning WiFi networks",
        ));
        return api.wifiList().then(chooseWiFi);
      }));
    };
    api.echo(nonce).then((response) {
      print("Received response: $response");
      if (response != nonce)
        return _sbStateController.add(SensorBoardConfigurationFailed());
      api.wifiDisconnect().then((_) {
        _sbStateController.add(SensorBoardActionInProgress(
          "Awaiting sensor board response",
          userMessage: "Sensor Board is scanning WiFi networks",
        ));
        return api.wifiList().then(chooseWiFi);
      });
    }).timeout(const Duration(seconds: 5), onTimeout: () {
      print("Aborted due timeout.");
      abort();
    });
  }

  /// Aborts the configuration
  void abort({String? userMessage, String? technicalDetails}) {
    _deviceSubscription?.cancel();
    _deviceSubscription = null;
    _sink.close();
    _sbStateController.add(SensorBoardConfigurationFailed(
        userMessage: userMessage, technicalDetails: technicalDetails));
  }

  String _randomAlphaNumStr(int length) {
    const alphabet =
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    final r = Random.secure();
    var ret = "";
    for (int i = 0; i < length; ++i)
      ret += alphabet[r.nextInt(alphabet.length)];
    return ret;
  }
}

class WiFiNetworkCredentials {
  final String ssid;
  final String password;
  const WiFiNetworkCredentials(this.ssid, this.password);
}

abstract class SensorBoardState {
  final String? userMessage;
  final String? logMessage;
  const SensorBoardState({this.userMessage, this.logMessage});
}

class SensorBoardActionInProgress extends SensorBoardState {
  final String title;
  const SensorBoardActionInProgress(this.title, {userMessage, logMessage})
      : super(userMessage: userMessage, logMessage: logMessage);
}

class SensorBoardConnectionFailed extends SensorBoardState {
  final String? technicalDetails;
  const SensorBoardConnectionFailed(
      {this.technicalDetails, userMessage, logMessage})
      : super(userMessage: userMessage, logMessage: logMessage);
}

class SensorBoardHandshakingFailed extends SensorBoardState {
  final String? technicalDetails;
  const SensorBoardHandshakingFailed(
      {userMessage, logMessage, this.technicalDetails})
      : super(userMessage: userMessage, logMessage: logMessage);
}

class SensorBoardWiFiResults extends SensorBoardState {
  final List<WiFiNetworkEntry> networks;
  final void Function(WiFiNetworkCredentials) onSelect;
  final Future<void> Function() onRefresh;

  const SensorBoardWiFiResults(this.networks, this.onSelect, this.onRefresh,
      {userMessage, logMessage})
      : super(userMessage: userMessage, logMessage: logMessage);
}

class SensorBoardConnectionSuccessful extends SensorBoardState {
  final SensorBoardConfigurationResult result;
  const SensorBoardConnectionSuccessful(this.result, {userMessage, logMessage})
      : super(userMessage: userMessage, logMessage: logMessage);
}

class SensorBoardConfigurationFailed extends SensorBoardState {
  final String? technicalDetails;
  const SensorBoardConfigurationFailed(
      {this.technicalDetails, userMessage, logMessage})
      : super(userMessage: userMessage, logMessage: logMessage);
}
