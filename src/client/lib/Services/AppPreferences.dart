import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

/// Preferences used by the Greenzz application
class AppPreferences {
  static final _instance = AppPreferences._();
  static Future<AppPreferences> get instance async {
    if (!_instance.waitInit.isCompleted) // instance is still initializing...
      await _instance.waitInit.future;
    return _instance;
  }

  static const String _SERVER_WS_API_URL_KEY = "server.uri.ws";
  static const String _SERVER_REST_API_URL_KEY = "server.uri.rest";
  static const String _SERVER_PRIVACY_POLICY_URL_KEY =
      "server.url.privacyPolicy";
  static const String _USER_USERNAME_KEY = "user.username";
  static const String _TUTORIAL_PLANT_PROFILE = "tutorial.plant_profile";
  Completer waitInit = Completer();

  AppPreferences._() {
    _setDefaults();
  }

  /// The URI of our server web socket API
  Future<Uri?> get serverWsApiUri async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? serverUri = prefs.getString(_SERVER_WS_API_URL_KEY);
    if (serverUri == null) return null;
    Uri? parsed = Uri.tryParse(serverUri);
    if (parsed == null) return null;
    return parsed;
  }

  Future<void> setServerWsApiUri(Uri uri) async =>
      SharedPreferences.getInstance().then((SharedPreferences prefs) async {
        await prefs.setString(_SERVER_WS_API_URL_KEY, uri.toString());
      });

  /// The URI of our server REST API
  Future<Uri?> get serverRestApiUri async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? serverUri = prefs.getString(_SERVER_REST_API_URL_KEY);
    if (serverUri == null) return null;
    Uri? parsed = Uri.tryParse(serverUri);
    if (parsed == null) return null;
    return parsed;
  }

  Future<void> setServerRestApiUri(Uri uri) async =>
      SharedPreferences.getInstance().then((SharedPreferences prefs) async {
        await prefs.setString(_SERVER_REST_API_URL_KEY, uri.toString());
      });

  /// The URI of our server REST API
  Future<String?> get serverPrivacyPolicyUrl async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final url = prefs.getString(_SERVER_PRIVACY_POLICY_URL_KEY);
    if (url?.isEmpty ?? true) return null;
    return url;
  }

  Future<void> setServerPrivacyPolicyUrl(String url) async =>
      SharedPreferences.getInstance().then((SharedPreferences prefs) async {
        await prefs.setString(_SERVER_PRIVACY_POLICY_URL_KEY, url);
      });

  /// The user name of the user connected with the application
  Future<String?> get loginUsername async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(_USER_USERNAME_KEY);
  }

  Future<void> setLoginUsername(String? username) async =>
      SharedPreferences.getInstance().then((SharedPreferences prefs) async {
        if (username == null) {
          await prefs.remove(_USER_USERNAME_KEY);
        } else {
          await prefs.setString(_USER_USERNAME_KEY, username);
        }
      });

  /// If the tutorial is enabled
  Future<bool> get plantProfileTutorialIsEnabled async =>
      SharedPreferences.getInstance().then((prefs) async {
        if (!prefs.containsKey(_TUTORIAL_PLANT_PROFILE))
          await prefs.setBool(_TUTORIAL_PLANT_PROFILE, true);
        return prefs.getBool(_TUTORIAL_PLANT_PROFILE)!;
      });

  /// Disables the tutorial launched on the first connection
  Future<void> disablePlantProfileTutorial() async =>
      SharedPreferences.getInstance().then((prefs) async {
        await prefs.setBool(_TUTORIAL_PLANT_PROFILE, false);
      });

  // TODO: How to force this values to be set at compile-time?
  Future<void> _setDefaults() async {
    final storedWsApiUri = await serverWsApiUri;
    final storedRestApiUri = await serverRestApiUri;
    final storedPrivacyPolicyUrl = await serverPrivacyPolicyUrl;

    if (storedWsApiUri != null &&
        storedRestApiUri != null &&
        storedPrivacyPolicyUrl != null) {
      waitInit.complete(null);
      return;
    }

    final vendorWsApiUri = Uri.parse(
      const String.fromEnvironment(
        "SERVER_WS_API",
        defaultValue: "wss://greenzz.orbstheorem.ch/ws",
      ),
    );
    final vendorRestApiUri = Uri.parse(
      const String.fromEnvironment(
        "SERVER_REST_API",
        defaultValue: "https://greenzz.orbstheorem.ch/",
      ),
    );
    final vendorPrivacyPolicyUrl = const String.fromEnvironment(
      "SERVER_PRIVACY_POLICY_URL",
      defaultValue:
          "https://gitlab.com/roosemberth/Greenzz/-/blob/master/reports/10-privacy-policy.md",
    );

    await setServerWsApiUri(vendorWsApiUri);
    await setServerRestApiUri(vendorRestApiUri);
    await setServerPrivacyPolicyUrl(vendorPrivacyPolicyUrl);
    waitInit.complete(null);
  }
}
