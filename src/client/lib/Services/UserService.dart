import 'RemoteServicesProvider.dart';

class UserInfluxCredentials {
  final String username;
  final String password;
  final String endpoint;
  final String database;

  UserInfluxCredentials(
      this.username, this.password, this.endpoint, this.database);

  @override
  String toString() =>
      "InfluxDB Credentials for user $username on $endpoint/$database";
}

class SensorID {
  final String str;
  SensorID(this.str);
  @override
  String toString() => "Sensor $str";
}

class UserID {
  final String str;
  UserID(this.str);
  @override
  String toString() => "User $str";
}

class UserService {
  static final _instance = UserService._();
  static UserService get instance => _instance;

  UserService._();

  Future<UserID> register(String username) async {
    return (await _userApi).register(username);
  }

  Future<UserID> resolveUsername(String username) async {
    return (await _userApi).resolveUsername(username);
  }

  Future<List<SensorID>> get sensors async {
    return (await _authUserApi).getSensors();
  }

  Future<GreenzzServerUserService> get _userApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.userApi);

  Future<GreenzzServerAuthenticatedUserService> get _authUserApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.authUserApi);
}
