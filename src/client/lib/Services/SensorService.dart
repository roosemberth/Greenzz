import 'PlantService.dart';
import 'RemoteServicesProvider.dart';

/// A class that holds the connection information to an influxDB
class SensorInfluxRegistration {
  final String sensorId;
  final String influxUser;
  final String influxPass;
  final String influxApiUrl;
  final String influxDatabase;

  SensorInfluxRegistration(this.sensorId, this.influxUser, this.influxPass,
      this.influxApiUrl, this.influxDatabase);

  @override
  String toString() {
    return "Influx config for sensor with ID $sensorId: "
        "$influxUser@$influxApiUrl/$influxDatabase";
  }
}

class SensorService {
  static final _instance = SensorService._();
  static SensorService get instance => _instance;

  SensorService._();
  /// Register on the server a new sensor with [macAddr] and associated with
  /// [plantId]. Returns the influxDB connection information.
  Future<SensorInfluxRegistration> registerSensor(
      PlantID plantId, String macAddr) async {
    return (await _sensorApi).registerSensor(plantId, macAddr);
  }

  Future<GreenzzServerSensorService> get _sensorApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.sensorApi);
}
