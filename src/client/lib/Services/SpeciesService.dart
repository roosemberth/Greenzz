import 'RemoteServicesProvider.dart';

class SpeciesPreferences {
  final String specie;
  final int minTemp;
  final int maxTemp;
  final int minLight;
  final int maxLight;
  final int minMoist;
  final int maxMoist;

  SpeciesPreferences(this.specie, this.minTemp, this.maxTemp, this.minLight,
      this.maxLight, this.minMoist, this.maxMoist);
  @override
  String toString() => "Preferences min/max : Temperature : $minTemp/$maxTemp,"
      "Light : $minLight/$maxLight, Humidity : $minMoist/$maxMoist";
}

class SpeciesDetailsService {
  static final _instance = SpeciesDetailsService._();
  static SpeciesDetailsService get instance => _instance;

  SpeciesDetailsService._();

  /// Returns a String with all the details on care for [species]
  Future<String> getSpeciesDetails(String species) async {
    return (await _speciesApi).getSpeciesDetails(species);
  }

  /// Returns the preferences for [species]
  Future<SpeciesPreferences> getSpeciesPreferences(String species) async {
    return (await _speciesApi).getSpeciesPreferences(species);
  }

  /// Returns the list of all the species available on the server's database
  Future<List<String>> getSpeciesList() async {
    return (await _speciesApi).getSpeciesList();
  }

  Future<GreenzzServerSpeciesService> get _speciesApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.speciesApi);
}

