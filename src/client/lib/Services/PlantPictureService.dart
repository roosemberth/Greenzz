import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:uuid/uuid.dart';

import '../data/PlantPhoto.dart';
import '../data/PlantProfile.dart';
import '../util/GreenzzTypeExtensions.dart';
import 'AppDirectory.dart';
import 'PlantService.dart';
import 'RemoteServicesProvider.dart';

/// A service for the photos of the plants taken by the user
class PlantPictureService {
  static final _instance = PlantPictureService._();
  static PlantPictureService get instance => _instance;

  Completer<Directory> _appDirectoryPromise = Completer();

  final Map<String, StreamController<List<PlantPhoto>>> _picturesPerPlant = {};
  final Map<PlantPhoto, Future<void>> _inflightUploads = {};
  final Map<PlantPhoto, Future<void>> _inflightDownloads = {};
  final Set<PlantPhoto> _publishedPhotosRefsCache = {};
  bool _synchronizingAllProfilesFromServer = false;

  PlantPictureService._() {
    AppDirectory.instance.getAppDir("DailyPhotos").then((directory) async {
      _appDirectoryPromise.complete(directory);
      Completer initServiceBarrier = Completer();
      PlantService.instance.stream.forEach((pps) async {
        await initServiceBarrier.future;
        _onPlantProfileStreamUpdate(pps);
      });
      await _purgeEmptyFolders(directory);
      await updateProfiles(); // FIXME: This should watch the FS
      initServiceBarrier.complete();
    });
  }

  Future<void> _onPlantProfileStreamUpdate(List<PlantProfile> plants) async {
    if (_synchronizingAllProfilesFromServer) {
      print("WARNING: Plant picture service averted concurrent update.");
      return;
    }
    print("Plant picture service received update from plant service");
    print(plants);
    _synchronizingAllProfilesFromServer = true;

    final _plantsWithPreviousPublications =
        _publishedPhotosRefsCache.map((p) => p.plantUuid).toSet();
    final _deletedPlantProfiles =
        _plantsWithPreviousPublications.difference(plants.toSet());
    if (_deletedPlantProfiles.isNotEmpty) {
      // TODO: removePlantPhotos of deleted plant profiles on a synchronous
      // transaction after confirming with the server.
      // We cannot reliably do this as of now since the PlantProfile service
      // Does not give us correct information on remote-registered plants.
    }

    final syncTask = () =>
        Future.wait(plants.map(refreshAndDownloadExistingPlantPhotosFromServer))
            .whenComplete(() => _synchronizingAllProfilesFromServer = false);

    await Future.wait(plants
            .map((p) => p.uuid)
            .toSet()
            .difference(_picturesPerPlant.keys.toSet())
            .map(_createPhotoDirAndRegisterStream))
        .whenComplete(syncTask);
  }

  Future<String> get _appDir async =>
      _appDirectoryPromise.future.then((d) => d.path);

  /// Returns a sorted list of acceptable stock photos sorted by how close
  /// they match the plant tags and species.
  List<String> getAssetStockImagesForPlant(PlantProfile plant) {
    const Map<String, List<String>> bySpecie = {
      "Rose": ["potted-rose-1.png"],
      "Lily": ["lily.png"],
      "Tulip": ["potted-tulip-1.png"],
      "Orchid": ["orchid.png"],
    };
    const Map<String, List<String>> byTag = {
      "Rose": ["potted-rose-1.png"],
      "Flower": [],
      "Garden": ["potted-4.png"],
      "Living-Room": ["potted-bonzai-1.png", "potted-1.png"],
    };

    const List<String> genericPlant = [
      "potted-1.png",
      "potted-2.png",
      "potted-3.png",
      "potted-4.png",
      "potted-bonzai-1.png",
      "potted-cactus-1.png",
      "potted-cactus-2.png",
    ];

    List<String> specieImages = bySpecie[plant.species] ?? [];
    List<String> images = specieImages +
        plant.tags.expand<String>((t) => byTag[t] ?? []).toList() +
        genericPlant;

    // Keep unique entries
    final toKeep = images.toSet();
    images.retainWhere((x) => toKeep.remove(x));

    return images
        .map((img) => "resources/stock-illustrations/pngs/$img")
        .toList();
  }

  /// Continuously returns a list of paths to the photos of the specified plant
  Stream<List<String>> photos(PlantProfile plant) =>
      _getUpdatedPhotoStream(plant.uuid).asyncMap((l) async {
        return Future.wait(l.map(_getPhotoPath));
      });

  /// Get the plant photos from the server
  Future<void> refreshAndDownloadExistingPlantPhotosFromServer(
      PlantProfile p) async {
    await _pullPublishedPhotoRefs(p.uuid);
    await _downloadMissingPhotoRefs();
    return _updatePhotoStream(p.uuid);
  }

  /// Refreshes the stream of currently available pictures
  Future<void> updateProfiles() async {
    print("Updaing plant photos streams.");
    final Set<String> foundUuids = await _storedPlantUuids;

    final Set<String> removedProfiles =
        _picturesPerPlant.keys.toSet().difference(foundUuids);
    if (removedProfiles.isNotEmpty) {
      print("Plant picture service removing plants: $removedProfiles");
      removedProfiles.map(removePlantPhotos);
    }

    final Set<String> newProfiles =
        foundUuids.difference(_picturesPerPlant.keys.toSet());
    if (newProfiles.isNotEmpty) {
      print("Plant picture service registering plants: $newProfiles");
      newProfiles.forEach(_createPhotoDirAndRegisterStream);
      // TODO: Sync with server
    }

    await Future.wait(_picturesPerPlant.keys.map(_updatePhotoStream));
  }

  Future<List<File>> photosAtDate(PlantProfile plant,
      {DateTime? atDate}) async {
    final photos = _getPlantPhotos(plant.uuid).where((photo) =>
        (atDate ?? DateTime.now()).isSameDay(DateTime.parse(photo.timestamp)));
    return photos.asyncMap(_getPhotoPath).map((path) => File(path)).toList();
  }

  /// If a photo taken in the same day exists, it is deleted.
  Future<PlantPhoto?> addDailyPhoto(
    PlantProfile plant,
    FutureOr<void> Function(String) saveFileFn,
  ) async {
    final List<File> sameDayPictures = await photosAtDate(plant);
    final photo = PlantPhoto(plant.uuid, DateTime.now().iso8601UtcDateTime);
    String filename = await _getPhotoPath(photo);
    print("Creating new daily photo: $photo.");
    File(filename).parent.createSync(recursive: true);
    await saveFileFn(filename);
    if (!await File(filename).exists()) {
      print("Callback failed to generate daily photo $filename.");
      return null;
    }

    await Future.wait(sameDayPictures.map((e) {
      print("Daily photo was replaced.");
      return removePhoto(PlantPhoto(plant.uuid, basename(e.path)));
    }));
    _publishPhotoToServer(photo);
    return _updatePhotoStream(plant.uuid).then((_) => Future.value(photo));
  }

  Future<Directory> _getPlantDirectory(String uuid) async {
    final String appDir = await _appDir;
    final dir = Directory("$appDir/$uuid");
    return dir.create();
  }

  Future<String> _getPhotoPath(PlantPhoto photo) =>
      _getPlantDirectory(photo.plantUuid)
          .then((p) => "${p.path}/${photo.timestamp}");

  Future<void> _updatePhotoStream(String plantUuid) async {
    final photos = await _getPlantPhotos(plantUuid).toList();
    photos.sort();
    final sortedPhotosCreatedDesc = photos.reversed;
    _picturesPerPlant[plantUuid]?.add(sortedPhotosCreatedDesc.toList());
  }

  Stream<PlantPhoto> _getPlantPhotos(String plantUuid) async* {
    final photoDir = await _getPlantDirectory(plantUuid);
    final photos = photoDir
        .list()
        .map((e) => basename(e.path))
        .where((e) => DateTime.tryParse(e) != null)
        .map((e) => PlantPhoto(plantUuid, e));
    yield* photos;
  }

  Stream<List<PlantPhoto>> _getUpdatedPhotoStream(String uuid) async* {
    await _updatePhotoStream(uuid);
    yield* _picturesPerPlant[uuid]!.stream;
  }

  Future<void> _createPhotoDirAndRegisterStream(String plantUuid) async {
    final Directory photoDir = await _getPlantDirectory(plantUuid);
    await photoDir.create();
    // Closed when directory updates if needed.
    // ignore: close_sinks
    _picturesPerPlant[plantUuid] = StreamController.broadcast(
      onListen: () => _updatePhotoStream(plantUuid),
    );
  }

  /// Removes all the photos of a plant
  Future<void> removePlantPhotos(String plantUuid) async {
    final photosDir = await _getPlantDirectory(plantUuid);
    _picturesPerPlant.remove(plantUuid)?.close();
    final photoFilter = (PlantPhoto photo) => photo.plantUuid == plantUuid;
    _inflightUploads.keys.where(photoFilter).map(removePhoto);
    _publishedPhotosRefsCache.where(photoFilter).map(removePhoto);
    await photosDir.delete(recursive: true);
  }

  /// Removes the [photo] from the server and the application
  Future<void> removePhoto(PlantPhoto photo) async {
    print("Removing daily photo: $photo");
    _inflightUploads[photo]?.then((_) => // Remove when it's done uploading
        _deletePublishedOrInflightPhoto(photo));
    final PlantPhoto? published = _publishedPhotosRefsCache.lookup(photo);
    if (published != null) // Remove from server, but don't wait
      _deletePublishedOrInflightPhoto(published);
    _inflightDownloads[photo]?.then((_) => // Remove when it's done downloading
        removePhoto(photo));
    final photoFile = File(await _getPhotoPath(photo));
    if (photoFile.existsSync()) {
      await photoFile.delete();
    }
  }

  Future<void> _pullPublishedPhotoRefs(String plantUuid) async {
    final published = await (await _plantApi).getPlantPhotos(plantUuid);
    _publishedPhotosRefsCache.addAll(published);
  }

  Future<void> _publishPhotoToServer(PlantPhoto photo) async {
    await _pullPublishedPhotoRefs(photo.plantUuid);
    if (_publishedPhotosRefsCache.contains(photo)) // Already published
      return;
    if (_inflightUploads.containsKey(photo)) // Publication already in progress
      return _inflightUploads[photo];
    final photoBytes = await File(await _getPhotoPath(photo)).readAsBytes();
    final upload = (await _restApi).uploadPhoto(photo, photoBytes);
    _inflightUploads[photo] = upload;
    upload.whenComplete(() {
      _inflightUploads.remove(photo);
      _publishedPhotosRefsCache.add(photo);
    });
    return upload;
  }

  Future<void> _deletePublishedOrInflightPhoto(PlantPhoto photo) async {
    if (_inflightUploads.containsKey(photo)) // Publication in progress
      return _inflightUploads[photo]
          ?.then((value) => _deletePublishedOrInflightPhoto(photo));
    _publishedPhotosRefsCache.remove(photo);
    (await _restApi).deletePhoto(photo);
  }

  Future<void> _downloadPhoto(PlantPhoto photo) async {
    final cachedInflight = _inflightDownloads[photo];
    if (cachedInflight != null) // Return cached completer
      return cachedInflight;
    Completer<void> completer = Completer();
    _inflightDownloads[photo] = completer.future;

    try {
      final photoBytes = await (await _restApi).downloadPhoto(photo);
      final photoPath = await _getPhotoPath(photo);
      await File(photoPath).writeAsBytes(photoBytes, flush: true);
      print("Saved $photo to disk.");
      completer.complete();
    } catch (e) {
      completer.completeError(e);
    } finally {
      _inflightDownloads.remove(photo);
    }
  }

  Future<void> _downloadMissingPhotoRefs() async {
    await Future.wait(_publishedPhotosRefsCache.map((photo) async {
      if (File(await _getPhotoPath(photo)).existsSync()) // We have a copy
        return;
      print("Downloading $photo from server.");
      await _downloadPhoto(photo);
    }));
  }

  Future<GreenzzServerApi> get _restApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.api);

  Future<GreenzzServerPlantService> get _plantApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.plantApi);

  Future<Set<String>> get _storedPlantUuids async {
    return Directory(await _appDir)
        .list()
        .where((entry) =>
            entry is Directory &&
            Uuid.isValidUUID(fromString: basename(entry.path)))
        .map((entry) => basename(entry.path))
        .toSet();
  }
}

Future<void> _purgeEmptyFolders(Directory directory) async {
  return directory
      .list()
      .where((e) => e is Directory)
      .map((e) => e as Directory)
      .where((Directory dir) => dir.listSync().isEmpty)
      .forEach((dir) => dir.delete());
}
