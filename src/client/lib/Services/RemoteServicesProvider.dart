import 'dart:async';
import 'dart:typed_data';

import 'package:http/http.dart' as http;
import 'package:json_rpc_2/json_rpc_2.dart';
import 'package:meta/meta.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

import '../data/PlantPhoto.dart';
import 'AppPreferences.dart';
import 'PlantService.dart';
import 'SensorService.dart';
import 'SpeciesService.dart';
import 'UserService.dart';

/// A class for the communication with the Greenzz server services
class RemoteServicesProvider {
  static final _instance = RemoteServicesProvider._();
  static Future<RemoteServicesProvider> get instance async {
    await _instance._connectTransport();
    return _instance;
  }

  RemoteServicesProvider._() {
    _tryLoginWithCachedCreds();
  }

  final inactivityDeadline = const Duration(minutes: 5);
  Client? _backingTransport;
  Completer<Null>? _inactivityTimer = Completer();
  final Set<void Function(Object?)> onDisconnect = Set();
  UserID? _loggedUserId;
  final Set<void Function()> onLogin = Set();
  final Set<void Function()> onLogout = Set();

  /// Checks with the server if [username] is valid and connects it if it is
  Future<void> logUserIn(String username) async {
    _loggedUserId = await (await userApi).resolveUsername(username);
    print("Logged succesfully as $username: $_loggedUserId");
    final appPreferences = await AppPreferences.instance;
    return await appPreferences.setLoginUsername(username).whenComplete(() {
      onLogin.forEach((cb) => cb.call());
    });
  }

  /// Log the user set in the AppPreferences out
  Future<void> logUserOut() async {
    if (_loggedUserId != null) print("Logging out $_loggedUserId");
    _loggedUserId = null;
    final appPreferences = await AppPreferences.instance;
    return appPreferences
        .setLoginUsername(null)
        .whenComplete(() => onLogout.forEach((cb) => cb.call()));
  }

  /// Returns a GreenzzServerPingService object used for pinging the server
  Future<GreenzzServerPingService> get pingApi =>
      transport.then((_) => GreenzzServerPingService(this));

  /// Returns a GreenzzServerUserService object used for calling the user
  /// services of the server
  Future<GreenzzServerUserService> get userApi =>
      transport.then((_) => GreenzzServerUserService(this));

  /// Returns a GreenzzServerAuthenticatedUserService object used for calling
  /// the authenticated user services of the server
  Future<GreenzzServerAuthenticatedUserService> get authUserApi =>
      _loggedUser.then((user) => transport
          .then((_) => GreenzzServerAuthenticatedUserService(this, user)));

  /// Returns a GreenzzServerSpeciesService object used for calling
  /// the species services of the server
  Future<GreenzzServerSpeciesService> get speciesApi =>
      transport.then((_) => GreenzzServerSpeciesService(this));

  /// Returns a GreenzzServerPlantService object used for calling
  /// the plant services of the server
  Future<GreenzzServerPlantService> get plantApi => _loggedUser.then(
      (user) => transport.then((_) => GreenzzServerPlantService(this, user)));

  /// Returns a GreenzzServerSensorService object used for calling
  /// the sensor services of the server
  Future<GreenzzServerSensorService> get sensorApi => _loggedUser.then(
      (user) => transport.then((_) => GreenzzServerSensorService(this, user)));


  Future<GreenzzServerApi> get api =>
      AppPreferences.instance.then((prefs) async {
        final apiUri = await prefs.serverRestApiUri;
        if (apiUri == null) throw ServerApiNotSet();
        final user = await _loggedUser;
        return GreenzzServerApi(apiUri, user);
      });

  void subscribeToTransportDisconnect(void Function(Object?) callback) =>
      onDisconnect.add(callback);

  void subcribeToUserLogin(void Function() cb) => onLogin.add(cb);

  void subcribeToUserLogout(void Function() cb) => onLogout.add(cb);

  Future<Client> get transport async {
    await _connectTransport();
    return _backingTransport!;
  }

  /// Forces the backing transport to be connected
  Future<void> _connectTransport() async {
    if (_backingTransport != null) // Already connected
      return _feedInactivityTimer();

    final appPreferences = await AppPreferences.instance;
    final serverUri = await appPreferences.serverWsApiUri;
    if (serverUri == null) throw ServerApiNotSet();

    print("Connecting to server: $serverUri");
    _backingTransport =
        Client(WebSocketChannel.connect(serverUri).cast<String>());
    print("Connected to server: $serverUri");

    final cleanAndNotifyAfterDisconnect = ({Object? error}) {
      _inactivityTimer?.complete(null);
      _inactivityTimer = null;
      _backingTransport = null;
      // Swap to allow listeners to resubscribe
      final callbacksToTrigger = onDisconnect.toList();
      onDisconnect.clear();
      callbacksToTrigger.forEach((f) => f.call(error));
    };

    // Never returns
    _backingTransport!.listen().then((_) {
      print("RSP transport was disconnected.");
      cleanAndNotifyAfterDisconnect();
    }).onError((Object e, __) {
      print("Could not connect websocket transport: $e");
      cleanAndNotifyAfterDisconnect(error: e);
    });

    if (_loggedUserId != null) // If we were logged in, refresh our session
      _tryLoginWithCachedCreds();

    return _feedInactivityTimer();
  }

  Future<void> _startInactivityTimer() async {
    if (_inactivityTimer != null) // Timer configured
      return;
    _inactivityTimer = Completer();
    _inactivityTimer!.future.timeout(inactivityDeadline, onTimeout: () async {
      _inactivityTimer = null;
      print("RSP: Inactivity timer expired. Disconnecting...");
      await _backingTransport?.close();
    });
  }

  Future<void> _feedInactivityTimer() {
    _inactivityTimer?.complete(null);
    _inactivityTimer = null;
    return _startInactivityTimer();
  }

  Future<UserID> get _loggedUser async {
    if (_loggedUserId != null) return Future.value(_loggedUserId);
    final username = await _username;
    if (username == null) throw UserIsNotLoggedIn();
    return (await userApi).resolveUsername(username);
  }

  Future<String?> get _username async =>
      AppPreferences.instance.then((prefs) => prefs.loginUsername);

  /// Will return whether the user is connected
  Future<bool> _tryLoginWithCachedCreds() => _username.then((username) async {
        if (username == null) return false;
        return logUserIn(username).then((_) => true);
      }).onError((e, _) {
        print("Could not log user in with cached credentials: $e");
        // Evict login state if API refused.
        return logUserOut().then((_) => false);
      });
}

class UserIsNotLoggedIn implements Exception {
  @override
  String toString() => "Exception: User is not logged in.";
}

class ServerApiNotSet implements Exception {
  @override
  String toString() => "Exception: Server API is not configured.";
}

/// A model class for using the services given by the server
abstract class GreenzzServerService {
  final RemoteServicesProvider rsp;
  Client? _backingTransport;

  GreenzzServerService(this.rsp);

  @protected
  Future<Client> get transport async {
    if (_backingTransport == null) {
      _backingTransport = await this.rsp.transport;
      this.rsp.subscribeToTransportDisconnect((_) => _backingTransport = null);
    }
    return _backingTransport!;
  }
}

class GreenzzServerPingService extends GreenzzServerService {
  GreenzzServerPingService(final rsp) : super(rsp);

  Future<String> ping() async {
    String result = await (await transport).sendRequest("ping.ping");
    print("Received ping from server!");
    return result;
  }
}

/// A class for using the user services offered by the server
class GreenzzServerUserService extends GreenzzServerService {
  GreenzzServerUserService(final rsp) : super(rsp);

  /// Returns the UserID associated to the [username] by the server during
  /// registration
  ///
  /// This register the [username] with the server
  Future<UserID> register(String username) async {
    print("Registering username: $username");
    final client = await transport;
    final response = await client.sendRequest("user.register", [username]);
    return UserID(readProperty(response, "userId"));
  }

  /// Returns the UserID registered on the server with [username]
  Future<UserID> resolveUsername(String username) async {
    print("Query username: $username");
    final client = await transport;
    final response =
        await client.sendRequest("user.resolveUsername", [username]);
    return UserID(readProperty(response, "userId"));
  }
}

/// A class for services that request to be logged in the server
class GreenzzServerAuthenticatedUserService extends GreenzzServerService {
  UserID userID;
  GreenzzServerAuthenticatedUserService(final rsp, this.userID) : super(rsp);

  /// Returns the list of SensorID associated to a given [userID]
  Future<List<SensorID>> getSensors() async {
    print("Querying sensors for $userID");
    final client = await transport;
    final response = await client.sendRequest("user.getSensors", [userID.str]);
    if (!(response is List<dynamic>))
      throw FormatException("RPC: user.getSensors didn't receive List<String>");
    return response
        .map((id) => SensorID(readProperty(id, "sensorId")))
        .toList();
  }

  /// Returns the UserInfluxCredentials associated with [plantUuid]
  Future<UserInfluxCredentials> getInfluxCredentials(String plantUuid) async {
    print("Retrieving InfluxDB credentials for $userID on ($plantUuid)");
    final client = await transport;
    final response = await client
        .sendRequest("user.getInfluxCredentials", [userID.str, plantUuid]);
    return UserInfluxCredentials(
      readProperty(response, "influxUsername"),
      readProperty(response, "influxPassword"),
      readProperty(response, "influxEndpoint"),
      readProperty(response, "influxDatabase"),
    );
  }
}

/// A class for the species related services of the server
class GreenzzServerSpeciesService extends GreenzzServerService {
  GreenzzServerSpeciesService(final rsp) : super(rsp);

  /// Returns a String with all the details on care for [species]
  Future<String> getSpeciesDetails(String species) async {
    print("Query details for : $species");
    final client = await transport;
    final response =
        await client.sendRequest("plant.getSpeciesDetails", [species]);
    return response;
  }

  /// Returns the preferences for [species]
  Future<SpeciesPreferences> getSpeciesPreferences(String species) async {
    print("Query preferences for : $species");
    final client = await transport;
    final response =
        await client.sendRequest("plant.getSpeciesPreferences", [species]);
    return SpeciesPreferences(
        species,
        readProperty(response, "minTemp"),
        readProperty(response, "maxTemp"),
        readProperty(response, "minLight"),
        readProperty(response, "maxLight"),
        readProperty(response, "minMoist"),
        readProperty(response, "maxMoist"));
  }

  /// Returns the list of all the species available on the server's database
  Future<List<String>> getSpeciesList() async {
    print("Query list of all species");
    final client = await transport;
    final response = await client.sendRequest("plant.getSpeciesList");
    if (!(response is List))
      throw FormatException("RPC: plant.getPlants didn't receive List<String>");
    return response.map((e) => e as String).toList();
  }
}

/// A class for the plant related services of the server
class GreenzzServerPlantService extends GreenzzServerService {
  UserID userID;
  GreenzzServerPlantService(final rsp, this.userID) : super(rsp);

  /// Registers the [uuid] with the server and returns the PlantID associated
  /// with it
  Future<PlantID> registerPlant(String uuid) async {
    print("Registering plant: $uuid");
    final client = await transport;
    final response =
        await client.sendRequest("plant.registerPlant", [userID.str, uuid]);
    return PlantID(readProperty(response, "uuid"));
  }

  /// Returns the PlantID associated on the server with [uuid]
  Future<PlantID> resolvePlant(String uuid) async {
    print("Resolving plant with UUID: $uuid");
    final client = await transport;
    final response = await client.sendRequest("plant.resolvePlant", [uuid]);
    return PlantID(readProperty(response, "uuid"));
  }

  /// Returns the list of PlantID associated with [userID]
  Future<List<PlantID>> getPlants(UserID userID) async {
    final client = await transport;
    final response = await client.sendRequest("plant.getPlants", [userID.str]);
    if (!(response is List<dynamic>))
      throw FormatException("RPC: plant.getPlants didn't receive List<String>");
    return response.map((id) => PlantID(readProperty(id, "uuid"))).toList();
  }

  /// Returns the list of PlantPhotos associated with [plantUuid]
  Future<List<PlantPhoto>> getPlantPhotos(String plantUuid) async {
    final client = await transport;
    final String method = "plant.getPhotoTimestamps";
    final response = await client.sendRequest(method, [plantUuid]);
    if (!(response is List<dynamic>))
      throw FormatException("RPC: $method didn't receive List<String>");
    return response.map((ts) => PlantPhoto(plantUuid, ts)).toList();
  }
}

/// A class for the sensor related services
class GreenzzServerSensorService extends GreenzzServerService {
  UserID userID;
  GreenzzServerSensorService(final rsp, this.userID) : super(rsp);

  /// Register on the server a new sensor with [macAddr] and associated with
  /// [plantId]. Returns the influxDB connection information
  Future<SensorInfluxRegistration> registerSensor(
      PlantID plantId, String macAddr) async {
    print("Registering new sensor board: $macAddr");
    final client = await transport;
    final response = await client.sendRequest(
        "sensor.registerSensor", [userID.str, plantId.str, macAddr]);
    return SensorInfluxRegistration(
      readProperty(response, "sensorId"),
      readProperty(response, "influxUser"),
      readProperty(response, "influxPass"),
      readProperty(response, "influxApiUrl"),
      readProperty(response, "influxDatabase"),
    );
  }
}

class GreenzzServerApi {
  final Uri apiUri;
  final UserID userID;

  GreenzzServerApi(this.apiUri, this.userID);

  Future<void> uploadPhoto(PlantPhoto photo, List<int> bytes) async {
    final request = http.MultipartRequest('POST', _getPlantPhotoUri(photo))
      ..files.add(http.MultipartFile.fromBytes('file', bytes, filename: '-'));
    final response = await request.send();
    if (response.statusCode != 201)
      throw Exception("Could not post daily photo $photo: "
          "${response.statusCode}");
  }

  Future<void> deletePhoto(PlantPhoto photo) async {
    final response = await http.delete(_getPlantPhotoUri(photo));
    if (response.statusCode != 204)
      throw Exception("Could not delete daily photo $photo: "
          "${response.statusCode}");
  }

  Future<Uint8List> downloadPhoto(PlantPhoto photo) async {
    final response = await http.get(_getPlantPhotoUri(photo));
    if (response.statusCode != 200)
      throw Exception("Could not retrieve daily photo $photo "
          "${response.statusCode}");
    return response.bodyBytes;
  }

  Uri _getPlantPhotoUri(PlantPhoto photo) => apiUri.resolveUri(
      Uri(path: "/dailyPhotos/${photo.plantUuid}/${photo.timestamp}"));
}

T readProperty<T>(dynamic src, property) {
  if (!(src is Map<String, dynamic>))
    throw FormatException("Specified source is not an object: $src");
  final field = src[property];
  if (field == null)
    throw FormatException("Specified source did not contain propety $property");
  if (!(field is T))
    throw FormatException("Specified source propety $property mismatched type");
  return field;
}
