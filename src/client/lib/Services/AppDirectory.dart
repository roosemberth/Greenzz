import 'dart:io';

import 'package:path_provider/path_provider.dart';

class AppDirectory {
  static final AppDirectory _instance = AppDirectory._();
  static AppDirectory get instance => _instance;

  AppDirectory._();

  Future<Directory> getAppDir(String name) async {
    Directory? storageBase;
    try {
      storageBase = await getExternalStorageDirectory();
    } catch (e) {}

    if (storageBase == null) {
      print("Could not access user-visible directory. Using fallback path.");
      storageBase = await getApplicationDocumentsDirectory();
    }
    Directory userAccesibleFolder = Directory("${storageBase.path}/$name");
    await userAccesibleFolder.create(recursive: true);
    return userAccesibleFolder;
  }
}
