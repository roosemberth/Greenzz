import 'dart:async';
import 'dart:convert' as convert;

import 'package:http/http.dart' as http;

import 'RemoteServicesProvider.dart';
import 'UserService.dart';

class LiveSensorService {
  static final Duration timerDuration = Duration(seconds: 5);
  late final StreamController<SensorData?> sc;
  final String plantUuid;
  Timer? refreshTimer;
  UserInfluxCredentials? credentials;

  LiveSensorService(this.plantUuid) {
    sc = StreamController<SensorData?>(
      onListen: () {
        print("Starting stream of live events for plant $plantUuid");
        feedStream();
      },
      onPause: () => stopTimer(),
      onResume: () => feedStream(),
      onCancel: () => stopTimer(),
    );
  }

  Stream<SensorData?> get stream => sc.stream;

  void stopTimer() {
    refreshTimer?.cancel();
    print("Stopped Live Sensor data for plant $plantUuid");
  }

  Future<Null> feedStream() async {
    if (credentials == null) {
      // Reauthenticate
      try {
        credentials = await _getInfluxCreds(plantUuid);
      } catch (_) {
        // Failure to collect credentials is acceptable.
        // The plant may not have a sensor board.
        print("Unable to retrieve $plantUuid credentials. "
            "The plant may not have a sensor board associated.");
        stopTimer();
        return null;
      }
    }

    final auth = credentials;
    if (auth == null) // Failed to log-in
      return;

    Uri uri = Uri.parse("${auth.endpoint}/query").replace(queryParameters: {
      "db": auth.database,
      "q": 'SELECT temperature,luminosity,humidity FROM '
          '"sensor-board" ORDER BY DESC LIMIT 1',
    });
    final response = (await http.get(uri)).body;
    SensorData data;
    try {
      data = _parseInfluxResponse(convert.jsonDecode(response));
    } catch (_) {
      print("Failed to decode influxdb response: '$response'");
      return;
    }
    sc.add(data);
    if (!(refreshTimer?.isActive ?? false)) {
      // Sucess
      print("Live Sensor data for plant $plantUuid will be published "
          "every $timerDuration");
      refreshTimer = Timer.periodic(timerDuration, (_) => feedStream());
    }
  }

  /// Extracts '.results[0].series[0].values[0]' from obj into a SensorData.
  SensorData _parseInfluxResponse(dynamic obj) {
    var o = obj;
    o = (o as Map<String, dynamic>)['results'];
    o = (o as List<dynamic>)[0];
    o = (o as Map<String, dynamic>)['series'];
    o = (o as List<dynamic>)[0];
    o = (o as Map<String, dynamic>)['values'];
    o = (o as List<dynamic>)[0];
    if (o[1] is int) o[1] = o[1].toDouble();
    if (o[2] is int) o[2] = o[2].toDouble();
    if (o[3] is int) o[3] = o[3].toDouble();
    return SensorData(DateTime.parse(o[0]), o[1], o[2], o[3]);
  }

  Future<UserInfluxCredentials> _getInfluxCreds(String plantUuid) async {
    final rsp = await RemoteServicesProvider.instance;
    return (await rsp.authUserApi).getInfluxCredentials(plantUuid);
  }
}

class SensorData {
  final DateTime time;
  final double? temperature;
  final double? luminosity;
  final double? soilMoisture;

  SensorData(this.time, this.temperature, this.luminosity, this.soilMoisture);

  @override
  String toString() {
    return "(SensorData: $temperature, $luminosity, $soilMoisture)";
  }
}
