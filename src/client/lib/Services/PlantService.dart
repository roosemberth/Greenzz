import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart';
import 'package:uuid/uuid.dart';

import '../data/PlantProfile.dart';
import 'AppDirectory.dart';
import 'PlantPictureService.dart';
import 'RemoteServicesProvider.dart';

class PlantID {
  final String str;
  PlantID(this.str);

  @override
  String toString() {
    return "PlantID $str";
  }
}

/// Service to manage plant profiles
class PlantService {
  static final _instance = PlantService._();
  static PlantService get instance => _instance;

  final List<PlantProfile> _plantProfiles = [];
  final Map<String, PlantID> _registeredPlants = Map<String, PlantID>();
  final Map<String, Future<PlantID>> _inflightRegistrations =
      Map<String, Future<PlantID>>();

  late final StreamController<List<PlantProfile>> _streamController;

  PlantService._() {
    _streamController =
        StreamController<List<PlantProfile>>.broadcast(onListen: feedStream);
    PersistedPlantProfiles.instance; // Forces link & start of the service.
    RemoteServicesProvider.instance.then(
      (rsp) => rsp.subcribeToUserLogout(() {
        _inflightRegistrations.clear();
        _registeredPlants.clear();
      }),
    );
  }

  Stream<List<PlantProfile>> get stream => _streamController.stream;

  void addPlantProfile(PlantProfile profile) => addPlantProfiles({profile});

  void addPlantProfiles(Set<PlantProfile> pps) {
    pps.forEach((PlantProfile newPp) {
      final idx = _plantProfiles.indexWhere((pp) => pp.uuid == newPp.uuid);
      if (idx < 0) {
        // Plant profile not registered, add.
        _plantProfiles.add(newPp);
      } else {
        // Plant profile is known, replace it.
        if (_plantProfiles[idx].lastModified.isBefore(newPp.lastModified))
          _plantProfiles.replaceRange(idx, idx + 1, [newPp]);
      }
    });
    _plantProfiles.sort((a, b) => a.created.compareTo(b.created));
    // Feed immediately since registering with the server sholdn't change
    // the stream. Prevents awaiting the server for a really long time.
    feedStream();
    pps.forEach((profile) {
      if (_inflightRegistrations[profile.uuid] == null &&
          _registeredPlants[profile.uuid] == null)
        _registerProfile(profile.uuid); // TODO: Publish profile data
    });
  }

  Future<void> removePlantProfile(PlantProfile profile) async {
    _plantProfiles.removeWhere((e) => e.uuid == profile.uuid);
    PlantPictureService.instance.removePlantPhotos(profile.uuid);
    PersistedPlantProfiles.instance.removePlantProfile(profile.uuid);
    // TODO: Schedule for deletion in server
    feedStream();
  }

  /// Returns the PlantID associated to the [profile]
  Future<PlantID> getRegisteredPlantId(PlantProfile profile) {
    final inflight = _inflightRegistrations[profile.uuid];
    if (inflight != null) return inflight;
    final registered = _registeredPlants[profile.uuid];
    if (registered != null) return Future.value(registered);
    addPlantProfile(profile);
    return getRegisteredPlantId(profile);
  }

  void feedStream() => _streamController.add(_plantProfiles);

  Future<PlantID> _registerProfile(String uuid) {
    if (_registeredPlants[uuid] != null)
      return Future.value(_registeredPlants[uuid]);
    if (_inflightRegistrations[uuid] != null)
      return _inflightRegistrations[uuid]!;
    _inflightRegistrations[uuid] = _plantApi.then((api) async {
      return api.registerPlant(uuid).onError((_, __) => api.resolvePlant(uuid));
    });
    return _inflightRegistrations[uuid]!.then((id) {
      _registeredPlants[uuid] = id;
      return id;
    }, onError: _handleRegistrationError).whenComplete(
      () => _inflightRegistrations.remove(uuid),
    );
  }

  void _handleRegistrationError(e) {
    print("Error registering plant with server: $e");
    // TODO: What to do?
  }

  Future<GreenzzServerPlantService> get _plantApi =>
      RemoteServicesProvider.instance.then((rsp) => rsp.plantApi);
}

/// A class for persisting plant profiles
class PersistedPlantProfiles {
  static final _instance = PersistedPlantProfiles._();
  static PersistedPlantProfiles get instance => _instance;

  Completer<Directory> _storage = Completer();

  PersistedPlantProfiles._() {
    AppDirectory.instance.getAppDir("PlantProfiles").then((directory) async {
      _storage.complete(directory);
      // Listen first to avoid missing any updates.
      PlantService.instance.stream.forEach(_onPlantProfileStreamUpdate);
      // This implies we'll be called with out own updates. This is okay.
      PlantService.instance.addPlantProfiles((await storedProfiles).toSet());
    });
  }

  Future<List<PlantProfile>> get storedProfiles async {
    final uuids = (await _storedPlantUuids).entries.toList()
      ..sort((a, b) => a.value.compareTo(b.value));
    return Stream.fromIterable(uuids)
        .map((e) => e.key)
        .asyncMap(_readProfile)
        .where((e) => e != null)
        .cast<PlantProfile>()
        .toList();
  }

  /// Removes the specified plant profile if it exists.
  Future<void> removePlantProfile(String uuid) async {
    File file = File("${await _appDir}/$uuid");
    if (!await file.exists()) return;
    await file.delete();
  }

  Future<void> _onPlantProfileStreamUpdate(List<PlantProfile> pps) async {
    final stored = await _storedPlantUuids;
    final newProfiles = pps.where((pp) => !stored.containsKey(pp.uuid)).toSet();
    final updated =
        pps.where((pp) => stored[pp.uuid]?.isBefore(pp.lastModified) ?? false);
    // Explicitly ignore any missing profiles.
    if (newProfiles.isNotEmpty) {
      print("Persisting new profiles: ${newProfiles.map((p) => p.uuid)}");
      await Stream.fromIterable(newProfiles).asyncMap(_writeProfile).toSet();
    }
    if (updated.isNotEmpty) {
      print("Updating profiles: ${updated.map((p) => p.uuid)}");
      await Stream.fromIterable(updated).asyncMap(_writeProfile).toSet();
    }
  }

  Future<String> get _appDir async => _storage.future.then((d) => d.path);

  Future<Map<String, DateTime>> get _storedPlantUuids async {
    final entries = Directory(await _appDir)
        .list()
        .where((entry) => entry is File)
        .cast<File>()
        .where((e) => Uuid.isValidUUID(fromString: basename(e.path)))
        .asyncMap((entry) async {
      return MapEntry(basename(entry.path), await entry.lastModified());
    });
    return Map.fromEntries(await entries.toList());
  }

  Future<void> _writeProfile(PlantProfile profile) async {
    print("Persisting plant profile with uuid ${profile.uuid}");
    File file = File("${await _appDir}/${profile.uuid}");
    file = await file.writeAsString(jsonEncode(profile.toJson()), flush: true);
    await file.setLastModified(profile.lastModified);
  }

  Future<PlantProfile?> _readProfile(String uuid) async {
    print("Reading plant profile with uuid $uuid from fs.");
    File file = File("${await _appDir}/$uuid");
    try {
      return PlantProfile.fromJson(jsonDecode(await file.readAsString()));
    } catch (_) {
      if (await file.exists()) {
        print("Failed to read plant profile $uuid from fs. Deleting...");
        await file.delete();
      }
      return null;
    }
  }
}
