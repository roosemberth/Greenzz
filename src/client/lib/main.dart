import 'package:flutter/material.dart';

import './theme.dart';
import 'UI/Routes.dart';

void main() => runApp(
      MaterialApp(
        title: "Greenzz",
        initialRoute: ApplicationRoutes.PlantList,
        theme: greenzzTheme,
        routes: applicationRoutes,
      ),
    );
