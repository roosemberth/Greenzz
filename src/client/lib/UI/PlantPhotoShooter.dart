// A screen that takes in a list of cameras and the Directory to store images.
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_editor/image_editor.dart';

class TakePlantDailyPictureScreen extends StatefulWidget {
  final Completer<List<CameraDescription>> cameras = Completer();
  final File destImage;

  TakePlantDailyPictureScreen(this.destImage, {Key? key}) : super(key: key) {
    // Ensure that plugin services are initialized so that `availableCameras()`
    // can be called before `runApp()`
    WidgetsFlutterBinding.ensureInitialized();

    availableCameras().then((availableCameras) {
      if (availableCameras.isEmpty)
        return cameras.completeError("Could not find any cameras.");
      cameras.complete(availableCameras);
    });
  }

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

enum Rotation { Rot0, Rot90, Rot180, Rot270 }

extension Rotate on Rotation {
  Rotation get cw {
    final values = Rotation.values;
    return values.elementAt((this.index + 1) % values.length);
  }

  Rotation get ccw {
    final values = Rotation.values;
    return values.elementAt((this.index - 1) % values.length);
  }
}

class TakePictureScreenState extends State<TakePlantDailyPictureScreen> {
  // Add two variables to the state class to store the CameraController and
  // the Future.
  CameraController? _controller;
  Future<void>? _initializeControllerFuture;
  final GlobalKey<ExtendedImageEditorState> _editorKey =
      GlobalKey<ExtendedImageEditorState>();
  Uint8List? _memoryImage;
  bool _isCapturing = false;
  bool _isCropping = false;
  Rotation _rotation = Rotation.Rot0; // Workaround extended_image#277

  @override
  void initState() {
    super.initState();
    // To display the current output from the camera,
    // create a CameraController.
    widget.cameras.future.then((cameras) {
      setState(() {
        _controller = CameraController(cameras.first, ResolutionPreset.max);
        _initializeControllerFuture = _controller!.initialize();
      });
    });
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_initializeControllerFuture == null) return waitingForCameraScreen();

    return FutureBuilder<void>(
      future: _initializeControllerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState != ConnectionState.done)
          return waitingForCameraScreen();

        if (_memoryImage == null) return _takePictureWidget();

        return _buildCroppingImage(_memoryImage!);
      },
    );
  }

  Widget waitingForCameraScreen() {
    final t = Theme.of(context);
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: Theme(
        data: ThemeData(accentColor: t.primaryTextTheme.bodyText1?.color),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const Icon(
                Icons.camera,
                size: 200.0,
                color: Colors.white54,
              ),
              Text(
                "Initializing camera...",
                softWrap: true,
                textAlign: TextAlign.center,
                style: t.primaryTextTheme.headline6,
              ),
              SizedBox(height: 10),
              CircularProgressIndicator(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCroppingImage(Uint8List photo) {
    final imageEditor = ExtendedImage.memory(
      photo,
      extendedImageEditorKey: _editorKey,
      cacheRawData: true,
      fit: BoxFit.contain,
      mode: ExtendedImageMode.editor,
      enableLoadState: true,
      initEditorConfigHandler: (state) {
        WidgetsBinding.instance?.addPostFrameCallback((_) {
          _editorKey.currentState?.setState(() { // Single transaction
            for (var i = 0; i < _rotation.index; ++i)
              _editorKey.currentState?.rotate();
          });
        });
        return EditorConfig(
          maxScale: 8.0,
          cropRectPadding: EdgeInsets.zero,
          hitTestSize: 20.0,
          initCropRectType: InitCropRectType.layoutRect,
          cropAspectRatio: _rotation.index % 2 == 0
              ? CropAspectRatios.ratio4_3
              : CropAspectRatios.ratio3_4,
        );
      },
    );

    final validateButton = IconButton(
      icon: Icon(Icons.check_circle),
      onPressed: () {
        if (_isCropping) return;
        setState(() => _isCropping = true);
        _cropImage().then((img) {
          setState(() => _isCropping = false);
          Navigator.of(context).pop(img);
        });
      },
    );
    final actionButton =
        _isCropping ? CircularProgressIndicator() : validateButton;
    return Scaffold(
      appBar: AppBar(actions: [actionButton]),
      body: imageEditor,
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            TextButton(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Icon(Icons.refresh),
                Text("Retake"),
              ]),
              onPressed: () => setState(() => _memoryImage = null),
            ),
            TextButton(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Icon(Icons.rotate_left),
                Text("Rotate left"),
              ]),
              onPressed: () => setState(() => _rotation = _rotation.ccw),
            ),
            TextButton(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Icon(Icons.rotate_right),
                Text("Rotate right"),
              ]),
              onPressed: () => setState(() => _rotation = _rotation.cw),
            ),
            TextButton(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Icon(Icons.flip),
                Text("Flip"),
              ]),
              onPressed: () => _editorKey.currentState?.flip(),
            ),
          ],
        ),
      ),
    );
  }

  Future<File?> _cropImage() async {
    print("Cropping image.");
    final cropState = _editorKey.currentState;
    final Rect? cropRect = cropState?.getCropRect();
    final EditActionDetails? action = cropState?.editAction;

    if (cropState == null || cropRect == null || action == null) {
      print("Warn: Invalid crop data $cropState, $cropRect, $action.");
      return null;
    }

    ImageEditorOption option = ImageEditorOption();

    if (action.needCrop) {
      option.addOption(ClipOption.fromRect(cropRect));
    }

    if (action.needFlip) {
      final flip = FlipOption(horizontal: action.flipY, vertical: action.flipX);
      option.addOption(flip);
    }

    if (action.hasRotateAngle)
      option.addOption(RotateOption(action.rotateAngle.toInt()));

    final result = await ImageEditor.editImage(
      image: cropState.rawImageData,
      imageEditorOption: option,
    );

    if (result == null) return null;

    print("Writing cropped image to ${widget.destImage.path}");
    await widget.destImage.writeAsBytes(result.toList(), flush: true);
    return widget.destImage;
  }

  Widget _takePictureWidget() {
    return Scaffold(
      body: CameraPreview(_controller!),
      floatingActionButton: _isCapturing
          ? CircularProgressIndicator()
          : FloatingActionButton(
              child: Icon(Icons.camera_alt),
              onPressed: () async {
                if (_isCapturing) return;
                setState(() {
                  _isCapturing = true;
                });
                await _controller!.takePicture().then((img) async {
                  img.readAsBytes().then((imgBytes) {
                    setState(() {
                      _memoryImage = imgBytes;
                      _isCapturing = false;
                    });
                  });
                });
              },
            ),
    );
  }
}
