import 'dart:io';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

import '../Services/PlantPictureService.dart';
import '../Services/PlantService.dart';
import '../data/PlantProfile.dart';
import '../util/GreenzzTypeExtensions.dart';
import 'DrawerMenu.dart';
import 'PlantProfile.dart';
import 'Routes.dart';

/// A screen where all the plant profiles of the user are displayed
class PlantListScreen extends StatelessWidget with WidgetsBindingObserver {
  final PlantService plantService = PlantService.instance;
  final PlantPictureService plantPicService = PlantPictureService.instance;

  @override
  Widget build(BuildContext context) {
    final AppBar appBar = AppBar(
      automaticallyImplyLeading: true,
      title: Text("My plants"),
      actions: [
        IconButton(
          icon: const Icon(Icons.library_add_rounded),
          onPressed: () => _createPlantProfile(context),
        )
      ],
    );

    final firstPlantProfileWidget = Center(
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(12),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(12),
                  child: DefaultTextStyle(
                    style: Theme.of(context).textTheme.headline6!,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: "A greener future ",
                            style: Theme.of(context).textTheme.headline6!,
                            //children: [TextSpan("A greener future ")],
                          ),
                        ),
                        AnimatedTextKit(
                          totalRepeatCount: 1,
                          pause: Duration(milliseconds: 500),
                          animatedTexts: [
                            "awaits...",
                            "is here!",
                            "is in your hands!",
                          ].map((t) {
                            return TypewriterAnimatedText(t,
                                speed: Duration(milliseconds: 100));
                          }).toList(),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "Track your plants and see them grow!",
                  style: Theme.of(context).textTheme.bodyText2?.copyWith(
                        color: Colors.grey,
                      ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.all(12),
                  child: InputChip(
                    backgroundColor: Theme.of(context).primaryColorDark,
                    avatar: Icon(Icons.add_circle, color: Colors.white),
                    label: Text(
                      "Add my first plant",
                      style: Theme.of(context).primaryTextTheme.bodyText2,
                    ),
                    onPressed: () => _createPlantProfile(context),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    final boundCard = (Card c) => Container(height: 170, width: 170, child: c);

    plantService.feedStream();
    return Scaffold(
      appBar: appBar,
      drawer: DrawerMenu(title: "Drawer"),
      body: StreamBuilder<List<PlantProfile>>(
        stream: plantService.stream,
        builder: (context, snapshot) {
          final plantProfiles = snapshot.data;
          if (plantProfiles == null || plantProfiles.isEmpty)
            return firstPlantProfileWidget;
          final children = plantProfiles
              .map((p) => _toCard(p, context))
              .map(boundCard)
              .toList();
          return SizedBox(
            width: double.infinity,
            child: Center(
              child: Wrap(children: children, alignment: WrapAlignment.start),
            ),
          );
        },
      ),
    );
  }

  static void _createPlantProfile(BuildContext context) async {
    final PlantService plantService = PlantService.instance;
    final result = await Navigator.pushNamed(
        context, ApplicationRoutes.PlantProfileCreate);
    if (!(result is PlantProfile?))
      throw StateError("Plant create activity did not return a PlantProfile.");
    if (result == null) // User aborted
      return;
    PlantProfile profile = result;

    plantService.addPlantProfile(profile);
    await Navigator.pushNamed(context, ApplicationRoutes.PlantProfile,
        arguments: profile);
    plantService.feedStream();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) PlantService.instance.feedStream();
  }

  Card _toCard(PlantProfile p, BuildContext context) {
    final plantImage = (context, AsyncSnapshot<List<String>> snapshot) {
      final List<String>? photos = snapshot.data;
      if (photos == null) return Center(child: CircularProgressIndicator());
      final ImageProvider imageSrc = photos.isNotEmpty
          ? FileImage(File(photos.first))
          : AssetImage(plantPicService.getAssetStockImagesForPlant(p).first)
              as ImageProvider;
      return Image(image: imageSrc);
    };

    final namePillStyle = Theme.of(context).primaryTextTheme.headline6;

    final plantNamePill = (context, AsyncSnapshot<List<String>> snapshot) {
      final shouldTakeDaily = () {
        List<String>? photos = snapshot.data;
        if (photos == null) return false;
        if (photos.isEmpty) return true;
        final lastPicDate = DateTime.tryParse(basename(photos.first));
        return !DateTime.now().isSameDay(lastPicDate);
      };
      final Widget quickAddPhoto = IconButton(
        icon: Icon(
          Icons.add_a_photo_rounded,
          color: Colors.white,
        ),
        onPressed: () => addDailyPhoto(p, context),
      );
      return ClipRRect(
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(4),
          bottomLeft: Radius.circular(4),
        ),
        child: Container(
          width: 170,
          color: Theme.of(context).primaryColor,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Text(p.name, style: namePillStyle),
                ),
                (shouldTakeDaily() ? quickAddPhoto : SizedBox()),
              ],
            ),
          ),
        ),
      );
    };

    return Card(
      child: StreamBuilder<List<String>>(
        stream: plantPicService.photos(p),
        builder: (ctx, snapshot) => GestureDetector(
          child: Column(
            children: <Widget>[
              Expanded(flex: 3, child: plantImage(ctx, snapshot)),
              Expanded(child: plantNamePill(ctx, snapshot)),
            ],
          ),
          onTap: () {
            print("Opening profile for plant ${p.uuid}");
            Navigator.pushNamed(context, ApplicationRoutes.PlantProfile,
                arguments: p);
          },
        ),
      ),
    );
  }
}
