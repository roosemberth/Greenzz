import 'dart:async';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../Services/SpeciesService.dart';
import '../data/PlantProfile.dart';
import '../data/PlantStatus.dart';

/// Screen for the creation of a plant profile
class PlantProfileCreationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PlantProfileCreationScreenState();
  }
}

class _PlantProfileCreationScreenState
    extends State<PlantProfileCreationScreen> {
  final _formKey = GlobalKey<FormState>();
  StreamController<List<String>?>? knownPlantSpeciesSc;
  Timer? scheduledRetry;

  String? name;
  int? age;
  PlantStatus? status;
  String? specie;

  @override
  void dispose() {
    scheduledRetry?.cancel();
    knownPlantSpeciesSc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData t = Theme.of(context);
    knownPlantSpeciesSc = StreamController(onListen: updateKnownSpecies);
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColorDark,
      body: Theme(
        data: Theme.of(context).copyWith(
          backgroundColor: t.primaryColorDark,
          inputDecorationTheme: t.inputDecorationTheme.copyWith(
            labelStyle: t.primaryTextTheme.bodyText1,
            hintStyle: t.primaryTextTheme.caption,
            suffixStyle: t.inputDecorationTheme.suffixStyle
                ?.copyWith(color: t.primaryTextTheme.bodyText1?.color),
            fillColor: t.primaryColor,
            errorStyle: t.inputDecorationTheme.errorStyle?.copyWith(
              color: Color(0xfff58549), // "Mango tango"
              fontSize: t.textTheme.bodyText1?.fontSize,
              shadows: [BoxShadow(color: Colors.white, blurRadius: 1)],
            ),
          ),
          textSelectionTheme: t.textSelectionTheme.copyWith(
            selectionColor: t.disabledColor,
          ),
          textTheme: t.primaryTextTheme,
        ),
        child: Center(
          child: Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 30),
              shrinkWrap: true,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 60, bottom: 10),
                  child: Text(
                    "Let's get to know your plant!",
                    style: Theme.of(context).primaryTextTheme.headline6,
                  ),
                ),
                TextFormField(
                  autofocus: true,
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    labelText: "Give your plant a name",
                    hintText: "e.g. 'Minnie' or 'Dalfodil'",
                  ),
                  style: Theme.of(context).primaryTextTheme.bodyText1,
                  validator: (v) =>
                      v?.isEmpty ?? true ? "Field is required" : null,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  onSaved: (v) => name = v,
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  decoration: InputDecoration(
                    //labelText: "An estimate of its age",
                    labelText: "How many weeks old is your plant?",
                    border: OutlineInputBorder(),
                    suffixText: "Weeks",
                  ),
                  style: Theme.of(context).primaryTextTheme.bodyText1,
                  validator: (v) {
                    if (v?.isEmpty ?? true) return "Field is required";
                    if (int.tryParse(v!) == null)
                      return "Field must be a number";
                  },
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  onSaved: (v) => age = int.parse(v!),
                ),
                DropdownSearch<String>(
                  mode: Mode.MENU,
                  label: "How is your plant looking?",
                  items: PlantStatus.values.map(toPrettyString).toList(),
                  validator: (v) =>
                      fromPrettyString(v) == null ? "Field is required" : null,
                  popupBackgroundColor: Theme.of(context).primaryColor,
                  autoValidateMode: AutovalidateMode.onUserInteraction,
                  onSaved: (v) => status = fromPrettyString(v),
                ),
                StreamBuilder<List<String>?>(
                  stream: knownPlantSpeciesSc?.stream,
                  builder: (context, snapshot) {
                    final data = snapshot.data;
                    if (data == null)
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: Column(
                          children: <Widget>[
                            Text("Retrieving available species...",
                                style: t.primaryTextTheme.bodyText1),
                            SizedBox(height: 4),
                            LinearProgressIndicator(),
                          ],
                        ),
                      );
                    return plantList(snapshot.data);
                  },
                ),
                SizedBox(height: 10),
                ElevatedButton(
                  child: Text("Continue"),
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) return;
                    _formKey.currentState!.save();
                    if (specie == null) {
                      // Known species are still loading.
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("We are still retrieving the "
                            "list of species known by the server. "
                            "It shouldn't take much longer!"),
                      ));
                      return;
                    }
                    final estBirthday =
                        DateTime.now().subtract(Duration(days: age! * 7));
                    final profile =
                        PlantProfile(name!, estBirthday, status!, specie!);
                    Navigator.of(context).pop(profile);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  /// Gets the list of all species
  void updateKnownSpecies() {
    SpeciesDetailsService.instance
        .getSpeciesList()
        .then((list) => knownPlantSpeciesSc?.add(list), onError: (_) {
      print("Failed to retrieve list of known species.");
      scheduledRetry = Timer(Duration(seconds: 2), updateKnownSpecies);
    });
  }

  /// Returns the widget that asks for the species of the plant being registered
  Widget plantList(List<String>? plants) {
    return DropdownSearch<String>(
      mode: Mode.DIALOG,
      label: "What species is your plant of?",
      items: plants,
      showSearchBox: true,
      validator: (v) => v == null ? "Field is required" : null,
      popupBackgroundColor: Theme.of(context).primaryColor,
      autoValidateMode: AutovalidateMode.onUserInteraction,
      onSaved: (v) => specie = v,
    );
  }
}
