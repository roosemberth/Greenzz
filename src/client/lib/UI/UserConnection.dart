import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../Services/AppPreferences.dart';
import '../Services/RemoteServicesProvider.dart';

class UserConnectionDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _UserConnectionDialogState();
}

class _UserConnectionDialogState extends State<UserConnectionDialog>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;
  final _loginFormKey = GlobalKey<FormState>();
  final _registerFormKey = GlobalKey<FormState>();
  String? _loginUsernameError;
  String? _registerUsernameError;

  bool _apiBusy = false;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.zero,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Center(
          child: Container(
            constraints: BoxConstraints(maxHeight: 300),
            height: 300,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0),
                  blurRadius: 6.0,
                ),
              ],
            ),
            child: Column(children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                child: Container(
                  color: t.primaryColor,
                  child: TabBar(
                    controller: _tabController,
                    tabs: [
                      Tab(text: "Login"),
                      Tab(text: "Register"),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    getLoginContainer(),
                    getregisterContainer(),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  Widget getLoginContainer() {
    String? username;
    final loginButton = ElevatedButton(
      child: Row(mainAxisSize: MainAxisSize.min, children: [Text("Login")]),
      onPressed: () async {
        if (!_loginFormKey.currentState!.validate()) return;
        _loginFormKey.currentState!.save();
        setState(() => _apiBusy = true);
        (await RemoteServicesProvider.instance)
            .logUserIn(username!)
            .then(Navigator.of(context).pop, onError: (e) {
          print("Error: $e");
          setState(() {
            _loginUsernameError = e.toString();
            _apiBusy = false;
          });
        });
      },
    );
    return Center(
      child: Form(
        key: _loginFormKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 20),
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(
                labelText: "Username",
                errorText: _loginUsernameError,
                errorStyle:
                    t.textTheme.bodyText1?.copyWith(color: t.errorColor),
              ),
              validator: (v) => v?.isEmpty ?? true ? "Field is required" : null,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              onSaved: (v) => username = v,
            ),
            SizedBox(height: 4),
            ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: Container(
                  color: Colors.grey,
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: " Password validation is disabled",
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.zero,
                    ),
                    enabled: false,
                  ),
                )),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                _apiBusy ? CircularProgressIndicator() : loginButton
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getregisterContainer() {
    String? username;
    final doRegister = (String username) async {
      setState(() => _apiBusy = true);
      final rsp = await RemoteServicesProvider.instance;
      await (await rsp.userApi).register(username);
      return rsp.logUserIn(username);
    };
    final registrationButton = ElevatedButton(
      child: Row(mainAxisSize: MainAxisSize.min, children: [Text("Register")]),
      onPressed: () async {
        if (!_registerFormKey.currentState!.validate()) // Form is not valid
          return;
        _registerFormKey.currentState!.save();
        return doRegister(username!).then(Navigator.of(context).pop,
            onError: (e) {
          print("Error: $e");
          setState(() {
            _registerUsernameError = e.toString();
            _apiBusy = false;
          });
        });
      },
    );
    return Center(
      child: Form(
        key: _registerFormKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 20),
          children: <Widget>[
            SizedBox(height: 30),
            TextFormField(
              decoration: InputDecoration(
                labelText: "Username",
                errorText: _registerUsernameError,
                errorStyle:
                    t.textTheme.bodyText1?.copyWith(color: t.errorColor),
              ),
              validator: (v) => v?.isEmpty ?? true ? "Field is required" : null,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              onSaved: (v) => username = v,
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                _apiBusy ? CircularProgressIndicator() : registrationButton,
              ],
            ),
            SizedBox(height: 20),
            RichText(
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: "Take a moment to review our ",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                TextSpan(
                  text: "Privacy Policy",
                  recognizer: TapGestureRecognizer()
                    ..onTap = () async {
                      final prefs = await AppPreferences.instance;
                      final privacyPolicyUrl =
                          await prefs.serverPrivacyPolicyUrl;
                      if (privacyPolicyUrl != null) {
                        if (await canLaunch(privacyPolicyUrl)) {
                          await launch(privacyPolicyUrl);
                          return;
                        }
                      }
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          "Could not open the privacy policy link. "
                          "Please consult the project homepage.",
                        ),
                      ));
                    },
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1
                      ?.copyWith(color: Colors.blue),
                ),
                TextSpan(
                  text: ".",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ]),
            ),
          ],
        ),
      ),
    );
  }

  ThemeData get t => Theme.of(context);
}
