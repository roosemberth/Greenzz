import 'dart:async';

import 'package:flutter/material.dart';

import '../Services/RemoteServicesProvider.dart';
import 'UserConnection.dart';

final Map<Type, Future<bool> Function(BuildContext)> exceptionHandlers = {
  UserIsNotLoggedIn: (context) async {
    print("Handling 'UserIsNotLoggedIn': Will display brief explanation.");
    final bool? proceed = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("Please login to continue"),
        content: Text(
          "This functionality requires you to be logged in.",
          textAlign: TextAlign.justify,
        ),
        actions: [
          TextButton(
            child: Text("Cancel"),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text("Continue"),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
        ],
      ),
    );
    if (!(proceed ?? false)) {
      print("Handling 'UserIsNotLoggedIn': User refused login page.");
      return false;
    }
    print("Handling 'UserIsNotLoggedIn' will display login dialog.");
    await showDialog(context: context, builder: (_) => UserConnectionDialog());
    return true;
  },
};

/// Displays config screens that may be necessary for the future to complete.
///
/// Runs the future and in case of error, it inspects the error.
/// If the error is known and can be corrected by reconfiguring a service,
/// such screen will be displayed to the user.
/// Upon returning from the view, the future will be retried until all errors
/// are cleared and the future produces a result.
/// If the same error is encountered multiple times, it will be rethrown.
FutureOr<T> autoDisplayServiceConfigScreens<T>(
    BuildContext context, FutureOr<T> Function() future,
    {void Function(Object)? onError}) async {
  Set seenExceptions = Set();
  final giveUpFn = (e) {
    onError?.call(e);
    throw e;
  };
  while (true) {
    try {
      return await future();
    } catch (e) {
      final type = e.runtimeType;
      if (seenExceptions.contains(type)) // Tried and failed... give up.
        giveUpFn(e);
      seenExceptions.add(type);
      final handler = exceptionHandlers[type];
      if (handler == null) {
        print("Future threw unknown exception $e.");
        giveUpFn(e);
      }
      if (!await handler(context)) // Handler failed.
        giveUpFn(e);
    }
  }
}
