import 'package:flutter/material.dart';
import 'package:greenzz/Services/AppPreferences.dart';
import 'package:greenzz/Services/RemoteServicesProvider.dart';
import 'package:greenzz/UI/UserConnection.dart';

/// A drawer menu accessible in screens
class DrawerMenu extends StatefulWidget {
  final String title;

  DrawerMenu({Key? key, required this.title}) : super(key: key);

  @override
  _DrawerMenuState createState() => _DrawerMenuState();
}

class _DrawerMenuState extends State<DrawerMenu> {
  int _selectedDestination = 0;
  Future<String?> get _username async =>
      AppPreferences.instance.then((prefs) => prefs.loginUsername);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 100.0, 16.0, 15.0),
              child: getHeader(textTheme)),
          Divider(
            height: 1,
            thickness: 1,
          ),
          Padding(padding: const EdgeInsets.all(10.0)),
          ListTile(
            //
            leading: Icon(Icons.login),
            title: Text('login or register an account'),
            selected: _selectedDestination == 0,
            onTap: () {
              Navigator.pop(context);
              logIn();
            },
          ),
          ListTile(
            //
            leading: Icon(Icons.logout),
            title: Text('logout'),
            selected: _selectedDestination == 0,
            onTap: () {
              logOut();
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }

  /// Shows the UserConnectionDialog()
  void logIn() {
    showDialog(context: context, builder: (_) => UserConnectionDialog());
  }

  /// Log the user out
  void logOut() {
    RemoteServicesProvider.instance.then((v) => v.logUserOut());
  }

  /// Constructs the header of the drawer menu
  Widget getHeader(TextTheme textTheme) {
    return FutureBuilder<String?>(
      future: _username,
      builder: (context, snapshot) {
        final username = snapshot.data;
        if (username == null)
          return Text("Not logged in", style: textTheme.headline6);

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Logged in as", style: textTheme.subtitle2),
            Text(username, style: textTheme.headline6),
          ],
        );
      },
    );
  }
}

