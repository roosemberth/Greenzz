import 'dart:io';

import 'package:flutter/material.dart';

import '../Services/PlantService.dart';
import '../data/PlantProfile.dart';
import '../sensor_board/sb_task.dart';
import 'PlantList.dart';
import 'PlantPhotoShooter.dart';
import 'PlantProfile.dart';
import 'PlantProfileCreate.dart';

class ApplicationRoutes {
  static const String PlantList = "/plants";
  static const String PlantProfileCreate = "/plants/new";
  static const String PlantProfile = "/plants/profile";
  static const String SensorBoardConfigure = "/sensor_board/configure";
  static const String DailyPhotoNew = "/daily_photos/new";
}

Map<String, Widget Function(BuildContext)> applicationRoutes = {
  ApplicationRoutes.PlantList: (_) => PlantListScreen(),
  ApplicationRoutes.SensorBoardConfigure: (ctx) {
    final plantUuid = ModalRoute.of(ctx)!.settings.arguments as PlantID;
    return ConfigureSensorBoardTask(plantUuid);
  },
  ApplicationRoutes.PlantProfile: (ctx) {
    final profile = ModalRoute.of(ctx)!.settings.arguments as PlantProfile;
    return PlantProfileScreen(profile);
  },
  ApplicationRoutes.PlantProfileCreate: (_) => PlantProfileCreationScreen(),
  ApplicationRoutes.DailyPhotoNew: (ctx) {
    final file = ModalRoute.of(ctx)!.settings.arguments as File;
    return TakePlantDailyPictureScreen(file);
  }
};
