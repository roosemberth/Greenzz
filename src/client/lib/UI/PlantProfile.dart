import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:date_time_format/date_time_format.dart';
// Workaround for missing API
// ignore: implementation_imports
import 'package:dropdown_search/src/selectDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:full_screen_image_null_safe/full_screen_image_null_safe.dart';
import 'package:super_tooltip/super_tooltip.dart';

import './ConfigurationScreens.dart';
import '../PlantSpecieList.dart';
import '../Services/AppPreferences.dart';
import '../Services/LiveServices.dart';
import '../Services/PlantPictureService.dart';
import '../Services/PlantService.dart';
import '../Services/SpeciesService.dart';
import '../data/PlantProfile.dart';
import '../data/PlantStatus.dart';
import '../sensor_board/protocols.dart';
import 'DrawerMenu.dart';
import 'Routes.dart';

typedef void Continuation(void function());

/// A screen to interact with a plant profile
class PlantProfileScreen extends StatefulWidget {
  final PlantProfile _plant;

  PlantProfileScreen(this._plant);

  @override
  State createState() => _PlantProfileScreenState(_plant);
}

class _PlantProfileScreenState extends State<PlantProfileScreen> {
  final _refreshInProgressIndicatorKey = new GlobalKey<RefreshIndicatorState>();
  final PlantProfile _plant;
  final LiveSensorService liveData;
  bool _isEditing = false;

  final whiteEditIcon = const Icon(Icons.edit, color: Colors.white);
  final PageController galleryController = PageController();

  // Used for hints
  SuperTooltip? helpTooltip;
  final GlobalKey addTagKey = GlobalKey();
  final GlobalKey floatingPhotoKey = GlobalKey();
  final GlobalKey menuKey = GlobalKey();

  _PlantProfileScreenState(this._plant)
      : liveData = LiveSensorService(_plant.uuid);

  @override
  Widget build(BuildContext context) {
    AppPreferences.instance.then((prefs) async {
      if (await prefs.plantProfileTutorialIsEnabled)
        Future.delayed(Duration(milliseconds: 100), () {
          WidgetsBinding.instance?.addPostFrameCallback(startHelpOverlays);
        });
    });
    return WillPopScope(
        onWillPop: () async {
          if (helpTooltip != null && helpTooltip!.isOpen) {
            helpTooltip!.close();
            helpTooltip = null;
            return false;
          }
          return true;
        },
        child: Scaffold(
          appBar: mkAppBar(),
          drawer: DrawerMenu(title: "Drawer"),
          body: Container(
            padding: EdgeInsets.symmetric(horizontal: 5),
            child: RefreshIndicator(
              key: _refreshInProgressIndicatorKey,
              onRefresh: () async {
                await PlantPictureService.instance
                    .refreshAndDownloadExistingPlantPhotosFromServer(_plant)
                    .timeout(Duration(seconds: 20))
                    .then(
                  (_) => print(
                    "Received latest photos after forced "
                    "refresh for ${_plant.uuid}.",
                  ),
                  onError: (e, _) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Could not retrieve latest photos "
                          "from server."),
                    ));
                  },
                );
                await liveData.feedStream();
              },
              child: ListView(
                children: <Widget>[
                  Container(height: 264, child: mkImageGallery()),
                  plantDetails(),
                  sensorInfo(),
                  PlantSpeciesWidget(_plant),
                  // Extra space to leave clearance for the floating button.
                  SizedBox(height: 100),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            key: floatingPhotoKey,
            child: Icon(
              Icons.add_a_photo,
              color: Theme.of(context).primaryIconTheme.color,
            ),
            tooltip: "Add a daily photo",
            onPressed: () => addDailyPhoto(_plant, context)
                .then((_) => galleryController.jumpToPage(0)),
          ),
        ));
  }

  /// Returns a widget with two Column : the facts about the species and
  /// the tags associated to the plant
  Widget plantDetails() {
    return Column(children: [
      getPlantFactsWidget(),
      getPlantTagsWidget(),
    ]);
  }

  /// Returns a widget that display the images of the plant
  Widget mkImageGallery() {
    PlantPictureService.instance.updateProfiles().catchError((_) {});
    return StreamBuilder<List<String>>(
      stream: PlantPictureService.instance.photos(_plant),
      builder: (context, snapshot) {
        final mkGallery = () {
          if (snapshot.data == null)
            return Center(
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text("Loading daily photos"),
                SizedBox(height: 20),
                CircularProgressIndicator(),
              ]),
            );
          final photos = snapshot.data!;
          final Iterable<ImageProvider> images = photos.isNotEmpty
              ? photos.map((p) => FileImage(File(p)))
              : PlantPictureService.instance
                  .getAssetStockImagesForPlant(_plant)
                  .map((p) => AssetImage(p));
          final imageWidgets = images.map(_widgetForImage).toList();
          return PageView(
            controller: galleryController,
            scrollDirection: Axis.horizontal,
            children: imageWidgets,
          );
        };
        return FullScreenWidget(child: mkGallery());
      },
    );
  }

  static Widget _widgetForImage(ImageProvider<Object> image) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: Image(image: image),
      ),
    );
  }

  /// Returns the tile with the age of the plant
  Widget getPlantAgeWidget() {
    final header = ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text("Age", textAlign: TextAlign.start),
      trailing: !_isEditing ? null : whiteEditIcon,
      dense: true,
      onTap: !_isEditing
          ? null
          : () => changePlantAgeDialog(_plant, context).then((newAge) {
                if (newAge == null) return;
                setState(() => _plant.setAgeInWeeks(newAge));
              }),
    );
    final indicator = Text(
      _plant.ageInWeeks.toString(),
      style: Theme.of(context).primaryTextTheme.headline5,
    );
    final footer = Text(
      "Weeks",
      style: Theme.of(context).primaryTextTheme.bodyText1,
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [header, indicator, footer],
    );
  }

  /// Returns the tile with the status of the plant
  Widget getPlantStatusWidget() {
    final header = ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text("Status", textAlign: TextAlign.start),
      trailing: !_isEditing ? null : whiteEditIcon,
      dense: true,
      onTap: !_isEditing
          ? null
          : () => changePlantStatusDialog(_plant, context).then((newStatus) {
                if (newStatus == null) return;
                setState(() => _plant.status = newStatus);
              }),
    );
    final indicator = Text(
      toPrettyString(_plant.status),
      style: Theme.of(context).primaryTextTheme.headline5,
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [header, indicator],
    );
  }

  /// Returns the tile with the species of the plant
  Widget getSpeciesWidget() {
    final header = ListTile(
      contentPadding: EdgeInsets.zero,
      title: Text("Species", textAlign: TextAlign.start),
      trailing: !_isEditing ? null : whiteEditIcon,
      dense: true,
      onTap: !_isEditing
          ? null
          : () => changeSpeciesDialog(_plant, context).then((v) {
                if (v == null) return;
                setState(() => _plant.species = v);
              }),
    );
    final indicator = Text(_plant.species,
        style: Theme.of(context).primaryTextTheme.headline5);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [header, indicator],
    );
  }

  /// Returns a widget with the tags of the plant
  Widget getPlantTagsWidget() {
    Widget addTag = ActionChip(
      key: addTagKey,
      avatar: Icon(Icons.add),
      label: Text("Add tag"),
      onPressed: () => addTagDialog(_plant, context).then((v) {
        if (v == null) return;
        setState(() => _plant.addTag(v));
      }),
    );
    final toChip = (t) {
      final removeTagFn = () => setState(() => _plant.removeTag(t));
      return Chip(
        padding: EdgeInsets.all(2),
        label: Text(t),
        onDeleted: _isEditing ? removeTagFn : null,
      );
    };
    final tagChips = _plant.tags.map<Widget>(toChip).toList();
    final bool shouldShowAddTag = (_isEditing || tagChips.isEmpty) &&
        getExistingTags().difference(_plant.tags).isNotEmpty;

    final chips = tagChips + (shouldShowAddTag ? [addTag] : []);
    final pad = const SizedBox(width: 2);

    return Wrap(children: (chips).expand((w) => [w, pad]).toList());
  }

  /// Returns an expandable container with the facts about the species og the
  /// plant
  Widget getPlantFactsWidget() {
    ThemeData t = Theme.of(context);
    final factsPadding =
        const EdgeInsets.all(4).add(EdgeInsets.symmetric(horizontal: 4));
    final factsMargin = const EdgeInsets.symmetric(horizontal: 4);
    return Container(
      child: Theme(
        data: ThemeData(textTheme: Theme.of(context).primaryTextTheme),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Expanded(
                child: Card(
                  color: t.primaryColor,
                  child: Container(
                    padding: factsPadding,
                    margin: factsMargin,
                    child: getPlantAgeWidget(),
                  ),
                ),
              ),
              Expanded(
                child: Card(
                  color: t.primaryColor,
                  child: Container(
                    padding: factsPadding,
                    margin: factsMargin,
                    child: getPlantStatusWidget(),
                  ),
                ),
              ),
              Expanded(
                child: Card(
                  color: t.primaryColor,
                  child: Container(
                    padding: factsPadding,
                    margin: factsMargin,
                    child: getSpeciesWidget(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// Returns the AppBar with the menu for quick actions on the plant profile
  AppBar mkAppBar() {
    final Widget editMenu = PopupMenuButton(
      key: menuKey,
      itemBuilder: (BuildContext ctx) {
        return <PopupMenuEntry<void Function()?>>[
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.title),
              title: Text("Rename plant"),
            ),
            value: () => showRenameDialog(_plant.name, context).then((name) {
              if (name == null) return;
              setState(() {
                _plant.name = name;
                PlantService.instance.addPlantProfile(_plant);
              });
            }),
          ),
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.edit),
              title: Text("Edit details"),
            ),
            value: () => setState(() => _isEditing = true),
          ),
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.delete),
              title: Text("Delete profile"),
            ),
            value: () => showDeletionConfirmation(_plant, context).then((r) {
              if (!(r ?? false)) return;
              PlantService.instance
                  .removePlantProfile(_plant)
                  .then((_) => Navigator.of(context).pop());
            }),
          ),
          PopupMenuItem(
            child: ListTile(
              leading: Icon(Icons.developer_board),
              title: Text("Add sensor board"),
            ),
            value: () => autoDisplayServiceConfigScreens(context, () async {
              final plantService = PlantService.instance;
              plantService.addPlantProfile(_plant);
              final plantId = await plantService.getRegisteredPlantId(_plant);
              print("Calling SB configuration activity.");
              final result = await Navigator.pushNamed(
                  context, ApplicationRoutes.SensorBoardConfigure,
                  arguments: plantId);
              if (!(result is SensorBoardConfigurationResult?))
                throw StateError("SB config activity bad return type.");
              if (result == null) // User aborted
                return;
              print("Sensor board was assigned ID ${result.sensorId}");
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(
                  "Succesfully linked to ${result.name}. "
                  "Live sensor data should start displaying shortly.",
                ),
              ));
              _refreshInProgressIndicatorKey.currentState?.show();
              // Continues
            }, onError: (e) {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("There was a problem configuring your sensor "
                    "board. Please try again."),
              ));
              throw e;
            }),
          ),
        ];
      },
      onSelected: (void Function()? v) => v?.call(),
    );

    final Widget formConfirmButton = IconButton(
      icon: Icon(Icons.check_circle),
      onPressed: () => setState(() {
        _plant.lastModified = DateTime.now();
        PlantService.instance.addPlantProfile(_plant);
        _isEditing = false;
      }),
    );

    final Widget actionMenu = _isEditing ? formConfirmButton : editMenu;

    return AppBar(title: Text(_plant.name), actions: [actionMenu]);
  }

  void startHelpOverlays(Duration timeStamp) {
    if (helpTooltip != null) // There's a tooltip already in display
      return;

    final helpTooltips = {
      floatingPhotoKey: "Take a daily photo of your plant and see them grow!",
      addTagKey: "Tags helps us provide better recommendations for your plant.",
    };

    final showTooltipsInSequence = helpTooltips.entries
        .toList()
        .reversed
        .fold<Continuation>((c) => c(), (showNextToolTip, entry) {
      final text = Container(
        width: MediaQuery.of(context).size.width / 2,
        child: Text(entry.value, softWrap: true, textAlign: TextAlign.justify),
      );
      return (next) {
        helpTooltip = mkHinkTooltip(entry.key,
            children: [text], closeText: "Next", onClose: () {
          helpTooltip = null;
          showNextToolTip(() => next());
        });
        helpTooltip?.show(entry.key.currentContext!);
      };
    });

    showTooltipsInSequence.call(() {
      // Do the menu key last since the button text and orientation is different
      final text = Text(
        "You can pair a sensor board, manage tags and edit your plant details from the menu.",
        softWrap: true,
        textAlign: TextAlign.justify,
      );
      helpTooltip = mkHinkTooltip(menuKey,
          children: [text],
          onClose: () => AppPreferences.instance.then((prefs) async {
                await prefs.disablePlantProfileTutorial();
                helpTooltip = null;
              }),
          popupDirection: TooltipDirection.down);
      helpTooltip?.show(menuKey.currentContext!);
    });
  }

  static SuperTooltip mkHinkTooltip(GlobalKey target,
      {@required void Function()? onClose,
      @required List<Widget>? children,
      TooltipDirection popupDirection = TooltipDirection.up,
      String closeText = "Close"}) {
    late final SuperTooltip self;
    self = SuperTooltip(
      borderWidth: 1,
      hasShadow: false,
      dismissOnTapOutside: false,
      popupDirection: popupDirection,
      content: Material(
        child: Card(
          child: Padding(
            padding: EdgeInsets.only(top: 2, left: 2, right: 2),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: children! +
                  [
                    IntrinsicWidth(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                            child: Text(closeText),
                            autofocus: true,
                            onPressed: () {
                              self.close();
                              onClose?.call();
                            })
                      ],
                    ))
                  ],
            ),
          ),
        ),
      ),
    );
    return self;
  }

  Widget getMeasureWidget(String headerStr, double? value, String unit) {
    final header = Container(
      alignment: Alignment.topLeft,
      padding: EdgeInsets.symmetric(vertical: 4),
      child: Text(headerStr, textAlign: TextAlign.start),
    );
    String valueStr = value.toString();
    valueStr = valueStr.substring(0, min(5, valueStr.length));
    if (valueStr.endsWith('.'))
      valueStr = valueStr.substring(0, valueStr.length - 1);
    final valueLabel = (value) => [
          Text(valueStr, style: Theme.of(context).textTheme.headline5),
          Container(
            padding: EdgeInsets.symmetric(vertical: 2),
            child: Text(unit, style: Theme.of(context).textTheme.bodyText2),
          ),
        ];
    const nullvalueLabel = [Text("Unavailable")];
    final amountLabel = Container(
      padding: EdgeInsets.symmetric(vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: value == null ? nullvalueLabel : valueLabel(value),
      ),
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [header, amountLabel],
    );
  }

  /// Returns the widget with the measurement from the sensor board
  Widget sensorInfo() {
    final padding =
        const EdgeInsets.all(4).add(EdgeInsets.symmetric(horizontal: 4));
    final margin = const EdgeInsets.symmetric(horizontal: 4);
    final measurementCard = (header, measure, unit) {
      return Expanded(
        child: Card(
          child: Container(
            padding: padding,
            margin: margin,
            child: getMeasureWidget(header, measure, unit),
          ),
        ),
      );
    };
    return StreamBuilder<SensorData?>(
      stream: liveData.stream,
      builder: (context, snapshot) {
        final data = snapshot.data;
        if (data == null) // Data not available
          return SizedBox();
        return Card(
          color: Theme.of(context).primaryColor,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(5),
                alignment: Alignment.centerLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text("Live sensor data:",
                        style: Theme.of(context).primaryTextTheme.headline6),
                  ],
                ),
              ),
              IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    measurementCard("Temperature", data.temperature, "°C"),
                    measurementCard("Light", data.luminosity, "Lux"),
                    measurementCard("Soil moisture", data.soilMoisture, "%"),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 2),
                alignment: Alignment.bottomRight,
                child: LastSeenWidget(data.time),
              ),
            ],
          ),
        );
      },
    );
  }
}

class LastSeenWidget extends StatefulWidget {
  final DateTime time;

  const LastSeenWidget(this.time, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LastSeenWidgetState();
}

class _LastSeenWidgetState extends State<LastSeenWidget> {
  Timer? everySecond;

  @override
  void initState() {
    super.initState();
    everySecond = Timer.periodic(Duration(seconds: 1), (_) => setState(() {}));
  }

  @override
  void dispose() {
    everySecond?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mkText =
        (text) => Text(text, style: Theme.of(context).primaryTextTheme.caption);
    if (DateTime.now().difference(widget.time).inSeconds < 15)
      return mkText("");
    if (DateTime.now().difference(widget.time).inSeconds < 30)
      return mkText("Data received more than 15 seconds ago");
    if (DateTime.now().difference(widget.time).inSeconds < 45)
      return mkText("Data received more than 30 seconds ago");
    if (DateTime.now().difference(widget.time).inSeconds < 60)
      return mkText("Data received more than 45 seconds ago");
    final lastUpdateStr = DateTimeFormat.relative(
      widget.time,
      appendIfAfter: 'ago',
      ifNow: 'now',
    );
    return mkText("Data received $lastUpdateStr");
  }
}

/// Add daily photo to the profile
Future<void> addDailyPhoto(PlantProfile plant, BuildContext context) async {
  final plantPicsService = PlantPictureService.instance;
  final takeNewPhoto = () {
    plantPicsService.addDailyPhoto(plant, (destFile) async {
      await Navigator.pushNamed(context, ApplicationRoutes.DailyPhotoNew,
          arguments: File(destFile));
    });
  };

  final todayPhotos = await plantPicsService.photosAtDate(plant);

  if (todayPhotos.isEmpty) return takeNewPhoto();
  showDialog(
    context: context,
    builder: (_) {
      return AlertDialog(
        title: Text("Replace today's photo?"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "A photo of this plant was taken earlier today. "
              "Taking a new one will replace it.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10),
            Text("Do you wish to continue?"),
          ],
        ),
        actions: [
          TextButton(
            child: Text("Cancel"),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text("Continue"),
            onPressed: () {
              Navigator.of(context).pop();
              takeNewPhoto();
            },
          ),
        ],
      );
    },
  );
}

Future<String?> showRenameDialog(String oldName, BuildContext context) {
  return showDialog(
    context: context,
    builder: (_) {
      final _nameFormKey = GlobalKey<FormState>();
      String? name;
      return AlertDialog(
        title: Text("Choose a new name for $oldName"),
        content: Form(
          key: _nameFormKey,
          child: TextFormField(
            autofocus: true,
            maxLines: 1,
            textCapitalization: TextCapitalization.words,
            validator: (v) => v?.isEmpty ?? true ? "Field is required" : null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            onSaved: (v) => name = v,
          ),
        ),
        actions: [
          TextButton(
            child: Text("Cancel"),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text("Confirm"),
            onPressed: () {
              if (!_nameFormKey.currentState!.validate()) return;
              _nameFormKey.currentState!.save();
              Navigator.of(context).pop(name);
            },
          ),
        ],
      );
    },
  );
}

Future<int?> changePlantAgeDialog(PlantProfile plant, BuildContext context) {
  return showDialog(
    context: context,
    builder: (_) {
      final _formKey = GlobalKey<FormState>();
      int? newAge;
      return AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(title: Text("How old is ${plant.name}?")),
            Form(
              key: _formKey,
              child: TextFormField(
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                decoration: InputDecoration(
                  //labelText: "An estimate of its age",
                  border: OutlineInputBorder(),
                  suffixText: "Weeks",
                ),
                validator: (v) {
                  if (v?.isEmpty ?? true) return "Field is required";
                  if (int.tryParse(v!) == null) return "Field must be a number";
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                onSaved: (v) => newAge = int.parse(v!),
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            child: Text("Cancel"),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text("Confirm"),
            onPressed: () {
              if (!_formKey.currentState!.validate()) return;
              _formKey.currentState!.save();
              Navigator.of(context).pop(newAge);
            },
          ),
        ],
      );
    },
  );
}

Future<PlantStatus?> changePlantStatusDialog(
    PlantProfile plant, BuildContext context) {
  return showDialog(
    context: context,
    builder: (_) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(0),
        content: SelectDialog(
          showSelectedItem: true,
          selectedValue: toPrettyString(plant.status),
          popupTitle:
              ListTile(title: Text("How is ${plant.name} looking today?")),
          items: PlantStatus.values.map(toPrettyString).toList(),
        ),
      );
    },
  ).then((v) => fromPrettyString(v));
}

Future<String?> changeSpeciesDialog(PlantProfile plant, BuildContext context) {
  late final StreamController<List<String>?> sc;
  late final feed;
  Timer? retrier;
  feed = () {
    SpeciesDetailsService.instance.getSpeciesList().then(sc.add, onError: (_) {
      print("Failed to retrieve known plant species list. Rescheduling...");
      retrier = Timer(Duration(seconds: 1), feed);
    });
  };
  sc = StreamController(onListen: feed);
  return showDialog(
    context: context,
    builder: (_) {
      return WillPopScope(
        child: StreamBuilder<List<String>?>(
          stream: sc.stream,
          builder: (context, snapshot) {
            final data = snapshot.data;
            if (data == null)
              return AlertDialog(
                title: Text("Fetching known species..."),
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [CircularProgressIndicator()],
                ),
              );
            return speciesAlertDialog(data, plant);
          },
        ),
        onWillPop: () async {
          retrier?.cancel();
          if (!sc.isClosed) sc.close();
          return true;
        },
      );
    },
  ).then((newSpecie) {
    if (!sc.isClosed) sc.close();
    return newSpecie;
  });
}

Widget speciesAlertDialog(List<String> plantList, PlantProfile plant) {
  return AlertDialog(
    contentPadding: EdgeInsets.all(0),
    content: SelectDialog(
      showSearchBox: true,
      showSelectedItem: true,
      hintText: "Search the specie of ${plant.name}?",
      autoFocusSearchBox: true,
      selectedValue: plant.species,
      items: plantList,
    ),
  );
}

Future<String?> addTagDialog(PlantProfile plant, BuildContext context) {
  return showDialog(
    context: context,
    builder: (_) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(0),
        content: SelectDialog(
          showSearchBox: true,
          hintText: "Search tags for ${plant.name}...",
          items: getExistingTags().difference(plant.tags).toList(),
        ),
      );
    },
  );
}

Future<bool?> showDeletionConfirmation(
    PlantProfile plant, BuildContext context) {
  return showDialog(
    context: context,
    builder: (_) {
      return AlertDialog(
        title: Text("Delete ${plant.name}'s profile?"),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "This will delete all data stored about the plant both locally "
              "and remotely. This procedure is irreversible.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10),
            Text("Do you wish to continue?"),
          ],
        ),
        actions: [
          TextButton(
            child: Text("Cancel"),
            onPressed: () => Navigator.of(context).pop(),
          ),
          TextButton(
            child: Text("Continue"),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
        ],
      );
    },
  );
}

/// A widget to display information on the plant
class PlantSpeciesWidget extends StatefulWidget {
  final PlantProfile _plant;

  const PlantSpeciesWidget(this._plant, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PlantSpeciesWidgetState();
}

class _PlantSpeciesWidgetState extends State<PlantSpeciesWidget> {
  StreamController<String>? plantSpeciesInformationSc;

  @override
  void dispose() {
    plantSpeciesInformationSc?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    plantSpeciesInformationSc =
        StreamController.broadcast(onListen: updateSpeciesInfo);
    final fetchFailedWidget = Card(
      child: ListTile(
        title: Text(
          "Failed to fetch information.",
          textAlign: TextAlign.center,
        ),
        trailing: ActionChip(
          avatar: Icon(Icons.sync),
          label: Text("Retry"),
          onPressed: updateSpeciesInfo,
        ),
      ),
    );
    return ExpansionTile(
      title: Text("Some facts about ${widget._plant.name}:"),
      children: <Widget>[
        StreamBuilder<String>(
          stream: plantSpeciesInformationSc?.stream,
          builder: (context, snapshot) {
            final speciesInformation = snapshot.data;
            if (speciesInformation == null)
              return Container(
                padding: EdgeInsets.all(10),
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              );
            if (speciesInformation == "") return fetchFailedWidget;
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 6),
              child: getSpeciesInfos(speciesInformation),
            );
          },
        ),
      ],
    );
  }

  /// Gets the facts
  void updateSpeciesInfo() {
    SpeciesDetailsService.instance
        .getSpeciesDetails(widget._plant.species)
        .catchError((_) {
      print("Failed to retrieve species information for ${widget._plant}.");
      return "";
    }).then((info) => plantSpeciesInformationSc?.add(info));
  }

  /// Returns the formatted TextSpan of the facts about the plant species
  Widget getSpeciesInfos(String infos) {
    List<String> lines = infos.split("\n");
    if (lines.length != 7) {
      for (int i = lines.length; i < 7; i++) {
        lines.add("");
      }
    }
    final mkFactHeader = (text) => TextSpan(
        text: "$text\n",
        style: Theme.of(context)
            .textTheme
            .subtitle2
            ?.copyWith(fontWeight: FontWeight.bold));
    final mkFactBody = (text) => TextSpan(
        text: "$text\n\n", style: Theme.of(context).textTheme.bodyText1);
    return RichText(
        text: TextSpan(
      style: TextStyle(color: Colors.black.withOpacity(1.0)),
      children: <TextSpan>[
        mkFactBody(lines[0]),
        mkFactHeader("Expected size"),
        mkFactBody(lines[1]),
        mkFactHeader("Preferred soil type"),
        mkFactBody(lines[2]),
        mkFactHeader("Lighting"),
        mkFactBody(lines[3]),
        mkFactHeader("Watering"),
        mkFactBody(lines[4]),
        mkFactHeader("Fertilizer application"),
        mkFactBody(lines[5]),
        mkFactHeader("Pruning"),
        mkFactBody(lines[6]),
      ],
    ));
  }
}

// Credit: https://stackoverflow.com/a/58788092
extension GlobalKeyExtension on GlobalKey {
  Rect? get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject();
    var translation = renderObject?.getTransformTo(null).getTranslation();
    if (translation != null && renderObject?.paintBounds != null) {
      return renderObject?.paintBounds
          .shift(Offset(translation.x, translation.y));
    } else {
      return null;
    }
  }
}
