import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

Future<bool> _displayTextInputDialog(
    BuildContext context, String title, String baseText, Widget content) async {
  bool valide = false;
  await showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: content,
          actions: [
            TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('OK'),
              onPressed: () {
                valide = true;
                Navigator.of(context).pop();
              },
            )
          ],
        );
      });
  return valide;
}

Future<String> displayTextInputDialog(BuildContext context, String title,
    {String baseText = ""}) async {
  final TextEditingController textController =
      TextEditingController(text: baseText);
  if (await _displayTextInputDialog(
      context, title, baseText, TextField(controller: textController))) {
    return textController.text;
  } else {
    return baseText;
  }
}

Future<String> displayTextInputDialogSuggestion(
    BuildContext context, String title, Iterable<String> suggestedString,
    {String baseText = ""}) async {
  final TextEditingController textController =
      TextEditingController(text: baseText);
  if (await _displayTextInputDialog(
      context,
      title,
      "",
      TypeAheadField<String>(
        textFieldConfiguration:
            TextFieldConfiguration(controller: textController),
        suggestionsCallback: (pattern) async {
          pattern = pattern.toLowerCase();
          return suggestedString
              .where((element) => element.toLowerCase().contains(pattern));
        },
        itemBuilder: (context, suggestion) {
          return ListTile(
            title: Text(suggestion),
          );
        },
        onSuggestionSelected: (suggestion) {
          textController.text = suggestion;
        },
      ))) {
    return textController.text;
  } else {
    return "";
  }
}