extension TemporalReference on DateTime {
  bool isSameDay(DateTime? other) {
    if (other == null) return false;
    DateTime otherUtc = other.toUtc();
    DateTime nowUtc = this.toUtc();
    return otherUtc.day == nowUtc.day &&
        otherUtc.month == nowUtc.month &&
        otherUtc.year == nowUtc.year;
  }

  bool get isToday => this.isSameDay(DateTime.now());

  String get iso8601UtcDateTime {
    DateTime now = this.toUtc();
    // I don't want to add a package just for a format...
    return "${now.year.toString()}-"
        "${now.month.toString().padLeft(2, '0')}-"
        "${now.day.toString().padLeft(2, '0')}"
        "T"
        "${now.hour.toString().padLeft(2, '0')}:"
        "${now.minute.toString().padLeft(2, '0')}:"
        "${now.second.toString().padLeft(2, '0')}"
        "Z";
  }
}
