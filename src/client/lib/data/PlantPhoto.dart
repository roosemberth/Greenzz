/// This class models a plant's photo
class PlantPhoto implements Comparable {
  final String plantUuid;
  final String timestamp;

  PlantPhoto(this.plantUuid, this.timestamp);

  @override
  String toString() {
    return "Photo $plantUuid@$timestamp";
  }

  @override
  int compareTo(other) => this.timestamp.compareTo(other.timestamp);

  @override
  bool operator ==(other) {
    if (other is! PlantPhoto) return false;
    return (plantUuid == other.plantUuid) && (timestamp == other.timestamp);
  }

  int? _hashCode;
  @override
  int get hashCode {
    if (_hashCode == null)
      _hashCode = (2 * plantUuid.hashCode + 1) + 2 * timestamp.hashCode;
    return _hashCode!;
  }
}
