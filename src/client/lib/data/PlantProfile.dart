import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

import '../util/GreenzzTypeExtensions.dart';
import 'PlantStatus.dart';

/// Returns a DateTime parsed from a String
///
/// Throws [ArgumentError] if [json] is not a String or its not an UTC date
/// and time.
DateTime readDateTime(json) {
  if (!(json is String))
    throw ArgumentError("Illegal argument when deserializing DataTime json.");
  final value = DateTime.parse(json);
  if (!value.isUtc)
    throw ArgumentError("The specified DateTime json is not UTC.");
  return value;
}

String toIso8601NoMilli(DateTime ts) => ts.toUtc().iso8601UtcDateTime;

/// A profile with all the information on a plant
class PlantProfile {
  /// ID of a profile
  final String uuid;
  /// Name of a profile
  String name;
  /// Time of birth of the plant
  DateTime _approximateBirthday;
  /// Status of the plant
  PlantStatus status;
  /// Tags associated  with the plant
  Set<String> tags;
  /// last time the profile was modified
  DateTime _lastModified;
  /// Time of creation of the plant profile
  DateTime _created;
  /// Species of the plant
  String species;

  /// Constructs a plant profile with all its data
  PlantProfile(this.name, this._approximateBirthday, this.status, this.species,
      {Set<String>? tags,
      DateTime? lastModified,
      DateTime? created,
      String? uuid})
      : uuid = uuid ?? Uuid().v4(),
        tags = tags ?? Set(),
        _lastModified = lastModified ?? DateTime.now(),
        _created = lastModified ?? DateTime.now() {
    // Sanitize values by using the setters
    approximateBirthday = this._approximateBirthday;
    lastModified = this._lastModified;
    created = this._created;
  }

  /// Constructs a plant profile from a map of json information
  PlantProfile.fromJson(Map<String, dynamic> json)
      : name = json["name"],
        _approximateBirthday = readDateTime(json["approximateBirthday"]),
        status = GreenzzPlantStatusJson.fromJson(json["status"])!,
        species = json["species"],
        uuid = json["uuid"],
        tags = (json["tags"]?.cast<String>() ?? <String>[]).toSet(),
        _lastModified = readDateTime(json["lastModified"]),
        _created = readDateTime(json["created"]);

  /// The age in weeks of a plant
  int get ageInWeeks =>
      (DateTime.now().difference(_approximateBirthday).inDays / 7).round();

  set approximateBirthday(DateTime v) =>
      _approximateBirthday = _removeMilliAndMicro(v);

  /// The birthday of a plant
  DateTime get approximateBirthday => _approximateBirthday;

  set lastModified(DateTime v) => _lastModified = _removeMilliAndMicro(v);

  /// The last time a plant was modified
  DateTime get lastModified => _lastModified;

  set created(DateTime v) => _created = _removeMilliAndMicro(v);

  /// The creation's date of a profile
  DateTime get created => _created;

  static DateTime _removeMilliAndMicro(DateTime v) => v
      .subtract(Duration(
        milliseconds: v.microsecond,
        microseconds: v.microsecond,
      ))
      .toUtc();

  /// Add a tag to the set of tags
  ///
  /// The [tag] is added in the Set<String> tags member
  void addTag(String tag) {
    if (tag.length > 1 && tag.length < 13) {
      tags.add(tag);
    }
  }

  /// Remove a tag to the set of tags
  ///
  /// The [tag] is removed from the Set<String> tags member
  void removeTag(String tag) {
    tags.remove(tag);
  }

  void setAgeInWeeks(int newAge) {
    _approximateBirthday = DateTime.now().subtract(Duration(days: newAge * 7));
  }

  /// Returns a JSON type Map<String, dynamic> from the data of a plant profile
  Map<String, dynamic> toJson() => {
        'uuid': uuid,
        'name': name,
        'approximateBirthday': toIso8601NoMilli(_approximateBirthday),
        'species': species,
        'status': status.toJson(),
        'tags': tags.toList(),
        'lastModified': toIso8601NoMilli(_lastModified),
        'created': toIso8601NoMilli(_created),
      };

  @override
  bool operator ==(other) {
    if (other is! PlantProfile) return false;
    return uuid == other.uuid &&
        name == other.name &&
        _approximateBirthday.iso8601UtcDateTime ==
            other._approximateBirthday.iso8601UtcDateTime &&
        status == other.status &&
        setEquals(tags, other.tags) &&
        _lastModified.iso8601UtcDateTime ==
            other._lastModified.iso8601UtcDateTime &&
        _created.iso8601UtcDateTime == other._created.iso8601UtcDateTime &&
        species == other.species;
  }

  int? _hashCode;
  @override
  int get hashCode {
    if (_hashCode == null)
      _hashCode = (11 * uuid.hashCode + 0) +
          (11 * name.hashCode + 1) +
          (11 * _approximateBirthday.hashCode + 2) +
          (11 * status.hashCode + 3) +
          (11 * tags.hashCode + 4) +
          (11 * _lastModified.hashCode + 5) +
          (11 * _created.hashCode + 6) +
          (11 * species.hashCode + 7);
    return _hashCode!;
  }

  @override
  String toString() {
    return "PlantProfile of $uuid ($name)";
  }
}
