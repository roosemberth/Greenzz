enum PlantStatus { happy, neutral, sad }

/// Returns a String corresponding to [plantStatus] with a capitalised first letter.
String toPrettyString(PlantStatus plantStatus) {
  switch (plantStatus) {
    case PlantStatus.happy:
      return "Happy";
    case PlantStatus.sad:
      return "Sad";
    case PlantStatus.neutral:
      return "Neutral";
  }
}
/// Returns the PlantStatus corresponding to the [str]
PlantStatus? fromPrettyString(String? str) {
  switch (str) {
    case "Happy":
      return PlantStatus.happy;
    case "Sad":
      return PlantStatus.sad;
    case "Neutral":
      return PlantStatus.neutral;
  }
  return null;
}

extension GreenzzPlantStatusJson on PlantStatus {
  static PlantStatus? fromJson(String str) {
    switch (str) {
      case "HAPPY":
        return PlantStatus.happy;
      case "NEUTRAL":
        return PlantStatus.neutral;
      case "SAD":
        return PlantStatus.sad;
    }
  }

  String toJson() {
    switch (this) {
      case PlantStatus.happy:
        return "HAPPY";
      case PlantStatus.neutral:
        return "NEUTRAL";
      case PlantStatus.sad:
        return "SAD";
    }
  }
}
