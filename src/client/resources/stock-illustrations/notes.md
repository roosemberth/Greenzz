# Stock illustrations

This directory contains processed stock illustrations used throughout the
application. Some details are provided here bellow:

## Plant profile pictures

Plant profile pictures have a 4:3 format.
Stock pictures are resampled to 1920x1440 over a white background with the
plant image in the middle.
The plant image or illustration should be centered in the canvas and its height
should be of 1000 pixels.
