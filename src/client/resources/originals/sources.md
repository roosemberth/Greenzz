# Stock images sources

The images in this directory have been downloaded free of charge under
[Pixabay License](https://pixabay.com/service/license/) for non-commential
purposes.

Under this license terms, we are legally allowed to make modifications and
distribute them with the application for as long as it is free of charge
without any attribution obligation.

URLs:

- <https://pixabay.com/illustrations/house-plants-cactus-flowerpot-plant-4116992/>
- <https://pixabay.com/illustrations/house-plants-cactus-flowerpot-plant-4116995/>
- <https://pixabay.com/illustrations/lily-flower-plant-blossom-bloom-1944236/>
- <https://pixabay.com/illustrations/orchid-flower-plant-blossom-bloom-1944240/>
