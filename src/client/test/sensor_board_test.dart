import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:permission_handler/permission_handler.dart';

import './sensor_board_mocks.dart';
import '../lib/Services/PlantService.dart';
import '../lib/sensor_board/protocols.dart';
import '../lib/sensor_board/rpc_commands.dart';
import '../lib/sensor_board/sb_task.dart';

void testConfigSbTask(
    String description,
    Future<void> Function(WidgetTester, BtMock, LocationPermissionMock,
            ConfigureSensorBoardTask)
        testBody) {
  testWidgets(description, (WidgetTester tester) async {
    final bt = BtMock(), lp = LocationPermissionMock();
    var underTest = ConfigureSensorBoardTask(
      PlantID("placeholder-plant-uuid"),
      bt: bt,
      locationPermission: lp,
    );
    testBody(tester, bt, lp, underTest);
  });
}

void testSbConfigScreen(
    String description,
    Future<void> Function(
            WidgetTester, SensorBoardConnectionControllerMock, SbConfigScreen)
        testBody) {
  testWidgets(description, (WidgetTester tester) async {
    final sb = SensorBoardConnectionControllerMock();
    var underTest = SbConfigScreen(
      PlantID("placeholder-plant-uuid"),
      BluetoothDeviceMock(),
      sb: sb,
    );
    testBody(tester, sb, underTest);
  });
}

void expectTextPat(String pattern) {
  expect(find.textContaining(RegExp(pattern)), findsOneWidget);
}

void permissionScreenTests() {
  group('Sensor configuration screen shows an adequate message when', () {
    testConfigSbTask('Bluetooth adapter is off', (wt, bt, lp, underTest) async {
      bt.setState(BluetoothState.off);
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expectTextPat('Please turn on your Bluetooth adapter.');
    });

    testConfigSbTask('Bluetooth adapter is turning on',
        (wt, bt, lp, underTest) async {
      bt.setState(BluetoothState.turningOn);

      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expectTextPat(r"Bluetooth.*turning on");
    });

    testConfigSbTask('Bluetooth adapter is unavailable',
        (wt, bt, lp, underTest) async {
      bt.setState(BluetoothState.unavailable);
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expectTextPat(r"Bluetooth adapter.*none available");
    });

    testConfigSbTask('Bluetooth adapter access is unauthorized',
        (wt, bt, lp, underTest) async {
      bt.setState(BluetoothState.unauthorized);
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expectTextPat(r"needs access.*Bluetooth adapter");
    });

    testConfigSbTask('Location access is not granted',
        (wt, bt, lp, underTest) async {
      bt.setState(BluetoothState.on);
      lp.setState(PermissionStatus.denied);
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expectTextPat(r"never use your location");
      expectTextPat(r"request access to your location.*Bluetooth adapter");
      expect(find.widgetWithText(ElevatedButton, "Request access."),
          findsOneWidget);
    });

    testConfigSbTask('Location access is permanently denied',
        (wt, bt, lp, underTest) async {
      bt.setState(BluetoothState.on);
      lp.setState(PermissionStatus.permanentlyDenied);
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expectTextPat(r"never use your location");
      expectTextPat(r"request access to your location.*Bluetooth adapter");
      expectTextPat(r"Location access has been permanently denied.");
      expect(find.widgetWithText(ElevatedButton, "Go to settings."),
          findsOneWidget);
    });
  });
}

void deviceSelectionScreenTests() {
  testConfigSbTask('Bluetooth device selection screen interactions',
      (wt, bt, lp, underTest) async {
    bt.setState(BluetoothState.on);
    lp.setState(PermissionStatus.granted);
    await wt.pumpWidget(MaterialApp(home: underTest));

    // A scan was triggered automatically
    await wt.pumpAndSettle();
    verify(bt.startScan()).called(1);
    bt.setScanning(true);
    await wt.pumpAndSettle();
    expect(
        find.widgetWithIcon(FloatingActionButton, Icons.stop), findsOneWidget);

    // Upon receiving the scan results, the icon allows to retrigger
    bt.setScanning(false);
    await wt.pumpAndSettle();
    verifyNever(bt.startScan());
    expect(find.widgetWithIcon(FloatingActionButton, Icons.search),
        findsOneWidget);

    // Pressing the search button retriggers a scan
    bt.setScanning(true);
    await wt.tap(find.widgetWithIcon(FloatingActionButton, Icons.search));
    await wt.pumpAndSettle();
    verify(bt.startScan()).called(1);
    expect(
        find.widgetWithIcon(FloatingActionButton, Icons.stop), findsOneWidget);

    // Upon finding a device, we can see it in the screen
    final btDevice = BtScanResultStub.mockDevice;
    bt.setResults(<ScanResult>[BtScanResultStub(device: btDevice)]);
    bt.setScanning(false);
    await wt.pumpAndSettle();
    final deviceMatcher = find.widgetWithText(ListTile, btDevice.name);
    expect(deviceMatcher, findsOneWidget);

    // Upon selecting a device, the Sb configuration screen is displayed
    await wt.tap(deviceMatcher);
    await wt.pump(); // We don't know whether SbConfigScreen settles.
    expect(find.byType(SbConfigScreen), findsOneWidget);
  });

  testConfigSbTask('Sensor Board configuration screen interactions',
      (wt, bt, lp, underTest) async {
    final btDevice = BtScanResultStub.mockDevice;
    bt.setState(BluetoothState.on);
    lp.setState(PermissionStatus.granted);
    await wt.pumpWidget(MaterialApp(home: underTest));
    await wt.pumpAndSettle();
    bt.setResults(<ScanResult>[BtScanResultStub(device: btDevice)]);
    await wt.pumpAndSettle();

    // Upon select the device
    await wt.tap(find.widgetWithText(ListTile, btDevice.name));
    await wt.pump(); // We don't know whether SbConfigScreen settles.
    expect(find.byType(SbConfigScreen), findsOneWidget);
  });
}

void sbConfigScreenTests() {
  group('Sensor board configuration screen shows an adequate message when', () {
    testSbConfigScreen('Board connecting', (wt, sbc, underTest) async {
      sbc.setState(SensorBoardActionInProgress("Placeholder title"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('Configuring your sensor board');
    });

    testSbConfigScreen('Board connecting with msg', (wt, sbc, underTest) async {
      sbc.setState(
          SensorBoardActionInProgress("", userMessage: "Random message"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('Random message');
    });

    testSbConfigScreen('connect failed', (wt, sbc, underTest) async {
      sbc.setState(SensorBoardConnectionFailed());
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('connection failed');
    });

    testSbConfigScreen('connect failed with msg', (wt, sbc, underTest) async {
      sbc.setState(SensorBoardConnectionFailed(userMessage: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('Some text');
      expect(find.textContaining('Technical details'), findsNothing);
    });

    testSbConfigScreen('connect failed with technical details',
        (wt, sbc, underTest) async {
      sbc.setState(SensorBoardConnectionFailed(technicalDetails: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('connection failed');
      expect(find.textContaining(RegExp('Some text')), findsNothing);
      await wt.tap(find.textContaining('Technical details'));
      await wt.pump();
      expectTextPat('Some text');
    });

    testSbConfigScreen('user chooses a network upon WiFi results',
        (wt, sbc, underTest) async {
      final n1 = const WiFiNetworkEntry("Foobar", -10, "D0:0D:FE:ED:13:37");
      final n2 = const WiFiNetworkEntry(
          "Another SSID in the list", -10, "AA:BB:CC:DD:EE:FF");
      final chooseSSID = n1.ssid;
      final choosePass = "Some password";
      bool networkChosen = false;
      bool refreshed = false;

      sbc.setState(SensorBoardWiFiResults([n1, n2], (creds) {
        networkChosen = true;
        expect(creds.ssid, chooseSSID);
        expect(creds.password, choosePass);
      }, () {
        refreshed = true;
        return Future(() => null);
      }));

      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();

      expectTextPat('WiFi network selection');
      expectTextPat('Sensor Board requires an internet connection');
      expectTextPat('select the WiFi network it will connect to');
      expectTextPat(n1.ssid);
      expectTextPat(n1.bssid);
      expectTextPat(n2.ssid);
      expectTextPat(n2.bssid);

      expect(refreshed, false);
      await wt.tap(find.byIcon(Icons.search));
      await wt.pumpAndSettle();
      expect(refreshed, true);

      expect(networkChosen, false);
      await wt.tap(find.textContaining(chooseSSID));
      await wt.pump();

      expectTextPat('Connect to ' + chooseSSID);

      expect(networkChosen, false);
      await wt.enterText(find.byType(TextField), choosePass);
      await wt.tap(find.byKey(const Key('Connect button')));
      await wt.pumpAndSettle();

      expect(networkChosen, true);
    });

    testSbConfigScreen('handshaking failed', (wt, sbc, underTest) async {
      sbc.setState(SensorBoardHandshakingFailed(userMessage: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('device.*not.*compatible');
      expectTextPat('A Bluetooth connection.*established');
      expectTextPat('firmware upgrade');
      expect(find.textContaining('Technical details'), findsNothing);
    });

    testSbConfigScreen('handshaking failed with message',
        (wt, sbc, underTest) async {
      sbc.setState(SensorBoardHandshakingFailed(userMessage: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('device.*not.*compatible');
      expectTextPat('Some text');
      expectTextPat('A Bluetooth connection.*established');
      expectTextPat('firmware upgrade');
      expect(find.textContaining('Technical details'), findsNothing);
    });

    testSbConfigScreen('handshaking failed with technical details',
        (wt, sbc, underTest) async {
      sbc.setState(SensorBoardHandshakingFailed(technicalDetails: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('device.*not.*compatible');
      expect(find.textContaining(RegExp('Some text')), findsNothing);
      await wt.tap(find.textContaining('Technical details'));
      await wt.pump();
      expectTextPat('A Bluetooth connection.*established');
      expectTextPat('firmware upgrade');
      expectTextPat('Some text');
    });

    testSbConfigScreen('configuration failed', (wt, sbc, underTest) async {
      sbc.setState(SensorBoardConfigurationFailed(userMessage: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('unable to configure');
      expectTextPat('A Bluetooth connection.*established');
      expectTextPat('firmware upgrade');
      expect(find.textContaining('Technical details'), findsNothing);
    });

    testSbConfigScreen('configuration failed with message',
        (wt, sbc, underTest) async {
      sbc.setState(SensorBoardConfigurationFailed(userMessage: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('unable to configure');
      expectTextPat('Some text');
      expectTextPat('A Bluetooth connection.*established');
      expectTextPat('firmware upgrade');
      expect(find.textContaining('Technical details'), findsNothing);
    });

    testSbConfigScreen('configuration failed with technical details',
        (wt, sbc, underTest) async {
      sbc.setState(
          SensorBoardConfigurationFailed(technicalDetails: "Some text"));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pump();
      expectTextPat('unable to configure');
      expect(find.textContaining(RegExp('Some text')), findsNothing);
      await wt.tap(find.textContaining('Technical details'));
      await wt.pump();
      expectTextPat('A Bluetooth connection.*established');
      expectTextPat('firmware upgrade');
      expectTextPat('Some text');
    });
  });
}

void main() {
  permissionScreenTests();
  deviceSelectionScreenTests();
  sbConfigScreenTests();
}
