import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:greenzz/UI/PlantProfile.dart';

void liveSensorDataLastSeenTests() {
  group("Last seen widget", () {
    testWidgets("no text displayed under 15 secs.", (WidgetTester wt) async {
      var underTest =
          LastSeenWidget(DateTime.now().subtract(Duration(seconds: 10)));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expect(find.textContaining("ago"), findsNothing);
    });

    testWidgets("text displayed between 15 and 30s.", (WidgetTester wt) async {
      var underTest =
          LastSeenWidget(DateTime.now().subtract(Duration(seconds: 20)));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expect(find.textContaining("more than 15 seconds ago"), findsOneWidget);
    });

    testWidgets("text displayed between 30 and 45s.", (WidgetTester wt) async {
      var underTest =
          LastSeenWidget(DateTime.now().subtract(Duration(seconds: 40)));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expect(find.textContaining("more than 30 seconds ago"), findsOneWidget);
    });

    testWidgets("text displayed between 45s and 1m.", (WidgetTester wt) async {
      var underTest =
          LastSeenWidget(DateTime.now().subtract(Duration(seconds: 50)));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expect(find.textContaining("more than 45 seconds ago"), findsOneWidget);
    });

    testWidgets("text displayed after a minute.", (WidgetTester wt) async {
      var underTest =
          LastSeenWidget(DateTime.now().subtract(Duration(seconds: 60)));
      await wt.pumpWidget(MaterialApp(home: underTest));
      await wt.pumpAndSettle();
      expect(find.textContaining("1 minute ago"), findsOneWidget);
    });
  });
}

void main() {
  liveSensorDataLastSeenTests();
}
