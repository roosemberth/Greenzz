import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:greenzz/data/PlantProfile.dart';
import 'package:greenzz/data/PlantStatus.dart';

void dateTimeIso8601ComplianceTests() {
  final expectedDate = DateTime.utc(2021, 05, 27, 13, 03, 34);
  final expectedStr = "2021-05-27T13:03:34Z";

  test("DateTime: Serialization & deserialization", () {
    expect(expectedStr, toIso8601NoMilli(expectedDate));
    expect(expectedDate, readDateTime(expectedStr));
  });
}

void plantStatusExamplesTests() {
  test("Plant status: Serialization & deserialization", () {
    expect(PlantStatus.neutral, GreenzzPlantStatusJson.fromJson("NEUTRAL"));
    expect("NEUTRAL", PlantStatus.neutral.toJson());
  });
}

void plantProfileMarshallingTests() {
  test("Test plant marshalling", () {
    final expectedStr =
        '{"uuid":"b5e1a98b-523b-4b80-aa94-e142fb25f4cb","name":"Étoile de Noël"'
        ',"approximateBirthday":"2021-05-27T17:14:57Z","species":"Coreopsis","s'
        'tatus":"HAPPY","tags":["Tag A","Tag B"],"lastModified":"2021-05-27T17:'
        '14:57Z","created":"2021-05-27T17:14:57Z"}';
    expect(
      expectedStr,
      jsonEncode(PlantProfile.fromJson(jsonDecode(expectedStr)).toJson()),
    );

    final expectedPp = PlantProfile(
      "Étoile de Noël",
      DateTime.now(),
      PlantStatus.happy,
      "Some species",
    );
    expect(
      expectedPp,
      PlantProfile.fromJson(jsonDecode(jsonEncode(expectedPp.toJson()))),
    );
  });
}

void main() {
  dateTimeIso8601ComplianceTests();
  plantStatusExamplesTests();
  plantProfileMarshallingTests();
}
