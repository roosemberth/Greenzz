import 'dart:async';

import 'package:flutter_blue/flutter_blue.dart';
import 'package:mockito/mockito.dart';
import 'package:permission_handler/permission_handler.dart';

import '../lib/sensor_board/location_permission_observer.dart';
import '../lib/sensor_board/protocols.dart';

class BtMock extends Mock implements FlutterBlue {
  final StreamController<BluetoothState> stateSc;
  final StreamController<bool> isScanningSc;
  final StreamController<List<ScanResult>> scanResultsSc;
  Stream<BluetoothState>? stateStream;
  Stream<bool>? isScanningStream;
  Stream<List<ScanResult>>? scanResultsStream;

  BtMock()
      : stateSc = StreamController<BluetoothState>(),
        isScanningSc = StreamController<bool>(),
        scanResultsSc = StreamController<List<ScanResult>>() {
    stateStream = stateSc.stream.asBroadcastStream();
    isScanningStream = isScanningSc.stream.asBroadcastStream();
    scanResultsStream = scanResultsSc.stream.asBroadcastStream();
  }

  @override
  Stream<BluetoothState> get state => stateStream!;
  void setState(BluetoothState value) => stateSc.add(value);

  @override
  Stream<bool> get isScanning => isScanningStream!;
  void setScanning(bool value) => isScanningSc.add(value);

  @override
  Stream<List<ScanResult>> get scanResults => scanResultsStream!;
  void setResults(List<ScanResult> value) => scanResultsSc.add(value);

  @override
  Future startScan(
          {scanMode = ScanMode.lowLatency,
          withServices = const [],
          withDevices = const [],
          timeout,
          allowDuplicates = false}) =>
      super.noSuchMethod(
        Invocation.method(#startScan, []),
        returnValue: Future.sync(() => null),
        returnValueForMissingStub: Future.sync(() => null),
      );

  @override
  Future stopScan() => super.noSuchMethod(Invocation.method(#stopScan, []),
      returnValue: Future.sync(() => null),
      returnValueForMissingStub: Future.sync(() => null));

  @override
  Future<List<BluetoothDevice>> get connectedDevices =>
      super.noSuchMethod(Invocation.getter(#connectedDevices),
          returnValue: Future.sync(() => <BluetoothDevice>[]),
          returnValueForMissingStub: Future.sync(() => <BluetoothDevice>[]));
}

class BtDeviceMock extends Mock implements BluetoothDevice {
  final DeviceIdentifier id;
  final String name;
  final BluetoothDeviceType type;
  final StreamController<BluetoothDeviceState> stateSc;
  Stream<BluetoothDeviceState>? stateStream;

  BtDeviceMock(this.id, this.name, this.type)
      : stateSc = StreamController<BluetoothDeviceState>() {
    stateStream = stateSc.stream.asBroadcastStream();
  }

  @override
  Future<void> connect({bool? autoConnect, Duration? timeout}) =>
      super.noSuchMethod(
          Invocation.method(
              #connect, [], {#autoConnect: autoConnect, #timeout: timeout}),
          returnValue: Future.sync(() => null),
          returnValueForMissingStub: Future.sync(() => null));

  @override
  Stream<BluetoothDeviceState> get state => stateStream!;
  void setState(BluetoothDeviceState value) => stateSc.add(value);
}

class FakeAdvData extends Fake implements AdvertisementData {}

class BtScanResultStub implements ScanResult {
  final BluetoothDevice? _device;
  final AdvertisementData? _advertisementData;
  final int rssi;

  static final mockDevice = BtDeviceMock(DeviceIdentifier("AA:BB:CC:DD:EE:FF"),
      "Mock Bluetooth device", BluetoothDeviceType.dual);
  static final fakeAdvData = FakeAdvData();

  get device => _device ?? mockDevice;
  get advertisementData => _advertisementData ?? fakeAdvData;

  BtScanResultStub(
      {BluetoothDevice? device,
      AdvertisementData? advertisementData,
      this.rssi = -20})
      : _device = device,
        _advertisementData = advertisementData;
}

class LocationPermissionMock extends Mock
    implements LocationPermissionObserver {
  @override
  Stream<PermissionStatus> get stream => super.noSuchMethod(
        Invocation.getter(#stream),
        returnValue: Stream<PermissionStatus>.empty(),
        returnValueForMissingStub: Stream<PermissionStatus>.empty(),
      );
  void setState(PermissionStatus value) =>
      when(stream).thenAnswer((_) => Stream.value(value));
}

class SensorBoardConnectionControllerMock extends Mock
    implements SensorBoardConnectionController {
  final StreamController<SensorBoardState> sbStateSc;
  late final Stream<SensorBoardState> sbState;

  SensorBoardConnectionControllerMock()
      : sbStateSc = StreamController<SensorBoardState>() {
    sbState = sbStateSc.stream.asBroadcastStream();
  }
  @override
  Stream<SensorBoardState> get status => sbState;
  void setState(SensorBoardState value) => sbStateSc.add(value);
}

class BluetoothDeviceMock extends Mock implements BluetoothDevice {}
