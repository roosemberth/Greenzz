# Server for Greenzz

## Producing a JAR

The following command can be used to produce a standalone JAR.

```console
mvn clean compile package -Dmaven.test.skip=true
```

## Running the server

This section describes the bare minimum required to operate the server.
It is highly discouraged for production or production-like (e.g. staging)
deployments.
If you're looking for a production-grade deployment example, you may want
to take a look at [_Roosembert's production deployment_][roos-greenzz-prod].

[roos-greenzz-prod]: https://gitlab.com/roosemberth/dotfiles/-/blob/master/nix/machines/containers/greenzz-prod.nix

### Prerequisites

Running the server requires both an influxdb and a PostgreSQL database.

The influxdb database may be provisioned using a [docker container][docker]:

```console
docker run --rm -p 8086:8086 --name influxdb-local influxdb:1.8.5
```

This will run an influxdb database for you and expose it on port 8086 of your
local machine.

[docker]: https://www.docker.com/resources/what-container

If a PostgreSQL database is not available, an in-memory database may be used
instead, though highly discouraged.

### Executing the application

The following command may be used to start the server:

```console
java -jar target/server-*.jar \
    --db.url=<dbhost>:<dbport>/<database>\
    --db.username=<username> \
    --db.password=<password> \
    --influx.url=<influxurl> \
    --influx.username=<influx-username> \
    --influx.password=<influx-password>
```

Example

```console
java -jar target/server-*.jar \
    --db.url=localhost:5432/myplants\
    --db.username=someuser \
    --db.password=placeholder-password \
    --influx.url=http://localhost:8086 \
    --influx.username=myinfluxuser \
    --influx.password=placeholder-password
```

The server run automatically the sql scripts **schema.sql** then **data.sql**
on the PostgreSQL database when it starts.

This create all the table and data it needs to works.

### Running the tests

To run the tests, you only require an influxdb database.
By default, the test configuration assumes the influxdb database can be reached
at <http://localhost:8086> with username `admin` and password `root`.
This is so it will work out of the box with the docker image explained in
[section](#prerequisites);

[Spring boot][spring] will provide an [embedded database][embedded-db] for the
tests to run with.
You can also provide a PostgreSQL database (at your option, but highly
recommended), but mind that you may have to manually delete and recreate the
database if you intend to develop on it.

[spring]: https://spring.io/projects/spring-boot
[embedded-db]: https://docs.spring.io/spring-session/docs/1.3.0.RELEASE/reference/html5/guides/httpsession-jdbc-boot.html#httpsession-jdbc-boot-configuration

#### Examples

Running with the embedded database:

```console
mvn clean test
```

Running with the embedded database and an influx database in another machine:

```console
mvn clean test \
    -Dgreenzz.datasource.influx.url=http://anotherHost:8086 \
    -Dgreenzz.datasource.influx.username=someUser \
    -Dgreenzz.datasource.influx.password=somePassword \
```

Running a specific test in the test suite:

```console
mvn clean test -Dtest='*#userRegistrationTest'
```

Running with an external PostgreSQL database.

```console
mvn clean test \
    -Dspring.datasource.url=jdbc:postgresql://localhost:5432/foo \
    -Dspring.datasource.username=anotherUser \
    -Dspring.datasource.password=anotherPassword \
    -Dspring.jpa.properties.hibernate.hbm2ddl.auto=none \
```

### Running the server during development

To ease the development cycle, the server may be ran directly from `mvn`.
It is recommended that you include the `clean` command every time you run to
avoid yourself some headaches, but you can remove it to speed up bootstrap time.

The same prerequisites as running in production apply.

Example command:

```console
mvn clean spring-boot:run -Dspring-boot.run.arguments="\
    --db.url=localhost:5432/myplants\
    --db.username=user\
    --db.password=pass\
    --influx.url=http://localhost:8086\
    --influx.username=admin\
    --influx.password=root\
    "
```

(For the eagle eye, this is effectively putting all arguments inside a single
argument to maven, hence any quotes inside must be escaped.)

If a PostgreSQL database is not available, an in-memory database may be used
(the same as for the tests), but this is highly discouraged:

```console
mvn spring-boot:run -Dspring-boot.run.arguments="\
    --db.url= --db.username= --db.password=\
    --influx.url=http://localhost:8086\
    --influx.username=admin\
    --influx.password=root\
    --spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1\
    "
```
