package ch.greenzz.server.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.TreeSet;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import ch.greenzz.server.api.PlantApi;
import ch.greenzz.server.api.SensorApi;
import ch.greenzz.server.api.UserApi;
import ch.greenzz.server.db.PlantProfile;
import ch.greenzz.server.db.PlantProfile.PlantStatus;
import ch.greenzz.server.id.PlantID;
import ch.greenzz.server.id.UserID;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PlantServiceTest {
    @LocalServerPort
    int port;

    UserApi api;
    SensorApi sApi;
    PlantApi pApi;

    @BeforeEach
    public void setupApi() throws MalformedURLException {
        URL url = new URL("http://localhost:" + port + "/rpc");
        JsonRpcHttpClient client = new JsonRpcHttpClient(url);
        api = RpcUtilsTest.createProxyFromClient("user",
                getClass().getClassLoader(), UserApi.class, client);
        sApi = RpcUtilsTest.createProxyFromClient("sensor",
                getClass().getClassLoader(), SensorApi.class, client);
        pApi = RpcUtilsTest.createProxyFromClient("plant",
                getClass().getClassLoader(), PlantApi.class, client);
    }

    @Test
    public void plantRegistrationTest() {
        UserID user = api.register("UserPlant123");
        String expected = "123e4567-e89b-12d3-a456-426614174000";
        PlantID registered = pApi.registerPlant(user.getUserId(), expected);
        assertEquals(expected, registered.getUuid());
    }

    @Test
    public void registerPlantProfile() {
        UserID user = api.register("UserPlant123-lakjs");
        String uuid = "d6b18518-6f59-4c49-8e22-b12d2f0a2f12";
        pApi.registerPlant(user.getUserId(), uuid);

        Set<String> tags = new TreeSet<>();
        tags.add("Garden");
        LocalDateTime birthdate = LocalDateTime.now().withNano(0);
        String expectedName = "Étoile de Noël";
        PlantProfile pp = PlantProfile.builder().name(expectedName)
                .plantUuid(uuid).created(LocalDateTime.now()).tags(tags)
                .status(PlantStatus.NEUTRAL).approximateBirthday(birthdate)
                .build();
        pApi.updatePlantProfile(uuid, pp);
        PlantProfile created = pApi.getPlantProfile(uuid);
        assertEquals(expectedName, created.getName());
        assertEquals(uuid, created.getPlantUuid());
        assertEquals(birthdate, created.getApproximateBirthday());
        long secondsSinceCreated = ChronoUnit.SECONDS
                .between(LocalDateTime.now(), created.getCreated());
        assertTrue(secondsSinceCreated < 5,
                "Profile automatically created less than 5 secs ago.");
        long secondsSinceModified = ChronoUnit.SECONDS
                .between(LocalDateTime.now(), created.getLastModified());
        assertTrue(secondsSinceModified < 5,
                "Profile automatically modified less than 5 secs ago.");
    }

    @Test
    public void updatePlantProfile() throws Exception {
        UserID user = api.register("UserPlant23-hqwoie");
        String uuid = "59d9fdde-4c60-4319-8955-49d2c8a22d2c";
        pApi.registerPlant(user.getUserId(), uuid);

        LocalDateTime birthdate = LocalDateTime.now().withNano(0);
        PlantProfile pp = PlantProfile.builder().name("name")
                .status(PlantStatus.NEUTRAL).approximateBirthday(birthdate)
                .build();
        pApi.updatePlantProfile(uuid, pp);
        PlantProfile created = pApi.getPlantProfile(uuid);

        birthdate = LocalDateTime.now().withNano(0);
        Set<String> tags = new TreeSet<>();
        tags.add("Garden");
        String expectedName = "Étoile de Noël";
        PlantProfile updateReq = created.toBuilder().tags(tags)
                .name(expectedName).approximateBirthday(birthdate).build();
        pApi.updatePlantProfile(uuid, updateReq);
        PlantProfile updated = pApi.getPlantProfile(uuid);

        assertEquals(expectedName, updated.getName());
        assertEquals(uuid, updated.getPlantUuid());
        assertEquals(birthdate, updated.getApproximateBirthday());
        Thread.sleep(3000);
        assertEquals(created.getCreated(), updated.getCreated());
        long secondsSinceModified = ChronoUnit.SECONDS
                .between(LocalDateTime.now(), updated.getLastModified());
        assertTrue(secondsSinceModified < 5,
                "Profile automatically modified less than 5 secs ago.");
    }

    @Test
    public void unexistingPlantProfileForRegisteredPlant() {
        UserID user = api.register("UserPlant123-18kj");
        String uuid = "8746e61f-359a-4dad-9bd1-e3f6d79cadd8";
        PlantID registered = pApi.registerPlant(user.getUserId(), uuid);
        assertEquals(uuid, registered.getUuid());
        assertEquals(null, pApi.getPlantProfile(uuid));
    }
}
