package ch.greenzz.server.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import ch.greenzz.server.db.Sensor;

public class SensorTest {
    @Test
    public void testSensorMacIsUpperCase() {
        String m1 = "00:b0:00:00:77:0a";
        String m2 = "f8:b5:a4:af:77:ba";
        assertEquals(m1.toUpperCase(),
                Sensor.builder().macaddr(m1).build().getMacaddr());
        Sensor s = new Sensor();
        s.setMacAddr(m2);
        assertEquals(m2.toUpperCase(), s.getMacaddr());
    }
}
