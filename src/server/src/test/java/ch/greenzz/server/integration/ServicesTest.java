package ch.greenzz.server.integration;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Base64;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.annotation.PreDestroy;

import ch.greenzz.server.data.SpeciesPreferences;
import ch.greenzz.server.db.PlantsDatabaseRepository;
import ch.greenzz.server.error.PlantDoesNotExist;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import ch.greenzz.server.Server;
import ch.greenzz.server.api.PlantApi;
import ch.greenzz.server.api.SensorApi;
import ch.greenzz.server.api.UserApi;
import ch.greenzz.server.data.SensorInfluxRegistration;
import ch.greenzz.server.db.SensorRepository;
import ch.greenzz.server.db.User;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.SensorBoardDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;
import ch.greenzz.server.id.PlantID;
import ch.greenzz.server.id.SensorID;
import ch.greenzz.server.id.UserID;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@ContextConfiguration(classes = { Server.class,
        ServicesTest.RemoveTmpdirForDailyPhotosCtxConfiguration.class//
}, initializers = ServicesTest.TmpdirForDailyPhotosCtxInitializer.class)
public class ServicesTest {
    @LocalServerPort
    int port;

    @Autowired
    SensorRepository sensorRepository;
    @Autowired
    PlantsDatabaseRepository plantsDatabaseRepository;
    UserApi api;
    SensorApi sApi;
    PlantApi pApi;

    @BeforeEach
    public void setupApi() throws MalformedURLException {
        URL url = new URL("http://localhost:" + port + "/rpc");
        JsonRpcHttpClient client = new JsonRpcHttpClient(url);
        api = RpcUtilsTest.createProxyFromClient("user",
                getClass().getClassLoader(), UserApi.class, client);
        sApi = RpcUtilsTest.createProxyFromClient("sensor",
                getClass().getClassLoader(), SensorApi.class, client);
        pApi = RpcUtilsTest.createProxyFromClient("plant",
                getClass().getClassLoader(), PlantApi.class, client);
    }

    @Test
    public void plantRegistrationTestWithNonRegisteredUserShouldThrow() {
        User user = new User();
        user.setId(43092);
        UserID userId = new UserID(user);

        UserDoesNotExist thrown = assertThrows(UserDoesNotExist.class,
                () -> pApi.registerPlant(userId.getUserId(),
                        "123e4567-e89b-12d3-a456-426614174001"),
                "Registering a plant with an unkonwn user should throw.");
        assertTrue(thrown.getMessage().matches(".*user.*could not be found.*"),
                "Error message explains user could not be found.");
    }

    @Test
    public void plantRegistrationTestWithIncorrectFormatUUIDShouldThrow() {
        UserID user = api.register("UserPlant1");

        InvalidParams thrown = assertThrows(InvalidParams.class,
                () -> pApi.registerPlant(user.getUserId(), "invalid uuid"),
                "Registering an invalid plant UUID should throw.");
        assertTrue(thrown.getMessage().contains("UUID is not valid"),
                "Error message explains UUID is not valid.");
    }

    @Test
    public void plantRegistrationTestWithAlreadyRegisteredPlantShouldThrow() {
        UserID user1 = api.register("UserPlant2");
        UserID user2 = api.register("UserPlant3");
        String expected = "123e4567-e89b-12d3-a456-426614174200";
        pApi.registerPlant(user1.getUserId(), expected);

        InvalidParams thrown = assertThrows(InvalidParams.class,
                () -> pApi.registerPlant(user2.getUserId(), expected),
                "Registering a plant with the same UUID should throw.");
        assertTrue(thrown.getMessage().matches(".*plant.*uuid.*exists.*"),
                "Error message explains a plant with the same UUID exists.");
    }

    @Test
    public void getPlantsTestWithNonRegisteredUserShouldThrow() {
        User user = new User();
        user.setId(100);
        UserID userId = new UserID(user);

        UserDoesNotExist thrown = assertThrows(UserDoesNotExist.class,
                () -> pApi.getPlants(userId.getUserId()),
                "Requesting plants of an unknown user should throw.");
        assertTrue(
                thrown.getMessage().matches(".*user .* could not be found.*"),
                "Error message explains user could not be found.");
    }

    @Test
    public void getPlantsTestFromOwnerWithNoPlantShouldReturnEmptyCollection() {
        UserID user = api.register("UserPlant4");
        String uID = user.getUserId();
        assertTrue(pApi.getPlants(uID).length == 0, "User has no plants");
    }

    @Test
    public void getPlantsForUnexistingUserThrows() {
        UserDoesNotExist e = assertThrows(UserDoesNotExist.class,
                () -> pApi.getPlants("100"),
                "Plant for non-existing user should throw.");
        assertTrue(e.getMessage().matches(".*user .* could not be found.*"),
                "Error message explains ID does not exist.");
    }

    @Test
    public void getPlantsTestShouldReturnUserPlants() {
        UserID user = api.register("UserPlant5");
        String uID = user.getUserId();
        String uuid1 = "123e4567-e89b-12d3-a456-426614174100";
        String uuid2 = "123e4567-e89b-12d3-a456-426614174101";
        String uuid3 = "123e4567-e89b-12d3-a456-426614174102";

        pApi.registerPlant(uID, uuid1);
        pApi.registerPlant(uID, uuid2);
        pApi.registerPlant(uID, uuid3);

        PlantID[] ids = pApi.getPlants(uID);
        String[] result = new String[3];
        for (int i = 0; i < 3; i++) {
            result[i] = ids[i].getUuid();
        }

        String[] expected = { uuid1, uuid2, uuid3 };
        assertArrayEquals(expected, result);
    }

    @Test
    public void getSpeciesDetailsTest() {
        String species = "Ageratum spp";
        String expected =
                "Flowering period June-September\n" +
                "Diameter ≥ 10 cm, height 15-50 cm\n" +
                "Peat or soil with specific nutrients\n" +
                "Put in sunny places, shade properly under " +
                "strong light\n" +
                "Water thoroughly after soil surface dries\n" +
                "Dilute fertilizers following " +
                "instructions,  apply 1-2 times monthly\n" +
                "Remove withered flowers, dead and diseased leaves timely\n";
        assertEquals(expected, pApi.getSpeciesDetails(species));
    }

    @Test
    public void getSpeciesDetailsWithUnknownSpeciesShouldThrow() {
        PlantDoesNotExist e = assertThrows(PlantDoesNotExist.class,
                () -> pApi.getSpeciesDetails("ahsbdhda"),
                "Unknown plant species should throw.");
        assertTrue(e.getMessage().matches("Species not found in database."),
                "Error message explains this species is not in database.");
    }

    @Test
    public void getSpeciesPreferencesTest() {
        String species = "Ferocactus emoryi";
        SpeciesPreferences spExpected = new SpeciesPreferences(5,35,
                3000,100000,7,50);
        SpeciesPreferences sp = pApi.getSpeciesPreferences(species);
        assertEquals(spExpected.getMaxLight(),sp.getMaxLight());
        assertEquals(spExpected.getMinLight(),sp.getMinLight());
        assertEquals(spExpected.getMaxTemp(),sp.getMaxTemp());
        assertEquals(spExpected.getMinTemp(),sp.getMinTemp());
        assertEquals(spExpected.getMaxMoist(),sp.getMaxMoist());
        assertEquals(spExpected.getMinMoist(),sp.getMinMoist());
    }

    @Test
    public void getSpeciesPreferencesWithUnknownSpeciesShouldThrow() {
        PlantDoesNotExist e = assertThrows(PlantDoesNotExist.class,
                () -> pApi.getSpeciesPreferences("ahsbdhda"),
                "Unknown plant species should throw.");
        assertTrue(e.getMessage().matches("Species not found in database."),
                "Error message explains this species is not in database.");
    }

    @Test
    public void getSpeciesListTest() {
        long nbOfRows = plantsDatabaseRepository.count();
        assertEquals(5, nbOfRows);
        String[] arrExpected = new String[]{"Ageratum spp","Ferocactus emoryi", "Astilbe x",
        "Tulipa 'fun for two'", "Betula spp."};
        String[] arr = pApi.getSpeciesList();
        Arrays.sort(arr);
        Arrays.sort(arrExpected);
        assertArrayEquals(arrExpected, arr);
    }

    @Test
    public void userRegistrationTest() {
        UserID id = api.register("TestUser");
        assertEquals(id.getUserId(),
                api.resolveUsername("TestUser").getUserId());

    }

    @Test
    public void registerAndGetSensorsTest() {
        // Create 2 users
        UserID user1 = api.register("TestUser1-9e4834bf");
        UserID user2 = api.register("TestUser2-9e4834bf");
        String sUser1 = user1.getUserId();
        String sUser2 = user2.getUserId();
        String pid1 = "123e4567-e8ca-12d3-a456-426614174100";
        String pid2 = "123e4567-e8ca-12d3-a456-426614174101";
        String pid3 = "123e4567-e8ca-12d3-a456-426614174102";
        String pid4 = "123e4567-e8ca-12d3-a456-426614174103";
        String pid5 = "123e4567-e8ca-12d3-a456-426614174104";

        pApi.registerPlant(sUser1, pid1);
        pApi.registerPlant(sUser1, pid2);
        pApi.registerPlant(sUser1, pid3);
        pApi.registerPlant(sUser2, pid4);
        pApi.registerPlant(sUser2, pid5);

        // Register 3 sensors to TestUser1 and 2 sensors to TestUser2
        SensorInfluxRegistration reg1 = //
                sApi.registerSensor(sUser1, pid1, "5E:FF:56:A2:AF:15");
        SensorInfluxRegistration reg2 = //
                sApi.registerSensor(sUser1, pid2, "5E:FF:56:A2:AF:14");
        SensorInfluxRegistration reg3 = //
                sApi.registerSensor(sUser1, pid3, "5E:FF:56:A2:AF:13");
        SensorInfluxRegistration reg4 = //
                sApi.registerSensor(sUser2, pid4, "5E:FF:56:A2:AF:16");
        SensorInfluxRegistration reg5 = //
                sApi.registerSensor(sUser2, pid5, "5E:FF:56:A2:AF:17");

        String[] sensorsUser1 = { reg1.getSensorId(), reg2.getSensorId(),
                reg3.getSensorId() };
        String[] sensorsUser2 = { reg4.getSensorId(), reg5.getSensorId() };

        Function<SensorID[], String[]> getIds = sensors -> Arrays
                .stream(sensors).map(s -> s.getSensorId())
                .toArray(l -> new String[l]);

        Function<SensorInfluxRegistration[], String[]> getInfluxDbNames = //
                regs -> Arrays.stream(regs).map(r -> r.getInfluxDatabase())
                        .toArray(l -> new String[l]);

        BiFunction<String[], String, String[]> queryAccessDbs = (plantIds,
                userId) -> Arrays.stream(plantIds)
                        .map(pid -> api.getInfluxCredentials(userId, pid)
                                .getInfluxDatabase())
                        .toArray(l -> new String[l]);

        String[] expectedInfluxDbNamesU1 = getInfluxDbNames
                .apply(new SensorInfluxRegistration[] { reg1, reg2, reg3 });
        String[] expectedInfluxDbNamesU2 = getInfluxDbNames
                .apply(new SensorInfluxRegistration[] { reg4, reg5 });
        String[] queriedInfluxDbNamesU1 = queryAccessDbs.apply( //
                new String[] { pid1, pid2, pid3 }, sUser1);
        String[] queriedInfluxDbNamesU2 = queryAccessDbs.apply( //
                new String[] { pid4, pid5 }, sUser2);

        assertArrayEquals(expectedInfluxDbNamesU1, queriedInfluxDbNamesU1);
        assertArrayEquals(expectedInfluxDbNamesU2, queriedInfluxDbNamesU2);

        assertThrows(SensorBoardDoesNotExist.class,
                () -> api.getInfluxCredentials(sUser1, pid5));

        assertArrayEquals(sensorsUser1, getIds.apply(api.getSensors(sUser1)));
        assertArrayEquals(sensorsUser2, getIds.apply(api.getSensors(sUser2)));
    }

    @Test
    public void userUploadsDailyPhoto() {
        UserID user = api.register("UserPlant10923");
        String plantUuid = "5d291d58-16ce-43c7-bb21-7eb31c6d774f";
        pApi.registerPlant(user.getUserId(), plantUuid);
        // https://drj11.wordpress.com/2007/11/14/using-the-rfc-2397-data-url-scheme-to-micro-optimise-small-images
        String rfc2397EncodedPng = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgAQMAAADYVuV7AAAABlBMVEUAAAD///+l2Z/dAAAAIUlEQVQ4jWP4DwMMQDDKIZHDAAGjnFEO7TkMcDDKIZYDAAVlPd9Ahj+EAAAAAElFTkSuQmCC";

        byte[] imgExpected = Base64.getDecoder()
                .decode(rfc2397EncodedPng.replaceFirst("data:.+,", ""));

        String timestamp = LocalDateTime.now(ZoneOffset.UTC).format(
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"));
        String url = "http://localhost:" + port + "/dailyPhotos/" + plantUuid
                + "/" + timestamp;

        MultipartBodyBuilder multipartBuilder = new MultipartBodyBuilder();
        multipartBuilder.part("file", imgExpected).header("Content-Disposition",
                "form-data; name=file; filename=-");

        WebTestClient.bindToServer().baseUrl(url).build().post()
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(BodyInserters.fromMultipartData(multipartBuilder.build()))
                .exchange().expectStatus().isCreated();

        String[] photoTimestamps = pApi.getPhotoTimestamps(plantUuid);
        assertArrayEquals(new String[] { timestamp }, photoTimestamps);

        WebTestClient.bindToServer().baseUrl(url).build().get().exchange()
                .expectStatus().isOk().expectHeader()
                .contentDisposition(ContentDisposition.attachment()
                        .filename(timestamp).build())
                .expectBody().equals((Object) imgExpected);

        WebTestClient.bindToServer().baseUrl(url).build().delete().exchange()
                .expectStatus().isNoContent();

        WebTestClient.bindToServer().baseUrl(url).build().get().exchange()
                .expectStatus().isNotFound();
    }

    static class TmpdirForDailyPhotosCtxInitializer implements
            ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext ctx) {
            String tmpdirPath;
            try {
                tmpdirPath = Files.createTempDirectory("greenzz-test").toFile()
                        .getAbsolutePath();
            } catch (Exception e) {
                throw new RuntimeException(
                        "Failed to create temporary directory for tests.", e);
            }
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(ctx,
                    "ch.greenzz.dailyPhotos.storagePath=" + tmpdirPath);
        }
    }

    static class RemoveTmpdirForDailyPhotosCtxConfiguration {
        private Logger logger = LoggerFactory.getLogger(ServicesTest.class);

        @Value("${ch.greenzz.dailyPhotos.storagePath}")
        private String dailyPhotosStoragePath; // Used on to clean it up

        @PreDestroy
        void cleanupTmpdirForDailyPhotos() {
            logger.info("Removing daily photos temporary directory.");
            Paths.get(dailyPhotosStoragePath).toFile().delete();
        }
    }
}
