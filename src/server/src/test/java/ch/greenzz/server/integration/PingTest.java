package ch.greenzz.server.integration;

import ch.greenzz.server.api.Ping;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PingTest {

    @LocalServerPort
    int port;

    Ping api;

    @BeforeEach
    public void setupApi() throws MalformedURLException {
        URL url = new URL("http://localhost:" + port + "/rpc");
        JsonRpcHttpClient client = new JsonRpcHttpClient(url);
        api = RpcUtilsTest.createProxyFromClient("ping",
                getClass().getClassLoader(), Ping.class, client);
    }

    @Test
    public void statusReturn200() {
        assertEquals("ping", api.ping());
    }
}
