CREATE TABLE IF NOT EXISTS users (
    id serial,
    date timestamp without time zone NOT NULL,
    ip_addr varchar(50) NOT NULL,
    influxuser varchar(30),
    influxpassword varchar(255),
    username varchar(20) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS sensors (
    idsensor serial,
    macaddr varchar(50) NOT NULL UNIQUE,
    plantid varchar (50) NOT NULL,
    disable boolean NOT NULL,
    owner INTEGER NOT NULL,
    influxuser varchar(50) NOT NULL,
    influxdb varchar(255) NOT NULL,
    PRIMARY KEY(idsensor)
);

CREATE TABLE IF NOT EXISTS plants (
    uuid varchar (36) NOT NULL,
    owner INTEGER NOT NULL,
    PRIMARY KEY(uuid)
);

CREATE TABLE IF NOT EXISTS daily_photos (
    plant_uuid varchar (36) NOT NULL,
    photo_timestamp timestamp NOT NULL,
    PRIMARY KEY(plant_uuid, photo_timestamp)
);

CREATE TABLE IF NOT EXISTS plants_database(
    pid             TEXT NOT NULL,
    display_pid     TEXT NOT NULL,
    alias           TEXT NOT NULL,
    image           TEXT NOT NULL,
    floral_language TEXT,
    origin          TEXT NOT NULL,
    production      TEXT NOT NULL,
    category        TEXT NOT NULL,
    blooming        TEXT NOT NULL,
    color           TEXT NOT NULL,
    size            TEXT NOT NULL,
    soil            TEXT NOT NULL,
    sunlight        TEXT NOT NULL,
    watering        TEXT NOT NULL,
    fertilization   TEXT NOT NULL,
    pruning         TEXT NOT NULL,
    max_light_mmol  INTEGER  NOT NULL,
    min_light_mmol  INTEGER  NOT NULL,
    max_light_lux   INTEGER  NOT NULL,
    min_light_lux   INTEGER  NOT NULL,
    max_temp        INTEGER  NOT NULL,
    min_temp        INTEGER  NOT NULL,
    max_env_humid   INTEGER  NOT NULL,
    min_env_humid   INTEGER  NOT NULL,
    max_soil_moist  INTEGER  NOT NULL,
    min_soil_moist  INTEGER  NOT NULL,
    max_soil_ec     INTEGER  NOT NULL,
    min_soil_ec     INTEGER  NOT NULL,
    PRIMARY KEY (pid)
);

CREATE TABLE IF NOT EXISTS plant_profiles (
    plant_uuid varchar (36) NOT NULL,
    name varchar(50) NOT NULL,
    approximate_birthday timestamp NOT NULL,
    status integer NOT NULL,
    last_modified timestamp NOT NULL,
    created timestamp NOT NULL,
    PRIMARY KEY(plant_uuid)
);

CREATE TABLE IF NOT EXISTS plant_tags (
    plant_uuid varchar (36) NOT NULL,
    tag varchar (50) NOT NULL,
    PRIMARY KEY(plant_uuid, tag)
);

ALTER TABLE plant_profiles
ADD FOREIGN KEY (plant_uuid) references plants(uuid);

ALTER TABLE plant_tags
ADD FOREIGN KEY (plant_uuid) references plant_profiles(plant_uuid);

ALTER TABLE sensors
ADD FOREIGN KEY (owner) references users(id);

ALTER TABLE sensors
ADD FOREIGN KEY (plantid) references plants(uuid);

ALTER TABLE plants
ADD FOREIGN KEY (owner) references users(id);

ALTER TABLE daily_photos
ADD FOREIGN KEY (plant_uuid) references plants(uuid);
