package ch.greenzz.server.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DailyPhotoStorageController {
    private final DateTimeFormatter imgTimestampFormat = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH_mm_ss'Z'");

    @Value("${greenzz.datasource.dailyPhotos.path}")
    private String storagePath;

    public void store(MultipartFile file, String plantUuid,
                      LocalDateTime timestamp) throws IOException {
        Path target = getPath(plantUuid, timestamp);
        target.getParent().toFile().mkdirs();
        try (OutputStream os = new FileOutputStream(target.toFile())) {
            os.write(file.getBytes());
            os.flush();
        }
    }

    public Path getPath(String plantUuid, LocalDateTime timestamp) {
        String resourcePath = timestamp.format(imgTimestampFormat);
        return Paths.get(storagePath).resolve(plantUuid).resolve(resourcePath)
                .toAbsolutePath();
    }
}