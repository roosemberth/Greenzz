package ch.greenzz.server.api;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;

import ch.greenzz.server.data.SensorInfluxRegistration;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;
import ch.greenzz.server.error.SensorBoardDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;

public interface SensorApi {
    @JsonRpcErrors({
      @JsonRpcError(exception=UserDoesNotExist.class, code=-32001),
      @JsonRpcError(exception=SensorBoardDoesNotExist.class, code = -32002),
      @JsonRpcError(exception=PlantDoesNotExist.class, code=-32003),
      @JsonRpcError(exception=InvalidParams.class, code=-32602),
    })
    SensorInfluxRegistration registerSensor(String userID, String plantID,
            String macAddr);
}
