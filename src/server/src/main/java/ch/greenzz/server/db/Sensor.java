package ch.greenzz.server.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "Sensors")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Sensor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idsensor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner")
    private User user;

    @Column(nullable = false)
    private String influxdb;

    @Column(nullable = false)
    private String influxuser;

    @Column(nullable = false)
    private String macaddr;

    @Column(nullable = false)
    private String plantID;

    @Builder.Default
    private boolean disable = false;

    public void setMacAddr(String newMac) {
        this.macaddr = newMac.toUpperCase();
    }

    public static class SensorBuilder {
        public SensorBuilder macaddr(String newMac) {
            this.macaddr = newMac.toUpperCase();
            return this;
        }
    }

    @Override
    public String toString() {
        return "Sensor [id=" + idsensor + ", user=" + user + ", plantID="
                + plantID + ", macaddr=" + macaddr + ", disable=" + disable
                + "]";
    }
}
