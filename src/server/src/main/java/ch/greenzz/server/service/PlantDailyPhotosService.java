package ch.greenzz.server.service;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import ch.greenzz.server.db.Plant;
import ch.greenzz.server.db.PlantRepository;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;

@Controller
public class PlantDailyPhotosService {
    private Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private PlantRepository plantRepository;
    @Autowired
    private HttpServletRequest httpContext;
    @Autowired
    private DailyPhotoStorageController storageController;

    public static final DateTimeFormatter imgTimestampFormat = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    @PostMapping("/dailyPhotos/{plantUuid}/{timestamp}")
    public ResponseEntity<Object> handleUpload(
            @RequestParam("file") MultipartFile file,
            @PathVariable String plantUuid, @PathVariable String timestamp)
            throws IOException {
        try {
            UUID.fromString(plantUuid);
        } catch (IllegalArgumentException e) {
            throw new InvalidParams("The specified UUID is not valid.");
        }
        LocalDateTime parsedTimestamp = parseRequestTimestamp(timestamp);

        Plant plant = plantRepository.findByUuid(plantUuid)
                .orElseThrow(() -> new PlantDoesNotExist(
                        "No plant with the specified UUID could be found."));
        String ipAddr = getClientIpAddress(httpContext);
        String normalizedTimestamp = parsedTimestamp.format(imgTimestampFormat);
        logger.info("Receiving daily photo for " + plantUuid + "@"
                + normalizedTimestamp + " from " + ipAddr);
        storageController.store(file, plantUuid, parsedTimestamp);
        plant.getDailyPhotos().add(parsedTimestamp);
        plantRepository.save(plant);

        return ResponseEntity.created(null).build();
    }

    @GetMapping("/dailyPhotos/{plantUuid}/{timestamp}")
    @ResponseBody
    public ResponseEntity<Resource> serveFile(@PathVariable String plantUuid,
            @PathVariable String timestamp) throws Exception {
        LocalDateTime parsedTimestamp = parseRequestTimestamp(timestamp);
        Path imagePath = storageController.getPath(plantUuid, parsedTimestamp);
        if (!imagePath.toFile().exists())
            return ResponseEntity.notFound().build();
        logger.info("Serving daily photo for " + plantUuid + "@"
                + parsedTimestamp.format(imgTimestampFormat));
        Resource file = new UrlResource(imagePath.toUri());
        String normalizedTimestamp = parsedTimestamp.format(imgTimestampFormat);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" + normalizedTimestamp + "\"")
                .body(file);
    }

    @DeleteMapping("/dailyPhotos/{plantUuid}/{timestamp}")
    @ResponseBody
    public ResponseEntity<Resource> deleteFile(@PathVariable String plantUuid,
            @PathVariable String timestamp) throws Exception {
        try {
            UUID.fromString(plantUuid);
        } catch (IllegalArgumentException e) {
            throw new InvalidParams("The specified UUID is not valid.");
        }

        LocalDateTime parsedTimestamp = parseRequestTimestamp(timestamp);
        Plant plant = plantRepository.findByUuid(plantUuid)
                .orElseThrow(() -> new PlantDoesNotExist(
                        "No plant with the specified UUID could be found."));
        String normalizedTimestamp = parsedTimestamp.format(imgTimestampFormat);
        logger.info("Deleting daily photo for plant " + plantUuid + "@"
                + normalizedTimestamp);
        plant.getDailyPhotos().remove(parsedTimestamp);
        plantRepository.save(plant);

        storageController.getPath(plantUuid, parsedTimestamp).toFile().delete();
        return ResponseEntity.noContent().build();
    }

    private LocalDateTime parseRequestTimestamp(String timestamp) {
        try {
            return LocalDateTime.parse(timestamp, imgTimestampFormat);
        } catch (DateTimeParseException e) {
            throw new InvalidParams("The specified timestamp is not valid. "
                    + "It should be ISO 8601 Date and Time in UTC.");
        }
    }

    private static final String[] FORWARDED_HEADERS_TO_TRY = {
            "X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR", "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP", "HTTP_CLIENT_IP", "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED", "HTTP_VIA", "REMOTE_ADDR" };

    static private String getClientIpAddress(HttpServletRequest request) {
        for (String header : FORWARDED_HEADERS_TO_TRY) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0
                    && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        return request.getRemoteAddr();
    }
}
