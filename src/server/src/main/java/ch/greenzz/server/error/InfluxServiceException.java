package ch.greenzz.server.error;

public class InfluxServiceException extends RuntimeException{
    public InfluxServiceException(String msg) { super(msg); }
}
