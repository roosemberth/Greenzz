package ch.greenzz.server.error;

public class InvalidParams extends RuntimeException {
  public InvalidParams(String msg) {
    super(msg);
  }
}
