package ch.greenzz.server.error;

public class UserDoesNotExist extends RuntimeException {
  public UserDoesNotExist(String msg) {
    super(msg);
  }
}
