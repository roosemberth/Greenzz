package ch.greenzz.server.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SpeciesPreferences {
    int minTemp;
    int maxTemp;
    int minLight;
    int maxLight;
    int minMoist;
    int maxMoist;
}
