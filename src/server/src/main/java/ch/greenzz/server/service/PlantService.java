package ch.greenzz.server.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.greenzz.server.api.PlantApi;
import ch.greenzz.server.data.SpeciesPreferences;
import ch.greenzz.server.db.Plant;
import ch.greenzz.server.db.PlantProfile;
import ch.greenzz.server.db.PlantProfileRepository;
import ch.greenzz.server.db.PlantRepository;
import ch.greenzz.server.db.PlantsDatabase;
import ch.greenzz.server.db.PlantsDatabaseRepository;
import ch.greenzz.server.db.User;
import ch.greenzz.server.db.UserRepository;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;
import ch.greenzz.server.id.PlantID;

@Component
@Transactional
public class PlantService implements PlantApi {
    private Logger logger = LoggerFactory.getLogger(PlantService.class);
    @Autowired
    private PlantRepository plantRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PlantsDatabaseRepository plantsDatabaseRepository;
    @Autowired
    private PlantProfileRepository plantProfileRepository;

    public PlantID registerPlant(String userID, String uuid) {
        int userIDint = Integer.parseInt(userID);
        User newOwner = userRepository.findById(userIDint)
                .orElseThrow(() -> new UserDoesNotExist("A user with the specified ID could not be found."));

        if (getPlantFromUuid(uuid).isPresent())
            throw new InvalidParams("A plant with the specified uuid already exists.");

        logger.info("Registering new plant with UUID " + uuid);
        Plant plant = Plant.builder().uuid(uuid).owner(newOwner).build();
        plantRepository.save(plant);
        return new PlantID(plant);
    }

    public PlantID resolvePlant(String uuid) {
        Plant plant = getPlantFromUuid(uuid)
                .orElseThrow(() -> new PlantDoesNotExist("No plant with the specified UUID could be found."));
        logger.info("Resolving plant with UUID " + uuid);
        return new PlantID(plant);
    }

    public PlantID[] getPlants(String userID) {
        int userIDint;
        try {
            userIDint = Integer.parseInt(userID);
        } catch (NumberFormatException e) {
            throw new InvalidParams("The specified userID is not valid.");
        }

        User owner = userRepository.findById(userIDint)
                .orElseThrow(() -> new UserDoesNotExist("A user with the specified ID could not be found."));

        logger.info("Retrieving plants for user " + owner.getUsername());
        Plant[] plants = plantRepository.getPlantsByOwner(owner);
        PlantID[] plantIDs = new PlantID[plants.length];

        for (int i = 0; i < plantIDs.length; i++) {
            plantIDs[i] = new PlantID(plants[i]);
        }
        return plantIDs;
    }

    public String[] getPhotoTimestamps(String plantUuid) {
        try {
            UUID.fromString(plantUuid);
        } catch (IllegalArgumentException e) {
            throw new InvalidParams("The specified UUID is not valid.");
        }

        Plant plant = plantRepository.findByUuid(plantUuid)
                .orElseThrow(() -> new PlantDoesNotExist("No plant with the specified UUID could be found."));

        logger.info("Listing photos of plant with UUID " + plantUuid);
        List<LocalDateTime> photoTimestamps = plant.getDailyPhotos();
        String[] photoIds = photoTimestamps.stream().map((ts) -> ts.format(PlantDailyPhotosService.imgTimestampFormat))
                .toArray(size -> new String[size]);
        return photoIds;
    }

    public String getSpeciesDetails(String species) {
        PlantsDatabase plant = plantsDatabaseRepository.findByDisplayPid(species)
                .orElseThrow(() -> new PlantDoesNotExist("Species not found in database."));
        StringBuilder res = new StringBuilder();
        logger.info("Retrieving infos for species  " + species);
        res.append(plant.getBlooming()).append("\n").append(plant.getSize()).append("\n").append(plant.getSoil())
                .append("\n").append(plant.getSunlight()).append("\n").append(plant.getWatering()).append("\n")
                .append(plant.getFertilization()).append("\n").append(plant.getPruning()).append("\n");
        return res.toString();
    }

    public SpeciesPreferences getSpeciesPreferences(String species) {
        PlantsDatabase plant = plantsDatabaseRepository.findByDisplayPid(species)
                .orElseThrow(() -> new PlantDoesNotExist("Species not found in database."));
        return new SpeciesPreferences(plant.getMin_temp(), plant.getMax_temp(), plant.getMin_light_lux(),
                plant.getMax_light_lux(), plant.getMin_soil_moist(), plant.getMax_soil_moist());
    }

    public String[] getSpeciesList() {
        logger.info("Retrieving list of all species.");
        List<PlantsDatabase> plantsList = plantsDatabaseRepository.findAll();
        String[] result = new String[plantsList.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = plantsList.get(i).getDisplayPid();
        }
        return result;
    }

    public void updatePlantProfile(String plantUuid, PlantProfile profile) {
        plantRepository.findByUuid(plantUuid)
                .orElseThrow(() -> new PlantDoesNotExist("No plant with the specified UUID could be found."));
        profile.setPlantUuid(plantUuid);
        try {
            plantProfileRepository.save(profile);
        } catch (Exception e) {
            e.printStackTrace(System.err);
            throw new InvalidParams("The specified plant profile is not valid");
        }
    }

    public PlantProfile getPlantProfile(String plantUuid) {
        Plant plant = getPlantFromUuid(plantUuid)
                .orElseThrow(() -> new PlantDoesNotExist("No plant with the specified UUID could be found."));
        return plant.getPlantProfile();
    }

    private Optional<Plant> getPlantFromUuid(String uuid) {
        try {
            UUID.fromString(uuid);
        } catch (IllegalArgumentException e) {
            throw new InvalidParams("The specified UUID is not valid.");
        }
        return plantRepository.findByUuid(uuid);
    }
}
