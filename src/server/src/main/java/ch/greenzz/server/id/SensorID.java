package ch.greenzz.server.id;


import ch.greenzz.server.db.Sensor;
import ch.greenzz.server.db.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SensorID {
    private String sensorId;
    public SensorID(Sensor sensor){
        sensorId = String.valueOf(sensor.getIdsensor());
    }
}
