package ch.greenzz.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.greenzz.server.db.PlantsDatabaseRepository;
import ch.greenzz.server.error.PlantsDatabaseIsEmpty;
import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.jsonrpc4j.InvocationListener;
import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;
import com.googlecode.jsonrpc4j.JsonRpcMultiServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.SimpleThreadScope;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import ch.greenzz.server.api.Ping;
import ch.greenzz.server.api.PlantApi;
import ch.greenzz.server.api.SensorApi;
import ch.greenzz.server.api.UserApi;
import ch.greenzz.server.service.ConnectionInfo;
import ch.greenzz.server.service.PingImpl;
import ch.greenzz.server.service.PlantService;
import ch.greenzz.server.service.SensorService;
import ch.greenzz.server.service.UserService;

@SpringBootApplication
@RestController
@EnableWebSocket
@ComponentScan
public class Server implements WebSocketConfigurer {
    private Logger logger = LoggerFactory.getLogger(Server.class);
    @Autowired
    private ApplicationContext ctx;
    @Autowired
    private UserService userService;
    @Autowired
    private SensorService sensorService;
    @Autowired
    private PlantService plantService;
    @Autowired
    private PlantsDatabaseRepository plantsDatabaseRepository;

    @EnableWebSecurity
    public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.cors().configurationSource(request -> new CorsConfiguration()
                    .applyPermitDefaultValues());
            http.csrf().disable();
        }
    }

    public static void main(String[] args) throws IOException {
        SpringApplication app = new SpringApplication(Server.class);
        app.run(args);
    }

    @Bean
    public JsonRpcMultiServer getJsonRpcMultiServer() {
        JsonRpcMultiServer exporter = new JsonRpcMultiServer();

        // Finer control of logged exceptions
        exporter.setShouldLogInvocationErrors(false);
        exporter.setInvocationListener(
                getUnregisteredExceptionsInvocationListener());
        exporter.addService("ping", new PingImpl(), Ping.class);
        exporter.addService("user", userService, UserApi.class);
        exporter.addService("sensor", sensorService, SensorApi.class);
        exporter.addService("plant", plantService, PlantApi.class);

        long nbSpecies = plantsDatabaseRepository.count();
        if(nbSpecies == 0){
            throw new PlantsDatabaseIsEmpty("No plant species in database.");
        }
        logger.info(nbSpecies + " plant species available.");
        return exporter;
    }

    @Bean
    public static CustomScopeConfigurer customScope() {
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        configurer.addScope("thread", new SimpleThreadScope());
        return configurer;
    }

    @Bean(name = "/rpc")
    public HttpRequestHandler rpcRequestHandler() {
        return new HttpRequestHandler() {
            @Override
            public void handleRequest(HttpServletRequest req,
                    HttpServletResponse res)
                    throws ServletException, IOException {
                ctx.getBean(ConnectionInfo.class)
                        .setUserIP(req.getRemoteAddr());
                ctx.getBean(JsonRpcMultiServer.class).handle(req, res);
                ctx.getBean(ConnectionInfo.class).clear();
            }
        };
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(wsRpcRequestHandler(), "/ws");
    }

    @Bean
    public TextWebSocketHandler wsRpcRequestHandler() {
        return new TextWebSocketHandler() {
            public void handleTextMessage(WebSocketSession session,
                    TextMessage message) throws IOException {
                ctx.getBean(ConnectionInfo.class)
                        .setUserIP(session.getRemoteAddress().getHostString());
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                InputStream in = new ByteArrayInputStream(message.asBytes());
                ctx.getBean(JsonRpcMultiServer.class).handleRequest(in, out);
                session.sendMessage(new TextMessage(out.toByteArray()));
                ctx.getBean(ConnectionInfo.class).clear();
            }
        };
    }

    /**
     * {@link InvocationListener} logging unregistered {@link Exception}s.
     *
     * Returns an RPC {@link InvocationListener} which logs any
     * {@link Exception} thrown by any RPC API service if such is not
     * 'registered' using a {@link JsonRpcError} clause.
     */
    private InvocationListener getUnregisteredExceptionsInvocationListener() {
        return new InvocationListener() {
            @Override
            public void willInvoke(Method method, List<JsonNode> arguments) {
                // Nothing to do here.
            }

            @Override
            public void didInvoke(Method method, List<JsonNode> arguments,
                    Object result, Throwable t, long duration) {
                if (t == null)
                    return;

                Throwable exceptionThrownByApi = t.getCause();

                if (!isRegisteredException(exceptionThrownByApi, method))
                    exceptionThrownByApi.printStackTrace();
            }

            private boolean isRegisteredException(Throwable t, Method method) {
                JsonRpcErrors registeredErrors = method
                        .getAnnotation(JsonRpcErrors.class);
                if (registeredErrors == null)
                    return false;

                for (JsonRpcError registeredError : registeredErrors.value())
                    if (registeredError.exception()
                            .isAssignableFrom(t.getClass()))
                        return true;

                return false;
            }
        };
    }
}
