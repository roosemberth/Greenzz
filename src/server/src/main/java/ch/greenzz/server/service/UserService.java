package ch.greenzz.server.service;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.greenzz.server.api.UserApi;
import ch.greenzz.server.db.InfluxManager;
import ch.greenzz.server.db.Plant;
import ch.greenzz.server.db.PlantRepository;
import ch.greenzz.server.db.Sensor;
import ch.greenzz.server.db.User;
import ch.greenzz.server.db.UserRepository;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;
import ch.greenzz.server.error.SensorBoardDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;
import ch.greenzz.server.id.InfluxCredentials;
import ch.greenzz.server.id.SensorID;
import ch.greenzz.server.id.UserID;

@Component
@Transactional
public class UserService implements UserApi {
    private Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private ApplicationContext ctx;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InfluxManager influxManager;
    @Autowired
    private PlantRepository plantRepository;

    public UserID register(String username) {
        if (!username.matches("[A-Za-z][A-Za-z0-9\\-_.<>]+")
                || username.length() < 3 || username.length() > 18) {
            throw new InvalidParams("The specified username is not valid.");
        }

        if (userRepository.findByUsername(username).isPresent())
            throw new InvalidParams("The specified username cannot be used.");

        logger.info("Registering new user " + username);
        String ipAddr = ctx.getBean(ConnectionInfo.class).getUserIP();
        User user = User.builder().username(username).ipAddr(ipAddr)
                .date(new Timestamp(System.currentTimeMillis())).build();
        userRepository.save(user);
        UserID userID = new UserID(user);
        String[] influxCredentials = influxManager
                .createUser("user-" + userID.getUserId());
        user.setInfluxuser(influxCredentials[0]);
        user.setInfluxpassword(influxCredentials[1]);

        userRepository.save(user);
        return userID;
    }

    public UserID resolveUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserDoesNotExist(
                        "No user with the specified ID could be found."));
        logger.info("Resolving username " + username);
        return new UserID(user);
    }

    public SensorID[] getSensors(String userID) {
        User user = getUser(userID);
        logger.info("Retrieving sensors for " + user.getUsername());
        List<Sensor> sensors = user.getSensors();
        SensorID[] sensorIDS = new SensorID[sensors.size()];
        for (int i = 0; i < sensorIDS.length; i++) {
            sensorIDS[i] = new SensorID(sensors.get(i));
        }
        return sensorIDS;
    }

    public InfluxCredentials getInfluxCredentials(String userID,
            String plantUuid) {
        User user = getUser(userID);
        String databaseName = user.getSensors().stream()
                .filter(s -> s.getPlantID().equals(plantUuid)).findAny()
                .orElseThrow(() -> new SensorBoardDoesNotExist(
                        "The specified plant UUID is not valid."))
                .getInfluxdb();
        String influxApi = influxManager.getServiceUrl();
        return new InfluxCredentials(user.getInfluxuser(),
                user.getInfluxpassword(), influxApi, databaseName);
    }

    private User getUser(String userID) {
        int userIDint;
        try {
            userIDint = Integer.parseInt(userID);
        } catch (NumberFormatException e) {
            throw new InvalidParams("The specified userID is not valid.");
        }
        return userRepository.findById(userIDint)
                .orElseThrow(() -> new UserDoesNotExist(
                        "No user with the specified ID could be found."));

    }
}
