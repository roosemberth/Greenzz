package ch.greenzz.server.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.greenzz.server.api.Ping;

public class PingImpl implements Ping {
    private Logger logger = LoggerFactory.getLogger(UserService.class);

    public String ping() {
        logger.info("Processing ping request!");
        return "ping";
    }
}
