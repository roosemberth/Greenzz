package ch.greenzz.server.api;


import com.googlecode.jsonrpc4j.JsonRpcParam;
import com.googlecode.jsonrpc4j.JsonRpcService;


public interface Ping {
    String ping();

}
