package ch.greenzz.server.error;

public class PlantDoesNotExist extends RuntimeException {
  public PlantDoesNotExist(String msg) {
    super(msg);
  }
}
