package ch.greenzz.server.db;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface PlantProfileRepository
        extends CrudRepository<PlantProfile, String> {
}
