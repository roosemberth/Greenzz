package ch.greenzz.server.db;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ch.greenzz.server.util.LocalDateTimeDeserializer;
import ch.greenzz.server.util.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

@Entity(name = "plant_profiles")
@Jacksonized
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlantProfile {
    public static enum PlantStatus {
        HAPPY, NEUTRAL, UNKNOWN
    };

    @Id
    @Column(name = "plant_uuid")
    private String plantUuid;

    @Column(nullable = false)
    private String name;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "approximate_birthday", nullable = false)
    private LocalDateTime approximateBirthday;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private PlantStatus status;

    @ElementCollection
    @CollectionTable(name = "plant_tags", joinColumns = @JoinColumn(name = "plant_uuid"))
    @Column(name = "tag")
    private Set<String> tags;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Builder.Default
    @Column(name = "last_modified", nullable = false)
    private LocalDateTime lastModified = LocalDateTime.now();

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Builder.Default
    @Column(name = "created", nullable = false)
    private LocalDateTime created = LocalDateTime.now();
}
