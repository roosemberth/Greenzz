package ch.greenzz.server.error;

public class SensorBoardDoesNotExist extends RuntimeException {
  public SensorBoardDoesNotExist(String msg) {
    super(msg);
  }
}
