package ch.greenzz.server.db;

import com.github.kevinsawicki.http.HttpRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ch.greenzz.server.error.InfluxServiceException;

@Component
public class InfluxManager {
    @Value("${greenzz.datasource.influx.username}")
    private String influxUsername;
    @Value("${greenzz.datasource.influx.password}")
    private String influxPassword;
    @Value("${greenzz.datasource.influx.url}")
    private String influxURL;

    /**
     * Returns the database where the sensor will be writing to
     */
    public String createDbForSensor(String macAdress) {
        String dbName = "database-" + macAdress;
        String query = String.format("CREATE DATABASE \"%s\"", dbName);
        makeRequest(query);
        return dbName;
    }

    /**
     * Returns a URL where the influxdb service may be reached
     */
    public String getServiceUrl() {
        return influxURL;
    }

    public String[] createUser(String username) {
        String password = RandomStringUtils.randomAlphanumeric(48);
        makeRequest(String.format("DROP USER \"%s\"", username));
        String query = String.format("CREATE USER \"%s\" WITH PASSWORD '%s'",
                username, password);
        makeRequest(query);
        return new String[] { username, password };
    }

    public void removeUser(String username) {
        makeRequest(String.format("DROP USER \"%s\"", username));
    }

    private String makeRequest(String query) {
        try {
            return HttpRequest.get(influxURL + "/query", true, 'u',
                    influxUsername, 'p', influxPassword, 'q', query).body();
        } catch (HttpRequest.HttpRequestException httpRequestException) {
            throw new InfluxServiceException(httpRequestException.getMessage());
        }
    }

    public void giveWritePrivileges(String user, String database) {
        String query = String.format("GRANT WRITE ON \"%s\" TO \"%s\"",
                database, user);
        makeRequest(query);

    }

    public void giveReadPrivileges(String user, String database) {
        String query = String.format("GRANT READ ON \"%s\" TO \"%s\"", database,
                user);
        makeRequest(query);
    }

    public String resetPassword(String username) {
        String password = RandomStringUtils.randomAlphanumeric(48);
        String query = String.format("SET PASSWORD FOR \"%s\" = '%s'", username,
                password);
        makeRequest(query);
        return password;
    }
}
