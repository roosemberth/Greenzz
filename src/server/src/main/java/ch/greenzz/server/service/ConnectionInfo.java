package ch.greenzz.server.service;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Component
@Scope("thread")
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ConnectionInfo {
    @Getter
    @Setter
    private String userIP;

    public void clear() {
        userIP = null;
    }
}
