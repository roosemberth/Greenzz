package ch.greenzz.server.id;

import ch.greenzz.server.db.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UserID {
    private String userId;
    public UserID(User user){
        userId = String.valueOf(user.getId());
    }
}
