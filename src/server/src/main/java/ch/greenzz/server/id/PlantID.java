package ch.greenzz.server.id;

import ch.greenzz.server.db.Plant;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class PlantID {
    private String uuid;
    public PlantID(Plant plant){
        uuid = plant.getUuid();
    }
}
