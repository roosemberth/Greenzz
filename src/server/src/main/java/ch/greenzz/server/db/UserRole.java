package ch.greenzz.server.db;

//at first glance it seems unnecessary, but it's required by the SpringSecurity
//authority component.
public enum UserRole {

    USER
}
