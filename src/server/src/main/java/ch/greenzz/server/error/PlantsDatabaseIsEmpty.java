package ch.greenzz.server.error;

public class PlantsDatabaseIsEmpty extends RuntimeException {
    public PlantsDatabaseIsEmpty(String msg) {
        super(msg);
    }
}