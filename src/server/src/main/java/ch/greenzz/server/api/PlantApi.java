package ch.greenzz.server.api;

import ch.greenzz.server.data.SpeciesPreferences;
import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;

import ch.greenzz.server.db.PlantProfile;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;
import ch.greenzz.server.id.PlantID;

public interface PlantApi {
    @JsonRpcErrors({
      @JsonRpcError(exception=InvalidParams.class, code=-32602),
      @JsonRpcError(exception=UserDoesNotExist.class, code=-32001),
    })
    PlantID registerPlant(String userID, String uuid);

    @JsonRpcErrors({
      @JsonRpcError(exception=InvalidParams.class, code=-32602),
      @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    PlantID resolvePlant(String uuid);

    @JsonRpcErrors({
      @JsonRpcError(exception=UserDoesNotExist.class, code=-32001),
      @JsonRpcError(exception=InvalidParams.class, code=-32602),
    })
    PlantID[] getPlants(String userID);

    @JsonRpcErrors({
      @JsonRpcError(exception=InvalidParams.class, code=-32602),
      @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    String[] getPhotoTimestamps(String uuid);

    @JsonRpcErrors({
            @JsonRpcError(exception=InvalidParams.class, code=-32602),
            @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    String getSpeciesDetails(String species);

    @JsonRpcErrors({
            @JsonRpcError(exception=InvalidParams.class, code=-32602),
            @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    SpeciesPreferences getSpeciesPreferences(String species);

    @JsonRpcErrors({
            @JsonRpcError(exception=InvalidParams.class, code=-32602),
            @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    String[] getSpeciesList();

    @JsonRpcErrors({
      @JsonRpcError(exception=InvalidParams.class, code=-32602),
      @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    void updatePlantProfile(String plantUuid, PlantProfile profile);

    @JsonRpcErrors({
      @JsonRpcError(exception=PlantDoesNotExist.class, code=-32001),
    })
    PlantProfile getPlantProfile(String plantUuid);
}
