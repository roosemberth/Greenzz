package ch.greenzz.server.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ch.greenzz.server.api.SensorApi;
import ch.greenzz.server.data.SensorInfluxRegistration;
import ch.greenzz.server.db.InfluxManager;
import ch.greenzz.server.db.Plant;
import ch.greenzz.server.db.PlantRepository;
import ch.greenzz.server.db.Sensor;
import ch.greenzz.server.db.SensorRepository;
import ch.greenzz.server.db.User;
import ch.greenzz.server.db.UserRepository;
import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;

@Component
@Transactional
public class SensorService implements SensorApi {
    private Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private SensorRepository sensorRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InfluxManager influxManager;
    @Autowired
    private PlantRepository plantRepository;

    public SensorInfluxRegistration registerSensor(String userID,
            String plantUuid, String macAddr) {
        if (!macAddr.matches("^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$")) {
            throw new InvalidParams("The specified MAC address is not valid.");
        }

        int userIDint;
        try {
            userIDint = Integer.parseInt(userID);
        } catch (NumberFormatException e) {
            throw new InvalidParams("The specified userID is not valid.");
        }

        User owner = userRepository.findById(userIDint)
                .orElseThrow(() -> new UserDoesNotExist(
                        "No user with the specified ID could be found."));

        Plant plant = plantRepository.findByUuid(plantUuid)
                .filter(p -> p.getOwner().equals(owner))
                .orElseThrow(() -> new PlantDoesNotExist(
                        "No plant with the specified UUID could be found."));

        logger.info("Registering sensor for " + owner + " (" + macAddr + ")");

        Optional<Sensor> sensorOpt = sensorRepository.findByMacaddr(macAddr);
        Sensor sensor;
        if (sensorOpt.isPresent()) {
            sensor = sensorOpt.get();
            User previousOwner = sensor.getUser();

            // We have no way of knowing whether the user reacquired a used
            // sensor board of is just playing with the API.
            // We trust by acquiescense but this should be improved.
            if (previousOwner.getId() != owner.getId()) {
                logger.warn("Sensor " + sensor + " shifting owner "
                        + previousOwner + "->" + owner);
                deregisterFromInflux(sensor);
                sensor.setUser(owner);
            }
        } else {
            sensor = Sensor.builder().macaddr(macAddr).plantID(plant.getUuid())
                    .user(owner).build();
        }

        sensor.setPlantID(plant.getUuid());
        sensor.setDisable(false);
        SensorInfluxRegistration registration = registerWithInflux(sensor, owner);
        sensorRepository.save(sensor);
        return registration;
    }

    private SensorInfluxRegistration registerWithInflux(Sensor sensor, User owner) {
        String macAddr = sensor.getMacaddr();
        String db = influxManager.createDbForSensor(macAddr);
        sensor.setInfluxdb(db);
        String[] credentials = influxManager
                .createUser("greenzz-sensor-" + macAddr);
        String user = credentials[0];
        String pass = credentials[1];
        sensor.setInfluxuser(user);
        sensorRepository.save(sensor);
        String id = String.valueOf(sensor.getIdsensor());
        String api = influxManager.getServiceUrl();
        influxManager.giveWritePrivileges(user, db);
        influxManager.giveReadPrivileges(owner.getInfluxuser(), db);
        return new SensorInfluxRegistration(id, user, pass, api, db);
    }

    private void deregisterFromInflux(Sensor sensor) {
        influxManager.removeUser(sensor.getInfluxuser());
        sensor.setInfluxdb(null);
        sensor.setInfluxuser(null);
    }
}
