package ch.greenzz.server.id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InfluxCredentials {
    private String influxUsername;
    private String influxPassword;
    private String influxEndpoint;
    private String influxDatabase;
}
