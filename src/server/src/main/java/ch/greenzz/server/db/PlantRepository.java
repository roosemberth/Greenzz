package ch.greenzz.server.db;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface PlantRepository extends CrudRepository<Plant, Long> {
  Optional<Plant> findByUuid(String uuid);

  Plant[] getPlantsByOwner(User owner);
}
