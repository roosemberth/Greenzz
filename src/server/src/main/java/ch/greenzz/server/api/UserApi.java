package ch.greenzz.server.api;

import com.googlecode.jsonrpc4j.JsonRpcError;
import com.googlecode.jsonrpc4j.JsonRpcErrors;

import ch.greenzz.server.error.InvalidParams;
import ch.greenzz.server.error.PlantDoesNotExist;
import ch.greenzz.server.error.SensorBoardDoesNotExist;
import ch.greenzz.server.error.UserDoesNotExist;
import ch.greenzz.server.id.InfluxCredentials;
import ch.greenzz.server.id.SensorID;
import ch.greenzz.server.id.UserID;

public interface UserApi {
    @JsonRpcErrors({
      @JsonRpcError(exception = InvalidParams.class, code = -32602),
    })
    UserID register(String username);

    @JsonRpcErrors({
      @JsonRpcError(exception = UserDoesNotExist.class, code = -32001),
      @JsonRpcError(exception = InvalidParams.class, code = -32602),
    })
    SensorID[] getSensors(String userID);

    @JsonRpcErrors({
      @JsonRpcError(exception = UserDoesNotExist.class, code = -32001),
      @JsonRpcError(exception = InvalidParams.class, code = -32602),
    })
    UserID resolveUsername(String username);

    @JsonRpcErrors({
      @JsonRpcError(exception = UserDoesNotExist.class, code = -32001),
      @JsonRpcError(exception = PlantDoesNotExist.class, code=-32001),
      @JsonRpcError(exception = SensorBoardDoesNotExist.class, code = -32001),
      @JsonRpcError(exception = InvalidParams.class, code = -32602),
    })
    InfluxCredentials getInfluxCredentials(String userID, String plantUuid);
}
