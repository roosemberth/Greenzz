package ch.greenzz.server.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "plants_database")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlantsDatabase {
    @Id
    private String pid;
    @Column(name = "display_pid")
    private String displayPid;
    private String alias;
    private String image;
    @Column(name = "floral_language")
    private String floralLanguage;
    private String origin;
    private String production;
    private String category;
    private String blooming;
    private String color;
    private String size;
    private String soil;
    private String sunlight;
    private String watering;
    private String fertilization;
    private String pruning;
    private int max_light_mmol;
    private int min_light_mmol;
    private int max_light_lux;
    private int min_light_lux;
    private int max_temp;
    private int min_temp;
    private int max_env_humid;
    private int min_env_humid;
    private int max_soil_moist;
    private int min_soil_moist;
    private int max_soil_ec;
    private int min_soil_ec;
}
