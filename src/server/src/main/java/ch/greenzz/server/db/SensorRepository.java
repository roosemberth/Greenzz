package ch.greenzz.server.db;

import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface SensorRepository extends CrudRepository<Sensor, Long> {
    Optional<Sensor> findByIdsensor(int sensorID);
    Optional<Sensor> findByMacaddr(String macaddr);
}
