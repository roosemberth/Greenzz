package ch.greenzz.server.db;

import ch.greenzz.server.db.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findById(int idUser);
    void deleteByUsername(String username);
}
