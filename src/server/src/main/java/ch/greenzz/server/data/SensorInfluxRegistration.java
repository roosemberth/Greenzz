package ch.greenzz.server.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SensorInfluxRegistration {
    private String sensorId;
    private String influxUser;
    private String influxPass;
    private String influxApiUrl;
    private String influxDatabase;
}
