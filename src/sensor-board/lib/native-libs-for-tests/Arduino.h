/**
 * This file provides minimal definitions of Arduino-provided classes needed by
 * machine-independent code to compile.
 *
 * Implementations should **never** rely in the correctness of these classes.
 */

#ifndef ARDUINO_STUB_H
#define ARDUINO_STUB_H

class String {
public:
  String(const char *){};
  String(int, int){};

  String &operator+(const char *) { return *this; };
  const char *c_str() { return nullptr; };
};

class {
public:
  void print(int){};
  void print(String){};
  void print(const char *){};
  void println(int){};
  void println(String){};
  void println(const char *){};
  void flush(){};
} Serial;

#endif // ARDUINO_STUB_H
// vim:ft=cpp
