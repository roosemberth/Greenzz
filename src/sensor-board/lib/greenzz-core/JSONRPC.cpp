#include <Arduino.h>

#include "JSONRPC.hpp"

/**
 * @param cb The method handler.
 * @param doc The document where the response objects will be allocated.
 */
JsonRpcAdapter::JsonRpcAdapter(GreenzzRpcCmdHandler &cb, JsonDocument &doc)
    : cb(cb), doc(doc) {}

/**
 * @brief Creates a jsonrpc error response object
 *
 * @param id id of the request.
 *
 * If the id argument is empty, it will try to reuse the id string in the
 * specified document, else the id will be null.
 * Resets the provided document.
 */
JsonObject JsonRpcAdapter::rpcError(JsonDocument &doc, const int code,
                                    const std::string message, std::string id) {
  Serial.print("Creating Json rpc error: ");
  Serial.print(message.c_str());
  Serial.print(". Code ");
  Serial.println(code);
  JsonVariant docid = doc["id"];
  if (id.empty() && !docid.isNull())
    id = docid.as<std::string>();
  doc.clear();
  JsonObject obj = doc.createNestedObject();
  if (id.empty())
    obj["id"] = nullptr;
  else
    obj["id"] = id;
  obj["jsonrpc"] = "2.0";
  JsonObject errorobj = obj.createNestedObject("error");
  errorobj["code"] = code;
  errorobj["message"] = message;
  return obj;
}

void fillObject(JsonObject o, const LocalNetworkData &src) {
  o["ipaddr"] = src.ipaddr;
  o["gateway"] = src.gateway;
  o["netmask"] = src.netmask;
  o["rssi"] = src.rssi;
}

void fillObject(JsonObject o, const WiFiNetworkEntry &src) {
  o["bssid"] = std::string(src.bssid, 18);
  o["rssi"] = src.rssi;
  o["ssid"] = src.ssid;
}

template <typename T> void fillObject(JsonArray o, std::vector<T> &src) {
  for (auto &entry : src)
    fillObject(o.createNestedObject(), entry);
}

JsonObject JsonRpcAdapter::checkRequestArguments(const JsonObject &req) {
  doc.clear();
  if (req["id"].isNull())
    return rpcError(ERR_INVALID_REQUEST, "Invalid Request");
  if (req["jsonrpc"].isNull())
    return rpcError(ERR_INVALID_REQUEST, "Invalid Request");
  if (req["jsonrpc"].as<std::string>() != "2.0")
    return rpcError(ERR_INVALID_REQUEST, "Invalid Request");
  if (req["method"].isNull())
    return rpcError(ERR_INVALID_REQUEST, "Invalid Request");
  return doc.as<JsonObject>();
}

/**
 * @param req The request to be processed.
 */
JsonObject JsonRpcAdapter::processRequest(const JsonObject &req) {
  JsonObject err = checkRequestArguments(req);
  if (!err.isNull())
    return err;

  JsonObject ret = doc.to<JsonObject>();
  std::string method = req["method"];
  ret["id"] = req["id"];
  ret["jsonrpc"] = "2.0";
  Serial.println(String("JSONRPC: Dispatching method ") + method.c_str());

  JsonObject args = req["params"];

  if (method == "echo") {
    if (args["msg"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter");
    auto rpcResult = cb.echo(args["msg"]);
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = *rpcResult.res;
  } else if (method == "reset") {
    auto rpcResult = cb.reset();
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = "OK";
  } else if (method == "wifi_query") {
    auto rpcResult = cb.wifi_query();
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    fillObject(ret.createNestedObject("result"), *rpcResult.res);
  } else if (method == "wifi_list") {
    auto rpcResult = cb.wifi_list();
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    fillObject(ret.createNestedArray("result"), *rpcResult.res);
  } else if (method == "wifi_disconnect") {
    auto rpcResult = cb.wifi_disconnect();
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = "OK";
  } else if (method == "wifi_connect") {
    if (args["ssid"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter 'ssid'");
    if (args["password"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter 'password'");
    auto rpcResult = cb.wifi_connect(args["ssid"], args["password"]);
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = "OK";
  } else if (method == "wifi_mac") {
    auto rpcResult = cb.wifi_mac();
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = *rpcResult.res;
  } else if (method == "network_local") {
    auto rpcResult = cb.network_local();
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    fillObject(ret.createNestedObject("result"), *rpcResult.res);
  } else if (method == "network_server") {
    if (args["ipv4"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter");
    auto rpcResult = cb.network_server(args["ipv4"]);
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = *rpcResult.res;
  } else if (method == "configure_influxdb") {
    if (args["influxUsername"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter");
    if (args["influxPass"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter");
    if (args["influxURL"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter");
    if (args["influxDatabase"].isNull())
      return rpcError(ERR_INVALID_PARAMS, "Missing parameter");
    auto rpcResult =
        cb.configure_influxdb(args["influxUsername"], args["influxPass"],
                              args["influxURL"], args["influxDatabase"]);
    if (rpcResult.err != nullptr)
      return rpcError(*rpcResult.err);
    ret["result"] = "OK";
  } else {
    return rpcError(ERR_METHOD_NOT_FOUND, "Method not found");
  }

  return ret;
}
