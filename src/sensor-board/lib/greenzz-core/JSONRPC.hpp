#ifndef JSONRPC_H
#define JSONRPC_H

#include <ArduinoJson.h>

#include "RPCCommands.hpp"

/**
 * @brief Enables JSON communication for an RPC Command handler
 */
class JsonRpcAdapter {
private:
  GreenzzRpcCmdHandler &cb;
  JsonDocument &doc;

  /**
   * Verifies the integrity of the request arguments.
   * Clears the adapter document.
   */
  JsonObject checkRequestArguments(const JsonObject &req);

public:
  /**
   * @param cb The method handler.
   * @param doc The document where the response objects will be allocated.
   */
  JsonRpcAdapter(GreenzzRpcCmdHandler &cb, JsonDocument &doc);

  /**
   * @brief Creates a jsonrpc error response object
   *
   * @param id id of the request. If empty, the id will be null.
   *
   * Resets the provided document.
   */
  static JsonObject rpcError(JsonDocument &doc, const int code,
                             const std::string message,
                             const std::string id = "");

  /**
   * @see JsonRpcAdapter::rpcError. Resets the provided document.
   */
  inline JsonObject rpcError(const int code, const std::string message,
                             const std::string id = "") {
    return rpcError(doc, code, message, id);
  }

  /*
   * @see JsonRpcAdapter::rpcError. Resets the provided document.
   */
  inline JsonObject rpcError(const RPCError &err,
                             const std::string id = std::string()) {
    return rpcError(err.code, err.message, id);
  }

  /**
   * @param req The request to be processed.
   * @param doc The document where to allocate the response.
   */
  JsonObject processRequest(const JsonObject &req);
};

#endif // JSONRPC_H
