#ifndef PROTOCOLS_HPP
#define PROTOCOLS_HPP

#include <string>

#include "RPCCommands.hpp"

/**
 * @brief Manages interactions by replying to a request.
 */
class ResponseController {
public:
  virtual std::string respond(const std::string &request) = 0;
};

/**
 * Singleton instance that echoes the received request back.
 */
class : public ResponseController {
  std::string respond(const std::string &request) { return request; }
} controller_echo_back;

/**
 * @brief RC processing jsonrpc
 */
class JsonRpcResponseController : public ResponseController {
public:
  /**
   * @brief Handler of JSONRPC requests.
   */
  GreenzzRpcCmdHandler &cb;

  JsonRpcResponseController(GreenzzRpcCmdHandler &cmdhandler);

  std::string respond(const std::string &request);
};

#endif // PROTOCOLS_HPP
