#include <Arduino.h>
#include <ArduinoJson.h>

#include "JSONRPC.hpp"
#include "Protocols.hpp"

JsonRpcResponseController::JsonRpcResponseController(
    GreenzzRpcCmdHandler &cmdhandler)
    : cb(cmdhandler) {}

template <typename Doc> static inline std::string moveJsonToStr(Doc *doc) {
  std::string ret;
  serializeJson(*doc, ret);
  doc->clear();
  return ret;
}

/**
 * @param reqdoc JsonDocument where to store either the deserialized document or
 *            the error response.
 * @returns a jsonrpc error in case of error, else the deserialized response.
 */
JsonObject deserializeRequest(DynamicJsonDocument &reqdoc,
                              const std::string &request) {
  auto err = deserializeJson(reqdoc, request);
  if (err != DeserializationError::Ok) {
    Serial.println(String("JrpcRC: Could not deserialize: ") + err.c_str());
    JsonRpcAdapter::rpcError(reqdoc, ERR_INVALID_REQUEST, "Invalid Request.");
    return reqdoc.as<JsonObject>();
  }
  reqdoc.shrinkToFit();

  if (reqdoc.overflowed()) {
    Serial.println("JrpcRC: Request did not fit in JsonDocument.");
    JsonRpcAdapter::rpcError(reqdoc, ERR_INVALID_REQUEST,
                             "Request entity too big.");
    return reqdoc.as<JsonObject>();
  }

  JsonObject req = reqdoc.as<JsonObject>();
  if (req.isNull()) { // The request did not contain an object
    Serial.println("JrpcRC: Request was not an object.");
    JsonRpcAdapter::rpcError(reqdoc, ERR_INVALID_REQUEST,
                             "Exactly one request object is accepted a time.");
    return reqdoc.as<JsonObject>();
  }
  return req;
}

std::string JsonRpcResponseController::respond(const std::string &request) {
  Serial.println("JsonRpcResponseController invoked.");
  DynamicJsonDocument *reqdoc = new DynamicJsonDocument(1024);

  JsonObject req = deserializeRequest(*reqdoc, request);
  if (!req["error"].isNull())
    return moveJsonToStr(&req);

  // https://arduinojson.org/v6/assistant/ estimated size for 32 networks with
  // SSIDs length μ=12.94, σ=4.88. Any other response should be smaller.
  DynamicJsonDocument *resdoc = new DynamicJsonDocument(4096);

  JsonObject response = JsonRpcAdapter(this->cb, *resdoc).processRequest(req);
  if (resdoc->overflowed()) {
    Serial.println("JrpcRC: Unable to serialize response.");
    JsonRpcAdapter::rpcError(*reqdoc, ERR_INVALID_REQUEST,
                             "Could not serialize the response.");
    std::string ret = moveJsonToStr(reqdoc);
    delete reqdoc;
    delete resdoc;
    return ret;
  }
  resdoc->shrinkToFit();

  Serial.println("JrpcRC: Request handled.");
  std::string ret = moveJsonToStr(&response);
  delete reqdoc;
  delete resdoc;
  return ret;
};
