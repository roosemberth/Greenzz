#ifndef RPCCOMMANDS_HPP
#define RPCCOMMANDS_HPP

#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

enum greenzzrpcerror {
  /**
   * @brief Request requires no active WiFi AP, but connected to a WiFi AP.
   */
  ERR_CONNECTED = -32099,
  /**
   * @brief The device was unable to disassociate from the WiFi AP.
   */
  ERR_COULD_NOT_DISCONNET,
  /**
   * @brief Device occupied or otherwise unable to handle the specified request.
   */
  ERR_BUSY,
  /**
   * @brief Connection to the requested access point could not complete.
   */
  ERR_AP_CONNECTION_FAILED,
  /**
   * @brief Device associated to an AP but no IP configuration yet.
   */
  ERR_IN_PROGRESS,
  /**
   * @brief Device associated to an AP but received no IP configuration.
   */
  ERR_NO_IP,
  /**
   * @brief Device has IP connectivity but was unable to reach the server.
   */
  ERR_UNREACHABLE,
  /**
   * @brief Device is not associated to a WiFi AP.
   */
  ERR_NOT_CONNECTED_TO_AP,
  /**
   * @brief The requested method has not been implemented.
   */
  ERR_NOT_IMPLEMENTED,
};

/**
 * @brief JSONRPC 2.0 pre-defined errors. Refer to the specification.
 */
enum jsonrpcfaultcodes {
  ERR_PARSE_ERROR = -32700,
  ERR_INVALID_REQUEST = -32600,
  ERR_METHOD_NOT_FOUND = -32601,
  ERR_INVALID_PARAMS = -32602,
  ERR_INTERNAL_ERROR = -32603,
};

/**
 * @brief Container of an RPC error.
 */
struct RPCError {
public:
  std::string message;
  int code;

  RPCError(int code) : message(""), code(code) {}
  RPCError(const char *msg, int code) : message(msg), code(code) {}
};

/**
 * @brief Container of an RPC result.
 */
template <typename Result> class RPCResult {
public:
  RPCResult(Result *res, RPCError *err = nullptr)
      : res(std::unique_ptr<Result>(res)), err(std::unique_ptr<RPCError>(err)) {
    if (res == nullptr && err == nullptr)
      throw std::runtime_error("A result or an error was required.");
  }
  RPCResult(RPCError *err) : RPCResult(nullptr, err) {}

  std::unique_ptr<Result> res;
  std::unique_ptr<RPCError> err;
};

/**
 * @brief Container for information about a WiFi network.
 */
struct WiFiNetworkEntry {
public:
  char bssid[18];
  int8_t rssi;
  std::string ssid;
};

/**
 * @brief Container for the current wan network configuration.
 */
struct LocalNetworkData {
public:
  std::string ipaddr;
  std::string gateway;
  std::string netmask;
  int8_t rssi;
};

/**
 * @brief encodes an operation completed successfully and produced no result.
 */
class Unit {}; // Isomorphic to void without the aliasing.

/**
 * Handler class for an RPC session.
 */
class GreenzzRpcCmdHandler {
public:
  virtual RPCResult<std::string> echo(const std::string &) const = 0;
  virtual RPCResult<Unit> reset() const = 0;
  virtual RPCResult<WiFiNetworkEntry> wifi_query() const = 0;
  virtual RPCResult<std::vector<WiFiNetworkEntry>> wifi_list() const = 0;
  virtual RPCResult<Unit> wifi_disconnect() const = 0;
  virtual RPCResult<Unit> wifi_connect(const char *, const char *) const = 0;
  virtual RPCResult<std::string> wifi_mac() const = 0;
  virtual RPCResult<LocalNetworkData> network_local() const = 0;
  virtual RPCResult<int> network_server(const std::string &) const = 0;
  virtual RPCResult<Unit> configure_influxdb(const std::string &,
                                             const std::string &,
                                             const std::string &,
                                             const std::string &) const = 0;
};

#endif // RPCCOMMANDS_HPP
