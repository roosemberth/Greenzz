#include <Arduino.h>

#include "BoardID.hpp"

char wifiStaMacAddr[18] = {0};

const char *getBoardAddress() {
  // If the board identity was already calculated, just return its address.
  if (wifiStaMacAddr[0])
    return wifiStaMacAddr;

  uint8_t baseMac[6];
  esp_read_mac(baseMac, ESP_MAC_WIFI_STA); // Get MAC address for WiFi station
  sprintf(wifiStaMacAddr, "%02X:%02X:%02X:%02X:%02X:%02X", baseMac[0],
          baseMac[1], baseMac[2], baseMac[3], baseMac[4], baseMac[5]);
  return wifiStaMacAddr;
}
