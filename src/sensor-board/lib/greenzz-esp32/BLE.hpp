#ifndef BLE_HPP
#define BLE_HPP

#include <NimBLEDevice.h>
#include <string>

#include "Protocols.hpp"

class GreenzzBLESrvCallbacks;
class GreenzzCharacteristicHandler;

/**
 * Manages BTLE interactions.
 *
 * Configures a single service with two characteristics (TX, RX) and provides
 * a bidirectional tunnel with a peer.
 */
class GreenzzBLEManager {
public:
  /**
   * @param controller ResponseController managing the interaction.
   *
   * The default controller echoes back received messages.
   */
  GreenzzBLEManager(ResponseController &controller = controller_echo_back);

  /**
   * Enables Bluetooth communication.
   */
  void begin(void);

  /**
   * Enables Bluetooth communication.
   */
  void stop(void);

  const std::string name;

private:
  // @brief Controller managing the interaction.
  ResponseController &controller;
  /// @brief BLE server handler.
  BLEServer *pServer;
  /// @brief Service hosting the pipe.
  BLEService *pService;
  // Characteristics managing communication to and from the device.
  BLECharacteristic *tx, *rx;
  // Whether a device is connected.
  bool deviceConnected;

  BLECharacteristicCallbacks *charCbs;
  BLEServerCallbacks *srvcb;

  friend GreenzzBLESrvCallbacks;
  friend GreenzzCharacteristicHandler;
};

#endif // BLE_HPP
