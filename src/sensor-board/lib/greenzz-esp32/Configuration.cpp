#include "Configuration.hpp"

#include <Arduino.h>
#include <FS.h>
#include <SPIFFS.h>
#include <stdexcept>

#define FORMAT_SPIFFS_IF_FAILED 1
#define INFLUX_CFG_FILEPATH "/influx.cfg"
#define WIFI_CFG_FILEPATH "/wifi.cfg"

using namespace std;

GreenzzConfigurationManager *GreenzzConfigurationManager::instance = nullptr;

GreenzzConfigurationManager::GreenzzConfigurationManager() {
  if (!SPIFFS.begin()) {
    Serial.println("Failed to initialize flash. Formatting...");
    if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED))
      throw runtime_error("Could not initialize new SPIFFS partition.");
    Serial.println("Successfully formatted and initialized SPIFFS partition.");
  } else {
    Serial.println("Mounted existing SPIFFS filesystem.");
  }
}

//// @brief Returns non-zero in case of error.
int GreenzzConfigurationManager::readInfluxExporterConfig(string &user,
                                                          string &pass,
                                                          string &url,
                                                          string &database) {
  if (!SPIFFS.exists(INFLUX_CFG_FILEPATH))
    return -1;
  File configFile = SPIFFS.open(INFLUX_CFG_FILEPATH, FILE_READ);
  try {
    user = string(configFile.readStringUntil('\n').c_str());
    pass = ((configFile.readStringUntil('\n').c_str()));
    url = string(configFile.readStringUntil('\n').c_str());
    database = string(configFile.readStringUntil('\n').c_str());
    configFile.close();
  } catch (...) {
    Serial.println("Could not read Influx exporter configuration file.");
    configFile.close();
    return -1;
  }
  return 0;
}

GreenzzConfigurationManager &GreenzzConfigurationManager::getInstance() {
  if (instance == nullptr)
    instance = new GreenzzConfigurationManager();
  return *instance;
}

void GreenzzConfigurationManager::setInfluxExporterConfig(
    const std::string &username, const std::string &password,
    const std::string &apiUrl, const std::string &databaseName) {
  Serial.println("Saving InfluxDB Exporter configuration");
  File configFile = SPIFFS.open(INFLUX_CFG_FILEPATH, FILE_WRITE);
  if (!configFile)
    throw std::runtime_error("Could not persist the InfluxDB configuration.");
  configFile.print(username.c_str());
  configFile.print("\n");
  configFile.print(password.c_str());
  configFile.print("\n");
  configFile.print(apiUrl.c_str());
  configFile.print("\n");
  configFile.print(databaseName.c_str());
  configFile.print("\n");
  configFile.close();
}

int GreenzzConfigurationManager::readWiFiConfig(std::string &ssid,
                                                std::string &pass) {
  if (!SPIFFS.exists(WIFI_CFG_FILEPATH))
    return -1;
  File configFile = SPIFFS.open(WIFI_CFG_FILEPATH, FILE_READ);
  try {
    ssid = string(configFile.readStringUntil('\n').c_str());
    pass = ((configFile.readStringUntil('\n').c_str()));
    configFile.close();
  } catch (...) {
    Serial.println("Could not read WiFi configuration file.");
    configFile.close();
    return -1;
  }
  return 0;
}

void GreenzzConfigurationManager::setWiFiConfig(const std::string &ssid,
                                                const std::string &pass) {
  Serial.println("Saving WiFi configuration");
  File configFile = SPIFFS.open(WIFI_CFG_FILEPATH, FILE_WRITE);
  if (!configFile)
    throw std::runtime_error("Could not persist the WiFi configuration.");
  configFile.print(ssid.c_str());
  configFile.print("\n");
  configFile.print(pass.c_str());
  configFile.print("\n");
  configFile.close();
}

void GreenzzConfigurationManager::clearWiFiConfig() {
  if (!SPIFFS.exists(WIFI_CFG_FILEPATH))
    return;
  SPIFFS.remove(WIFI_CFG_FILEPATH);
}
