#include "Influx.hpp"

#include <Adafruit_MCP9808.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_TSL2591.h>
#include <Arduino.h>
#include <stdexcept>

#define ANALOG_GPIO 33

using namespace std;

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591);
Adafruit_MCP9808 mcp = Adafruit_MCP9808();

void configureTempSensor() {
  if (!mcp.begin(0x18))
    throw runtime_error("Could not configure temperature sensor.");

  mcp.setResolution(3);
  // sets the resolution mode of reading, the modes are defined in the table
  // bellow: Mode Resolution SampleTime
  //  0    0.5°C       30 ms
  //  1    0.25°C      65 ms
  //  2    0.125°C     130 ms
  //  3    0.0625°C    250 ms
}

void configureLightSensor() {
  if (!tsl.begin(0x29))
    throw runtime_error("Could not configure light sensor.");

  // You can change the gain on the fly, to adapt to brighter/dimmer light
  // situations
  tsl.setGain(TSL2591_GAIN_LOW); // 1x gain (bright light)

  // Changing the integration time gives you a longer time over which to sense
  // light longer timelines are slower, but are good in very low light
  // situtations!
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
}

float humiditySensorRead() {
  float sensorValue = (float)analogRead(ANALOG_GPIO);
  // tranform sensor value in %.The values are "adapted" to match <50 equal 0%
  // until 5%. Then it follows the rules as if 2200 equal 100%.
  if (sensorValue >= 2200.0) {
    return 100.0F;
  } else if (sensorValue <= 50) {
    return 0.0F;
  } else if (sensorValue <= 66) {
    return 1.0F;
  } else if (sensorValue <= 77) {
    return 2.0F;
  } else if (sensorValue <= 88) {
    return 3.0F;
  } else if (sensorValue <= 99) {
    return 4.0F;
  } else if (sensorValue <= 110) {
    return 5.0F;
  } else {
    return (float)(sensorValue * 100 / 2200.0);
  }
}

float tempSensorRead() {
  mcp.wake();
  float temperature = mcp.readTempC() - 3; // Account for wifi chip heating;
  mcp.shutdown_wake(1);
  return temperature;
}

float lightSensorRead() {
  /* Get a new sensor event */
  sensors_event_t event;
  tsl.getEvent(&event);
  if ((event.light == 0) | (event.light > 4294966000.0) |
      (event.light < -4294966000.0)) {
    /* If event.light = 0 lux the sensor is probably saturated */
    /* and no reliable data could be generated! */
    /* if event.light is +/- 4294967040 there was a float over/underflow */
    Serial.println("Invalid data (adjust gain or timing)");
  }
  return event.light;
}

InfluxExporter::InfluxExporter(const char *url, const char *user,
                               const char *pass, const char *database)
    : dataPoint("sensor-board") {
  influxClient.setConnectionParamsV1(url, database, user, pass);
  if (!influxClient.validateConnection())
    throw runtime_error("Could not validate connection.");
  Wire.begin();
  configureLightSensor();
  configureTempSensor();
}

void InfluxExporter::measureAndPublish() {
  float tempMesurement = tempSensorRead();
  float lightMesurement = lightSensorRead();
  float humiMesurement = humiditySensorRead();

  dataPoint.clearFields();
  dataPoint.addField("temperature", tempMesurement);
  dataPoint.addField("luminosity", lightMesurement);
  dataPoint.addField("humidity", humiMesurement);

  if (!influxClient.writePoint(dataPoint)) {
    influxClient.flushBuffer();
    Serial.print("Failed to publish data to Influx database: ");
    Serial.println(influxClient.getLastErrorMessage());
  }
}
