#ifndef TASKS_H
#define TASKS_H

#include <Arduino.h>

class GreenzzTaskControl {
private:
  static GreenzzTaskControl *instance;
  GreenzzTaskControl();
  TaskHandle_t *influxTask;

public:
  static GreenzzTaskControl &getInstance();
  void startInfluxWork();
  void stopInfluxWork();
};

#endif // TASKS_H
