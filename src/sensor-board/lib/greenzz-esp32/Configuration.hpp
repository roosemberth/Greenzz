#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <string>

class GreenzzConfigurationManager {
private:
  static GreenzzConfigurationManager *instance;
  GreenzzConfigurationManager();

public:
  static GreenzzConfigurationManager &getInstance();

  int readInfluxExporterConfig(std::string &username, std::string &password,
                               std::string &apiUrl, std::string &databaseName);

  void setInfluxExporterConfig(const std::string &username,
                               const std::string &password,
                               const std::string &apiUrl,
                               const std::string &databaseName);

  int readWiFiConfig(std::string &ssid, std::string &password);

  void setWiFiConfig(const std::string &ssid, const std::string &password);

  void clearWiFiConfig();
};

#endif // CONFIGURATION_H
