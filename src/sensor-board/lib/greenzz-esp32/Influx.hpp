#ifndef INFLUX_HPP
#define INFLUX_HPP

#include <InfluxDbClient.h>

class InfluxExporter {
private:
  InfluxDBClient influxClient;
  Point dataPoint;

public:
  InfluxExporter(const char *url, const char *user, const char *pass,
                 const char *database);

  void measureAndPublish();
};

#endif // INFLUX_HPP
