#include "Tasks.hpp"

#include <Arduino.h>
#include <string>

#include "Configuration.hpp"
#include "Influx.hpp"

using namespace std;

GreenzzTaskControl *GreenzzTaskControl::instance = nullptr;

GreenzzTaskControl::GreenzzTaskControl() {}

InfluxExporter *initInfluxExporter() {
  string influxUser;
  string influxPass;
  string influxUrl;
  string influxDatabase;

  auto cfg = GreenzzConfigurationManager::getInstance();
  if (cfg.readInfluxExporterConfig(influxUser, influxPass, influxUrl,
                                   influxDatabase) != 0) {
    Serial.println("Failed to read InfluxDB configuration.");
    return nullptr;
  }

  try {
    return new InfluxExporter(influxUrl.c_str(), influxUser.c_str(),
                              influxPass.c_str(), influxDatabase.c_str());
    Serial.println("Failed to initialize InfluxDB exporter.");
  } catch (...) {
    return nullptr;
  }
}

static bool influxTaskShouldStop = false;

void influxWaitWiFi() {
  if (WiFi.status() == WL_CONNECTED)
    return;
  string ssid, pass;
  Serial.println("InfluxDB exporter waiting for WiFi connection.");
  auto cfg = GreenzzConfigurationManager::getInstance();
  while (WiFi.status() != WL_CONNECTED) {
    if (cfg.readWiFiConfig(ssid, pass) == 0) {
      Serial.print("Attempting to associate with WiFi network '");
      Serial.print(ssid.c_str());
      Serial.println("'.");
      WiFi.begin(ssid.c_str(), pass.c_str());
    }
    for (int i = 0; i < 200; ++i) {
      if (WiFi.status() == WL_CONNECTED || influxTaskShouldStop)
        break;
      vTaskDelay(100);
    }
  }
  Serial.println("InfluxDB exporter detected connection to WiFi network.");
}

void influxWork(void *) {
  influxWaitWiFi();
  auto exporter = initInfluxExporter();
  if (exporter == nullptr)
    return vTaskDelete(nullptr);
  Serial.println("InfluxDB will publish data every 5 seconds.");
  while (!influxTaskShouldStop) {
    influxWaitWiFi();
    exporter->measureAndPublish();
    for (int i = 0; (i < 50) && (!influxTaskShouldStop); ++i)
      vTaskDelay(100); // Allow earlier termination.
  }
  Serial.println("Stopped InfluxDB exporter task.");
  influxTaskShouldStop = false;
  vTaskDelete(nullptr);
}

GreenzzTaskControl &GreenzzTaskControl::getInstance() {
  if (instance == nullptr)
    instance = new GreenzzTaskControl();
  return *instance;
}

void GreenzzTaskControl::startInfluxWork() {
  if (influxTask != nullptr) { // Restart task if already running
    stopInfluxWork();
  }
  Serial.println("Starting InfluxDB exporter task.");
  influxTaskShouldStop = false;
  xTaskCreate(influxWork, "InfluxDB export worker", 10000, NULL, 1, influxTask);
}

void GreenzzTaskControl::stopInfluxWork() {
  Serial.println("Stopping InfluxDB exporter task.");
  if (influxTask != nullptr) {   // Restart task if already running
    influxTaskShouldStop = true; // Cleared when task is stop.
    while (influxTaskShouldStop)
      vTaskDelay(100);
    influxTask = nullptr;
  }
}
