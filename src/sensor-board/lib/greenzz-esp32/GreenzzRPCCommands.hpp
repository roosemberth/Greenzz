#ifndef GREENZZ_RPC_COMMANDS_HPP
#define GREENZZ_RPC_COMMANDS_HPP

#include <string>
#include <vector>

#include "RPCCommands.hpp"

class GreenzzRpcCmdHandlerImpl : public GreenzzRpcCmdHandler {
public:
  RPCResult<std::string> echo(const std::string &) const;
  RPCResult<Unit> reset() const;
  RPCResult<WiFiNetworkEntry> wifi_query() const;
  RPCResult<std::vector<WiFiNetworkEntry>> wifi_list() const;
  RPCResult<Unit> wifi_disconnect() const;
  RPCResult<Unit> wifi_connect(const char *, const char *) const;
  RPCResult<std::string> wifi_mac() const;
  RPCResult<LocalNetworkData> network_local() const;
  RPCResult<Unit> configure_influxdb(const std::string &, const std::string &,
                                     const std::string &,
                                     const std::string &) const;
  RPCResult<int> network_server(const std::string &) const;
};

#endif // GREENZZ_RPC_COMMANDS_HPP
