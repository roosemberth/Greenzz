#include "GreenzzRPCCommands.hpp"

#include <Arduino.h>
#include <SPIFFS.h>
#include <WiFi.h>
#include <stdexcept>
#include <stdio.h>

#include "Configuration.hpp"
#include "Tasks.hpp"

using namespace std;

RPCResult<string> GreenzzRpcCmdHandlerImpl::echo(const string &msg) const {
  Serial.println(String("RPC: Handling echo request: '") + msg.c_str() + "'.");
  return RPCResult<string>(new string(msg));
};

RPCResult<Unit> GreenzzRpcCmdHandlerImpl::reset() const {
  Serial.println("RPC: Handling reset request.");
  ESP.restart();
  // Execution should never continue after here.
  throw runtime_error("Execution continued after device RESET.");
};

void fillNetworkEntry(WiFiNetworkEntry *dst, const uint8_t *bssid,
                      const int rssi, const char *ssid) {
  sprintf(dst->bssid, "%02X:%02X:%02X:%02X:%02X:%02X", bssid[0], bssid[1],
          bssid[2], bssid[3], bssid[4], bssid[5]);
  dst->rssi = rssi;
  dst->ssid = string(ssid);
}

RPCResult<WiFiNetworkEntry> GreenzzRpcCmdHandlerImpl::wifi_query() const {
  Serial.println("RPC: Handling wifi query request.");
  if (!WiFi.isConnected())
    return RPCResult<WiFiNetworkEntry>(new RPCError(ERR_NOT_CONNECTED_TO_AP));

  WiFiNetworkEntry *info = new WiFiNetworkEntry();
  fillNetworkEntry(info, WiFi.BSSID(), WiFi.RSSI(), WiFi.SSID().c_str());
  return RPCResult<WiFiNetworkEntry>(info);
};

RPCResult<vector<WiFiNetworkEntry>>
GreenzzRpcCmdHandlerImpl::wifi_list() const {
  Serial.println("RPC: Handling wifi list request.");
  if (WiFi.isConnected())
    return RPCResult<vector<WiFiNetworkEntry>>(new RPCError(ERR_CONNECTED));
  Serial.println("Scanning wifi networks.");
  int nNetworks = WiFi.scanNetworks();
  if (nNetworks < 0) {
    Serial.print("WiFi scan returned a negative number: ");
    Serial.println(nNetworks);
    return RPCResult<vector<WiFiNetworkEntry>>(
        new RPCError("Internal error.", ERR_INTERNAL_ERROR));
  }
  vector<WiFiNetworkEntry> *res = new vector<WiFiNetworkEntry>();
  res->reserve(nNetworks);
  for (int i = 0; i < nNetworks; ++i) {
    WiFiNetworkEntry entry;
    fillNetworkEntry(&entry, WiFi.BSSID(i), WiFi.RSSI(i), WiFi.SSID(i).c_str());
    res->push_back(entry);
  }
  return RPCResult<vector<WiFiNetworkEntry>>(res);
};

RPCResult<Unit> GreenzzRpcCmdHandlerImpl::wifi_disconnect() const {
  Serial.println("RPC: Handling wifi disconnect request.");
  GreenzzConfigurationManager::getInstance().clearWiFiConfig();
  GreenzzTaskControl::getInstance().stopInfluxWork();
  if (!WiFi.disconnect(false, true))
    return RPCResult<Unit>(new RPCError(ERR_COULD_NOT_DISCONNET));
  return RPCResult<Unit>(new Unit());
};

RPCResult<Unit>
GreenzzRpcCmdHandlerImpl::wifi_connect(const char *ssid,
                                       const char *password) const {
  Serial.println("RPC: Handling wifi connect request.");
  if (WiFi.isConnected())
    return RPCResult<Unit>(new RPCError(ERR_CONNECTED));

  auto err = WiFi.begin(ssid, password);
  try {
    GreenzzConfigurationManager::getInstance().setWiFiConfig(ssid, password);
  } catch (...) {
    return RPCResult<Unit>(new RPCError("Failed to persist WiFi configuration.",
                                        ERR_INTERNAL_ERROR));
  }
  /**
   * WL_IDLE_STATUS: temporary status until the number of attempts expires
   * (resulting in WL_CONNECT_FAILED) or a connection is established
   * (resulting in WL_CONNECTED).
   * We accept WL_NO_SHIELD since there is a bug in the Arduino framework for
   * esp32. As a workaround clients may call `wifi_query` to verify whether a
   * connection was established.
   * WL_DISCONNECTED: Internal race condition between setup and query.
   */
  if (err == WL_NO_SSID_AVAIL || err == WL_CONNECT_FAILED ||
      err == WL_CONNECTION_LOST)
    return RPCResult<Unit>(new RPCError(ERR_AP_CONNECTION_FAILED));
  // TODO(@Roos): Add a flag preventing other call while this WiFi connection
  // is in progress; a WiFi callback should be added to clear such flag after
  // any result from the WiFi stack.
  return RPCResult<Unit>(new Unit());
};

RPCResult<string> GreenzzRpcCmdHandlerImpl::wifi_mac() const {
  Serial.println("RPC: Handling wifi MAC address request.");
  return RPCResult<string>(new string(WiFi.macAddress().c_str()));
};

RPCResult<LocalNetworkData> GreenzzRpcCmdHandlerImpl::network_local() const {
  Serial.println("RPC: Handling network local request.");
  if (!WiFi.isConnected())
    return RPCResult<LocalNetworkData>(new RPCError(ERR_NOT_CONNECTED_TO_AP));

  LocalNetworkData *info = new LocalNetworkData();
  info->ipaddr = string(WiFi.localIP().toString().c_str()),
  info->gateway = string(WiFi.gatewayIP().toString().c_str()),
  info->netmask = string(WiFi.subnetMask().toString().c_str()),
  info->rssi = WiFi.RSSI();

  return RPCResult<LocalNetworkData>(info);
};

RPCResult<Unit> GreenzzRpcCmdHandlerImpl::configure_influxdb(
    const std::string &influxUser, const std::string &influxPass,
    const std::string &influxURL, const std::string &influxDatabase) const {
  if (!WiFi.isConnected())
    return RPCResult<Unit>(new RPCError(ERR_NOT_CONNECTED_TO_AP));
  try {
    GreenzzConfigurationManager::getInstance().setInfluxExporterConfig(
        influxUser, influxPass, influxURL, influxDatabase);
  } catch (...) {
    return RPCResult<Unit>(
        new RPCError("Could not write configuration.", ERR_INTERNAL_ERROR));
  }
  GreenzzTaskControl::getInstance().startInfluxWork();
  return RPCResult<Unit>(new Unit());
}

RPCResult<int>
GreenzzRpcCmdHandlerImpl::network_server(const string &addr) const {
  Serial.println("RPC: Handling network server request.");
  IPAddress ip;
  if (!WiFi.isConnected())
    return RPCResult<int>(new RPCError(ERR_NOT_CONNECTED_TO_AP));
  if (!ip.fromString(addr.c_str()))
    return RPCResult<int>(
        new RPCError("Could not parse IP address.", ERR_INVALID_PARAMS));
  return RPCResult<int>(new RPCError("Not implemented.", ERR_NOT_IMPLEMENTED));
};
