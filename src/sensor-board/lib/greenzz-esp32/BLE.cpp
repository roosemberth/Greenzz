#include <Arduino.h>
#include <NimBLEDevice.h>

#include "BLE.hpp"
#include "BoardID.hpp"

const static char SERVICE_UUID[] = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
const static char CHAR_RX_UUID[] = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
const static char CHAR_TX_UUID[] = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";

/**
 * @brief BTLE server-related events management.
 */
class GreenzzBLESrvCallbacks : public BLEServerCallbacks {
private:
  GreenzzBLEManager &mgr;

public:
  GreenzzBLESrvCallbacks(GreenzzBLEManager &mgr) : mgr(mgr) {}
  void onConnect(BLEServer *pServer) {
    Serial.println("Connected to BLE peer.");
    mgr.deviceConnected = true;
  };
  void onDisconnect(BLEServer *pServer) {
    Serial.println("Disconnected from BLE peer.");
    mgr.deviceConnected = false;
    pServer->getAdvertising()->start();
  };
};

/**
 * @brief Management of interactions with a peer (notably tx and rx).
 */
class GreenzzCharacteristicHandler : public BLECharacteristicCallbacks {
private:
  GreenzzBLEManager &mgr;
  // DLE max data channel payload.
  // Default value is that of the one in the Bluetooth LE specification.
  // The value will be updated onSubscribe to reflect the value negociated
  // with the peer.
  uint8_t mtu = 20;

  inline uint8_t next_chunk_size(const std::string &tx_buffer) {
    int tx_buf_length = tx_buffer.length();
    return tx_buf_length < mtu ? tx_buf_length : mtu;
  }

public:
  GreenzzCharacteristicHandler(GreenzzBLEManager &mgr) : mgr(mgr) {}
  void onWrite(BLECharacteristic *pCharacteristic) {
    std::string rxValue = pCharacteristic->getValue();
    if (rxValue.length() > 0) {
      Serial.println("Received message over BLE.");
      std::string tx_buffer = mgr.controller.respond(rxValue);
      for (uint8_t nbytes = next_chunk_size(tx_buffer); nbytes > 0;
           nbytes = next_chunk_size(tx_buffer)) {
        mgr.tx->setValue(reinterpret_cast<const uint8_t *>(tx_buffer.c_str()),
                         nbytes);
        // Indicating (as opposed to notifying) is synchronous and will stall
        // both this thread execution and air-time until the peer reads.
        mgr.tx->indicate();
        tx_buffer.erase(0, nbytes);
      }
      Serial.println("Finished handling BLE message.");
    }
  }
  void onSubscribe(NimBLECharacteristic *pCharacteristic,
                   ble_gap_conn_desc *desc, uint16_t subValue) {
    if (subValue == 1) {
      // Get Link MTU and remove the 3 bytes of ATT header.
      mtu = mgr.pServer->getPeerMTU(desc->conn_handle) - 3;
      Serial.println(
          String("Client subscribed. Connection MTU: " + String(mtu)));
    }
  }
};

static std::string getDeviceName() {
  std::string boardId = getBoardAddress();
  std::string name = "Greenzz Sensor Board " + boardId.substr(9, 8);
  return name;
}

GreenzzBLEManager::GreenzzBLEManager(ResponseController &controller)
    : name(getDeviceName()), controller(controller), deviceConnected(false) {}

void GreenzzBLEManager::begin() {
  BLEDevice::init(name);
  BLEDevice::setPower(ESP_PWR_LVL_P9); // Set the board to max power
  BLEDevice::setMTU(517);
  pServer = BLEDevice::createServer();
  srvcb = new GreenzzBLESrvCallbacks(*this);
  pServer->setCallbacks(srvcb);

  pService = pServer->createService(SERVICE_UUID);
  charCbs = new GreenzzCharacteristicHandler(*this);
  tx = pService->createCharacteristic(CHAR_TX_UUID, NIMBLE_PROPERTY::NOTIFY);
  tx->setCallbacks(charCbs);
  // 0x2904 descriptor will be added to chars with PROPERTY_NOTIFY by NimBLE.
  rx = pService->createCharacteristic(CHAR_RX_UUID, NIMBLE_PROPERTY::WRITE);
  rx->setCallbacks(charCbs);
  pService->start();

  pServer->getAdvertising()->setScanResponse(true);
  pServer->getAdvertising()->setMinPreferred(0x06); // Fixes iOS shenanigans.
  pServer->getAdvertising()->setMaxPreferred(0x12);
  pServer->getAdvertising()->start();

  auto bleAddr = BLEDevice::getAddress().toString();
  const char *address = bleAddr.c_str();
  Serial.println(String("Device ") + address + " ready and advertising.");
}

void GreenzzBLEManager::stop() { BLEDevice::deinit(true); }
