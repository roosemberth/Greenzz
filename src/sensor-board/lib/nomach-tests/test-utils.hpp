#ifdef UNIT_TEST

#ifndef TEST_UTILS_HPP
#define TEST_UTILS_HPP
#include <string>

/**
 * Returns a pseudo-random string 32 chars long.
 */
std::string prandom_string();

#endif // TEST_UTILS_HPP
#endif // UNIT_TEST
