#ifdef UNIT_TEST

#include <ArduinoJson.h>
#include <stdexcept>
#include <unity.h>

#include "Protocols.hpp"
#include "RPCCommands.hpp"

#include "jsonrpc-tests.hpp"
#include "test-utils.hpp"

#ifdef ARDUINO // Workaround Arduino compiler missing some C++11 features.
#include <sstream>
namespace std {
template <typename T> std::string to_string(T Value) {
  std::stringstream tmp;
  tmp << Value;
  return tmp.str();
}
} // namespace std
#endif

using namespace std;

#define TEST_ASSERT_EQ_STR_ATTR(a, b)                                          \
  TEST_ASSERT_EQUAL_STRING(((string)a).c_str(), b.as<const char *>())

DynamicJsonDocument doc(1024);

template <typename Doc> static inline string dump(const Doc &doc) {
  std::string ret;
  serializeJson(doc, ret);
  return ret;
}

void test_jsonrpc_echo() {
  class : public StubHandler {
  public:
    RPCResult<string> echo(const string &msg) const {
      return RPCResult<string>(new string(msg));
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string msg = prandom_string();

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "echo";
  auto params = req.createNestedObject("params");
  params["msg"] = msg;

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR(msg, resp["result"]);
}

void test_jsonrpc_reset() {
  class : public StubHandler {
  public:
    RPCResult<Unit> reset() const { return RPCResult<Unit>(new Unit()); };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "reset";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR("OK", resp["result"]);
}

void test_jsonrpc_wifi_query() {
  static WiFiNetworkEntry expected = {
      .bssid = "AA:BB:CC:DD:EE:FF",
      .rssi = -20,
      .ssid = prandom_string(),
  };
  class : public StubHandler {
  public:
    RPCResult<WiFiNetworkEntry> wifi_query() const {
      return RPCResult<WiFiNetworkEntry>(new WiFiNetworkEntry(expected));
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "wifi_query";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR(string(expected.bssid), resp["result"]["bssid"]);
  TEST_ASSERT_EQUAL_INT(expected.rssi, resp["result"]["rssi"]);
  TEST_ASSERT_EQ_STR_ATTR(expected.ssid, resp["result"]["ssid"]);
}

void test_jsonrpc_wifi_list() {
  static WiFiNetworkEntry expected = {
      .bssid = "AA:BB:CC:DD:EE:FF",
      .rssi = -20,
      .ssid = prandom_string(),
  };
  class : public StubHandler {
  public:
    RPCResult<vector<WiFiNetworkEntry>> wifi_list() const {
      auto res = new vector<WiFiNetworkEntry>();
      res->push_back(expected);
      return RPCResult<vector<WiFiNetworkEntry>>(res);
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "wifi_list";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQUAL_INT(1, resp["result"].size());
  TEST_ASSERT_EQ_STR_ATTR(expected.bssid, resp["result"][0]["bssid"]);
  TEST_ASSERT_EQUAL_INT(expected.rssi, resp["result"][0]["rssi"]);
  TEST_ASSERT_EQ_STR_ATTR(expected.ssid, resp["result"][0]["ssid"]);
}

void test_jsonrpc_wifi_disconnect() {
  class : public StubHandler {
  public:
    RPCResult<Unit> wifi_disconnect() const {
      return RPCResult<Unit>(new Unit());
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "wifi_disconnect";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR("OK", resp["result"]);
}

void test_jsonrpc_wifi_connect() {
  class : public StubHandler {
  public:
    RPCResult<Unit> wifi_connect(const char *ssid, const char *pass) const {
      TEST_ASSERT_EQUAL_STRING("foo", ssid);
      TEST_ASSERT_EQUAL_STRING("bar", pass);
      return RPCResult<Unit>(new Unit());
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "wifi_connect";
  auto params = req.createNestedObject("params");
  params["ssid"] = "foo";
  params["password"] = "bar";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR("OK", resp["result"]);
}

void test_jsonrpc_wifi_mac() {
  static string mac = "DE:AD:BE:EF:CC:AA";
  class : public StubHandler {
  public:
    RPCResult<string> wifi_mac() const {
      return RPCResult<string>(new string(mac));
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "wifi_mac";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR(mac, resp["result"]);
}

void test_jsonrpc_network_local() {
  static LocalNetworkData expected = {
      .ipaddr = "192.168.0.10",
      .gateway = "192.168.0.1",
      .netmask = "255.255.0.0",
      .rssi = -17,
  };
  class : public StubHandler {
  public:
    RPCResult<LocalNetworkData> network_local() const {
      return RPCResult<LocalNetworkData>(new LocalNetworkData(expected));
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "network_local";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR(expected.ipaddr, resp["result"]["ipaddr"]);
  TEST_ASSERT_EQ_STR_ATTR(expected.gateway, resp["result"]["gateway"]);
  TEST_ASSERT_EQ_STR_ATTR(expected.netmask, resp["result"]["netmask"]);
  TEST_ASSERT_EQUAL_INT(expected.rssi, resp["result"]["rssi"]);
}

void test_jsonrpc_configure_influx() {
  class : public StubHandler {
  public:
    RPCResult<Unit> configure_influxdb(const std::string &user,
                                       const std::string &pass,
                                       const std::string &url,
                                       const std::string &db) const {
      TEST_ASSERT_EQUAL_STRING("example-user", user.c_str());
      TEST_ASSERT_EQUAL_STRING("example-pass", pass.c_str());
      TEST_ASSERT_EQUAL_STRING("someurl", url.c_str());
      TEST_ASSERT_EQUAL_STRING("example-database-name", db.c_str());
      return RPCResult<Unit>(new Unit());
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "configure_influxdb";
  auto params = req.createNestedObject("params");
  params["influxUsername"] = "example-user";
  params["influxPass"] = "example-pass";
  params["influxURL"] = "someurl";
  params["influxDatabase"] = "example-database-name";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQ_STR_ATTR("OK", resp["result"]);
}

void test_jsonrpc_network_server() {
  class : public StubHandler {
  public:
    RPCResult<int> network_server(const string &ip) const {
      TEST_ASSERT_EQUAL_STRING("1.1.1.1", ip.c_str());
      return RPCResult<int>(new int(9500));
    };
  } cmd_handler;
  JsonRpcResponseController rc(cmd_handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "network_server";
  auto params = req.createNestedObject("params");
  params["ipv4"] = "1.1.1.1";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["error"].isNull());
  TEST_ASSERT_EQUAL_INT(9500, resp["result"]);
}

void test_jsonrpc_unknown_method() {
  auto handler = StubHandler();
  JsonRpcResponseController rc(handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "non_existent_method";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);
  TEST_ASSERT_TRUE(resp["result"].isNull());
  TEST_ASSERT_EQUAL_INT(-32601, resp["error"]["code"]);
}

void test_jsonrpc_missing_id() {
  auto handler = StubHandler();
  JsonRpcResponseController rc(handler);

  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["method"] = "echo";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_TRUE(resp["id"].isNull());
  TEST_ASSERT_TRUE(resp["result"].isNull());
  TEST_ASSERT_EQUAL_INT(-32600, resp["error"]["code"]);
}

void test_jsonrpc_missing_parameter() {
  auto handler = StubHandler();
  JsonRpcResponseController rc(handler);

  string id = to_string(rand());
  JsonObject req = doc.to<JsonObject>();
  req["jsonrpc"] = "2.0";
  req["id"] = id;
  req["method"] = "echo";

  if (deserializeJson(doc, rc.respond(dump(req))) != DeserializationError::Ok)
    TEST_FAIL_MESSAGE("Could not deserialize response");

  JsonObject resp = doc.as<JsonObject>();
  TEST_ASSERT_EQ_STR_ATTR("2.0", resp["jsonrpc"]);
  TEST_ASSERT_EQ_STR_ATTR(id, resp["id"]);

  TEST_ASSERT_TRUE(resp["result"].isNull());
  TEST_ASSERT_EQUAL_INT(-32602, resp["error"]["code"]);
}

void run_jsonrpc_tests() {
  doc.clear();
  RUN_TEST(test_jsonrpc_echo);
  doc.clear();
  RUN_TEST(test_jsonrpc_reset);
  doc.clear();
  RUN_TEST(test_jsonrpc_network_local);
  doc.clear();
  RUN_TEST(test_jsonrpc_wifi_list);
  doc.clear();
  RUN_TEST(test_jsonrpc_wifi_disconnect);
  doc.clear();
  RUN_TEST(test_jsonrpc_wifi_connect);
  doc.clear();
  RUN_TEST(test_jsonrpc_wifi_mac);
  doc.clear();
  RUN_TEST(test_jsonrpc_network_local);
  doc.clear();
  RUN_TEST(test_jsonrpc_configure_influx);
  doc.clear();
  RUN_TEST(test_jsonrpc_network_server);
  doc.clear();
  RUN_TEST(test_jsonrpc_unknown_method);
  doc.clear();
  RUN_TEST(test_jsonrpc_missing_id);
  doc.clear();
  RUN_TEST(test_jsonrpc_missing_parameter);
  doc.clear();
}
#endif // UNIT_TEST
