#ifdef UNIT_TEST

#ifndef JSONRPC_TESTS_HPP
#define JSONRPC_TESTS_HPP

#include <stdexcept>

#include "Protocols.hpp"
#include "RPCCommands.hpp"

class StubHandler : public GreenzzRpcCmdHandler {
public:
  RPCResult<std::string> echo(const std::string &) const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<Unit> reset() const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<WiFiNetworkEntry> wifi_query() const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<std::vector<WiFiNetworkEntry>> wifi_list() const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<Unit> wifi_disconnect() const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<Unit> wifi_connect(const char *, const char *) const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<std::string> wifi_mac() const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<LocalNetworkData> network_local() const {
    throw std::runtime_error("Unimplemented stub.");
  };
  RPCResult<Unit> configure_influxdb(const std::string &, const std::string &,
                                     const std::string &,
                                     const std::string &) const {
    throw std::runtime_error("Unimplemented stub.");
  }
  RPCResult<int> network_server(const std::string &) const {
    throw std::runtime_error("Unimplemented stub.");
  };
};

void run_jsonrpc_tests();

#endif // JSONRPC_TESTS_HPP
#endif // UNIT_TEST
