#ifdef UNIT_TEST

#include <algorithm>
#include <random>

#include "test-utils.hpp"

#define ALPHABET                                                               \
  "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

std::string prandom_string() {
  std::string str(ALPHABET);
  std::random_device rd;
  std::mt19937 generator(rd());
  std::shuffle(str.begin(), str.end(), generator);
  return str.substr(0, 32);
}
#endif // UNIT_TEST
