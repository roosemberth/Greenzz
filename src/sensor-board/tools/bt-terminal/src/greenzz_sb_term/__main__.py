import argparse
import asyncio
import h11
import logging
import random
import string
import subprocess
import sys

from greenzz_sb_term.connmanager import ConnectionManager

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
logging.getLogger("bleak").setLevel(logging.WARN)

cli_parser = argparse.ArgumentParser()

# Subcommands registration
subparsers = cli_parser.add_subparsers()
subparsers.required = True

echotest_cmd_parser = subparsers.add_parser(
    "echotest",
    help="""
    Search for a Greenzz Sensor Board device, connect to it and send an 'ECHO '
    followed by 32 random printable chars and expect the same 32 chars back.
    The return code is 1 in case of failure, zero otherwise.
    """,
)

proxy_cmd_parser = subparsers.add_parser(
    "proxy",
    help="""
    Search for a Greenzz Sensor Board device, connect to it and proxy JSONRPC
    HTTP requests to the device. The HTTP server can handle a single client.
    """,
)
proxy_cmd_parser.add_argument("--port", default=61987, type=int)


async def echotest_cmd_handler(**_):
    log.info("Establishing connection")
    async with ConnectionManager() as (r, w):
        log.info("Connection established")
        message = "".join(random.sample(string.ascii_letters, 32)) + "\n"
        w.write(message.encode())
        log.info("Message sent.")
        response = await r.readline()
        log.info("Received reply.")

        if response != message.encode():
            p = subprocess.run("xxd", capture_output=True, input=response)
            log.error("Received unexpected reply: ")
            log.error(p.stdout.decode())
            sys.exit(1)


async def proxy_cmd_handler(*_, port: int, **__):
    log.info("Establishing connection")
    loop = asyncio.get_event_loop()

    async def http_client_handler(client_read, client_write):
        log.info("Received client connection.")
        conn = h11.Connection(h11.SERVER)
        ev = None
        while ev is None or ev is h11.NEED_DATA:
            log.info("Read client chunk.")
            reqdata = await client_read.read(4096)
            conn.receive_data(reqdata)
            ev = conn.next_event()

        if not isinstance(ev, h11.Request):
            log.warn("Could not handle h11 evt: " + ev)
            return
        if ev.target != b"/":
            client_write.write(conn.send(h11.Response(status_code=404)))
            return
        if ev.method != b"POST":
            client_write.write(conn.send(h11.Response(status_code=405)))
            return

        bodydata = b""
        ev = conn.next_event()
        while type(ev) != h11.EndOfMessage:
            assert type(ev) is h11.Data
            bodydata += ev.data
            ev = conn.next_event()

        def mk_header(content_length: int):
            headers = [
                ("content-type", "application/json"),
                ("content-length", str(content_length)),
            ]
            return h11.Response(status_code=200, headers=headers)

        async with ConnectionManager() as (bt_read, bt_write):
            bt_write.write(bodydata)
            # Wait for remote to generate a response.
            body = await bt_read.read(20)  # 20 bytes is the min BLE MTU
            # Keep pulling data until source is exhaused.
            try:
                while True:
                    body += await asyncio.wait_for(bt_read.read(512), timeout=0.1)
            except:
                pass

            client_write.write(conn.send(mk_header(len(body))))
            client_write.write(conn.send(h11.Data(data=body)))
            client_write.write(conn.send(h11.EndOfMessage()))

    server = await asyncio.start_server(http_client_handler, "localhost", port)
    log.info(f"Server listening on port {port}")
    try:
        await server.serve_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()


echotest_cmd_parser.set_defaults(cmd_handler=echotest_cmd_handler)
proxy_cmd_parser.set_defaults(cmd_handler=proxy_cmd_handler)

# Called by the setuptools' console_script wrapper
def main():
    cmdline = None if sys.argv[1:] else ["--help"]  # Hacky, but does the job...
    args = cli_parser.parse_args(cmdline)
    asyncio.get_event_loop().run_until_complete(args.cmd_handler(**vars(args)))


if __name__ == "__main__":
    main()
