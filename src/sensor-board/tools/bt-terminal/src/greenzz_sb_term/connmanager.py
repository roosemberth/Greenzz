from asyncio import Queue
from bleak import BleakClient, BleakScanner
from bleak.backends.device import BLEDevice
from threading import Event, Thread
from typing import Optional
import asyncio
import logging
import socket
import threading


class ConnectionManager:
    """Provides a context manager with a connection to the embedded board.

    Scans the address of the first device with name like "Greenzz Sensor Board"
    upon construction.
    Establishes the connection to the device upon entering the context.
    """

    SERVICE_UUID = "6e400001-b5a3-f393-e0a9-e50e24dcca9e"
    CHAR_RX_UUID = "6e400002-b5a3-f393-e0a9-e50e24dcca9e"
    CHAR_TX_UUID = "6e400003-b5a3-f393-e0a9-e50e24dcca9e"

    def __init__(self):
        self.log = logging.getLogger(__name__)
        self.peer_name = None
        self.client = None
        self.loop = None
        self.sockLocal, self.sockRemote = socket.socketpair()

        sidekick_ready = Event()
        self.controller = Thread(target=self._sidekick, args=(sidekick_ready,))
        self.controller.daemon = True
        self.controller.start()
        sidekick_ready.wait()

    def _sidekick(self, notify: Optional[Event] = None):
        self.log.info("Running BLE device controller.")
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.send_queue = Queue()

        if notify is not None:
            notify.set()
        try:
            self.loop.run_forever()
        finally:
            self.loop.close()

    async def _find_and_create_client(self):
        def match_name(device: BLEDevice):
            return "Greenzz Sensor Board" in device.name

        async with BleakScanner() as scanner:
            while self.client is None:
                self.log.info("Searching for device...")
                await asyncio.sleep(0.2)
                for device in await scanner.get_discovered_devices():
                    if match_name(device):
                        self.log.info("Found device " + device.name)
                        kw = dict(disconnected_callback=self._disconnect)
                        self.peer_name = device.name
                        self.client = BleakClient(device.address, **kw)
                        return

    def _disconnect(self, client):
        self.log.info("Client client disconnected: " + self.peer_name)
        self.peer_name = None
        self.client = None

    async def _setup_device_connection(self, notify: Optional[Event] = None):
        await self._find_and_create_client()
        self.log.info("Connecting to " + self.peer_name)

        await self.client.connect()
        self.log.debug(f"Connected to {self.peer_name}.")

        service = (await self.client.get_services())[self.SERVICE_UUID]
        if not service:
            self.log.error(f"{self.peer_name} does not have required service.")
            raise Exception("Probed device lacks service.")
        if self.CHAR_TX_UUID not in [c.uuid for c in service.characteristics]:
            self.log.error(f"{self.peer_name} does not provide TX char.")
            raise Exception("Probed device lacks TX characteristic")
        if self.CHAR_RX_UUID not in [c.uuid for c in service.characteristics]:
            self.log.error(f"{self.peer_name} does not provide RX char.")
            raise Exception("Probed device lacks RX characteristic")

        async def dispatch_packets():
            while True:
                chunk = await self.send_queue.get()
                while not await self.client.is_connected():
                    await asyncio.sleep(0.2)  # Wait for eventual reconnection
                await self.client.write_gatt_char(self.CHAR_RX_UUID, chunk)

        asyncio.run_coroutine_threadsafe(dispatch_packets(), self.loop)

        def enqueue_packet():
            # Hope 1k of data is enough
            data = self.sockRemote.recv(1024)
            self.log.debug(f"Sending packet of {len(data)} bytes.")
            self.send_queue.put_nowait(list(data))

        def handle_rx(_: int, data: bytearray) -> None:
            self.log.debug(f"Received message of {len(data)} bytes.")
            self.sockRemote.send(data)

        await self.client.start_notify(self.CHAR_TX_UUID, handle_rx)
        self.loop.add_reader(self.sockRemote, enqueue_packet)
        self.log.info(f"Channel with {self.peer_name} ready.")
        if notify is not None:
            notify.set()

    async def __aenter__(self):
        connected = Event()
        asyncio.run_coroutine_threadsafe(
            self._setup_device_connection(connected), self.loop
        )
        connected.wait()
        return await asyncio.open_connection(sock=self.sockLocal)

    async def __aexit__(self, exc_type, exc_value, exc_traceback):
        if self.peer_name:
            self.log.info("Disconnecting from " + self.peer_name)

        if self.client:
            client_disconnected = Event()

            async def disconnect():
                await self.client.disconnect()
                client_disconnected.set()

            asyncio.run_coroutine_threadsafe(disconnect(), self.loop)
            client_disconnected.wait()
        self.loop.stop()
