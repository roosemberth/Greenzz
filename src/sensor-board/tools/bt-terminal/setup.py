#!/usr/bin/env python3
import os
import re
from setuptools import find_packages, setup

# This can get annoying really quickly, don't ship any tests ever added.
EXCLUDE_TESTS_PATTERNS = ["*.tests", "*.tests.*", "tests.*", "tests"]

setup(
    name="greenzz_sb_term",
    version="0.0.0",
    author="Roosembert Palacios",
    author_email="roosemberth@posteo.ch",
    maintainer="Roosembert Palacios",
    maintainer_email="roosemberth@posteo.ch",
    description="Test tool to connect to the sensor board",
    install_requires=["bleak", "h11"],
    packages=find_packages(
        "src",
        include=["greenzz_sb_term", "greenzz_sb_term.*"],
        exclude=EXCLUDE_TESTS_PATTERNS,
    ),
    package_dir={"greenzz_sb_term": "src/greenzz_sb_term"},
    entry_points={
        "console_scripts": [
            "greenzz = greenzz_sb_term.__main__:main",
        ]
    },
    license="GPLv3",
    zip_safe=False,
)
