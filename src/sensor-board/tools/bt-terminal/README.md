This project provides a simple terminal for connecting to the sensor board.

# Reference workflow

This project may be installed for convenience into the user environment using
the following command:

```
$ python setup.py install
```

And can be run using the following command:

```
$ greenzz --help
```

During development, you may use the following command to run the application
without installing it.
Note this command is not guaranteed to work nor maintained and is provided here
for mere convenience:

```
PYTHONPATH=src python3 -m greenzz_sb_term
```

Please run with `--help` to see the tool documentation.

# License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
