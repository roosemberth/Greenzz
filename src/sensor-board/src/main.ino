#include <Arduino.h>
#include <WiFi.h>

#include "BLE.hpp"
#include "GreenzzRPCCommands.hpp"
#include "Protocols.hpp"
#include "Tasks.hpp"

auto rpcHandler = GreenzzRpcCmdHandlerImpl();
auto btHandler = JsonRpcResponseController(rpcHandler);
GreenzzBLEManager *btmgr;

void doDeadBlinkPattern() {
  Serial.print("Entering Dead blink pattern mode.");
  pinMode(2, OUTPUT);
  while (1) {
    for (int i = 0; i < 3; ++i) {
      digitalWrite(2, HIGH);
      delay(150);
      digitalWrite(2, LOW);
      delay(150);
    }
    delay(1000);
  }
}

void setup() {
  Serial.begin(115200);
  Serial.println("Hello world");

  btmgr = new GreenzzBLEManager(btHandler);
  btmgr->begin();

  WiFi.begin();
  GreenzzTaskControl::getInstance().startInfluxWork();
}

void loop() {
  // Nothing to do. Sleeping allows the RT scheduler to do something else.
  vTaskDelay(1000);
}
