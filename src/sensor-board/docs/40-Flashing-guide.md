# Flashing the sensor board

The following commands assume you have downloaded or compiled yourself the
latest version of the sensor board firmware.
You can find download links for the latest release [here][sb-download].

[sb-download]: https://gitlab.com/roosemberth/Greenzz#sensor-board

## Update only the application

This command updates only the sensor firmware.

```console
$ esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 \
           --before default_reset --after hard_reset write_flash -z \
           --flash_mode dio --flash_freq 40m --flash_size detect \
           0x10000 nodemcu-32s/greenzz.firmware.bin
```

## Flash all firmware components

This command performs a full device firmware flash, useful when an upgrade went
wrong of the flash was otherwise corrupted.
Note that this may take much longer than updating just the application.

```console
$ esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 \
           --before default_reset --after hard_reset write_flash -z \
           --flash_mode dio --flash_freq 40m --flash_size detect \
           0x1000 nodemcu-32s/bootloader_dio_40m.bin \
           0x8000 nodemcu-32s/partitions.bin \
           0xe000 nodemcu-32s/boot_app0.bin \
           0x10000 nodemcu-32s/greenzz.firmware.bin
```
