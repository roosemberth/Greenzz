# Greenzz Sensor Board

Welcome to the Sensor board knowledge base.

## I am a user of Sensor board

- [Getting started guide](./20-Getting-started_nodemcu-32s-B.md).

## I am a software or hardware developer

- [Hardware description](./10-Hardware-description.md).
- [Technical description](./30-Technical-description.md).
- [How to flash my sensor board](./40-Flashing-guide.md).
