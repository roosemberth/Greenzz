# Technical description

## Architectural walkthrough

The _sensor board_ (hereafter “sb”) may interact with the following components:

- The _Greenzz Android application_ over Bluetooth LE (BLE).
- The WiFi access point providing internet connectivity.
- The InfluxDB database over HTTP.

During development, the [bt-terminal](../tools/bt-terminal/) may be used as a
BLE<->HTTP proxy of jsonrpc requests.
See the corresponding project for details.

The different interactions among these components are described in the
[hardware description](./10-Hardware-description.md) document in this same
repository.

## Code organisation

The parent directory contains a [PlatformIO][pio] project.
See the corresponding README for practical details on working with the code.
The reader is advised to read about PlatformIO standard practices and project
structure.

[platformio]: https://platformio.org/

The main components of the project are:

- `lib/greenzz-core`: Platform-independent code; testable on all platforms.

- `lib/greenzz-esp32`: Platform-dependent implementations, this code is only
  testable on supported esp32 boards.

- `lib/native-libs-for-tests`: Platform-dependent stubs used for testing
  platform-independent code on the native platform.

- `lib/nomach-tests`: Platform-independent tests.

- `test/native`: Tests initialization code for the native platform.

- `test/embedded`: Tests initialization code for the embedded platforms and
  esp32 platform-exclusive tests.

- `test/all-machs-tests.h`: Test harness for loading platform-independent tests
  on tested platforms (included from each test initialization code).

- `src/main.ino`: Arduino sketch (entry point).

## JSON-RPC over BTLE

The Sensor board may be reached over Bluetooth LE (BLE), upon connecting to the
board, a _Nordic UART Service_ with UUID _6e400001-b5a3-f393-e0a9-e50e24dcca9e_
provides a JSON-RPC interface to interact with the device.

The JSON-RPC interface is described in [the openrpc document](./openrpc.yaml)
(see also [the openrpc specification][openrpc]).
An interactive version of this specification may be accessed from the [openrpc
playground][playground].

[openrpc]: https://spec.open-rpc.org/
[playground]: https://playground.open-rpc.org/?url=https://gitlab.com/roosemberth/Greenzz/-/raw/master/src/sensor-board/docs/openrpc.yaml

Mind that as of writing, some RPC are not completely implemented.

### Known issues

- The BTLE stack sometimes disconnects the _test_ bt-terminal client for no
  apparent reason. The following message is displayed:

  ```console
  Connected to BLE peer.
  lld_pdu_get_tx_flush_nb HCI packet count mismatch (0, 1)
  Disconnected from BLE peer.
  ```

  The cause is still unknown, note this only happens with the _test client_, so
  it may be an implementation issue in the _bleak_ library.
  Restarting the proxy fixes the issue.
