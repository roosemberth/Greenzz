# Hardware description

The _Sensor board_ (sb) is a hardware component designed to periodically report
measurements about the state of the plant.

## Technology walkthrough

The _Sensor board_ (sb) beacons a [bluart][bluart] service over _Bluetooth low
energy_ (BLE).
The _Greeenzz Android application_ (hereafter “The app”) may then connect to
the _sb_ through BLE, and setup a [json-rpc][json-rpc]-based bidirectional
communication protocol.

[bluart]: https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/samples/bluetooth/central_uart/README.html
[json-rpc]: https://www.jsonrpc.org/specification

The app then proceeds to request the sb WiFi adapter mac address and queries a
list of visible WiFi networks to the sb.
The user is then asked to authenticate the sb to one of these WiFi networks (the
user can refresh the list of networks at any time).

When the sensor board has established network connectivity, the app will
register the _sb_ to a plant on the _Greenzz server_.
The _Greenzz server_ allocates a new time-series bucket and account in an
InfluxDB database and sends back the database URL, bucket name, username and
password to be used by the sensor board to write data to the specified table.
The app relays this information to the sb over the jsonrpc link and the sensor
board attemps to connect (ie. ping) the database using the provided data.

If the connection to the InfluxDB database by the sb succeeds.
The _Greenzz Android application_ disconnects the BLE link, requests to the
_Greenzz server_ an account to read the data written by the sensor board and
starts streaming live data every 5 seconds.
Upon disconnection, the sensor board disables its BLE beaconing and reports
measurements every 5 seconds.

If the connection to the InfluxDB database by the sb fails, an error is reported
to the user and the sb aborts the jsonrpc connection and restarts BLE beaconing.
Depending on the details of the problem, the user may be advised to try again
or to contact the sensor board vendor if the error is deemed irrecoverable.

Upon reset, the sensor board verifies its internal filesystem (SPIFFS)
for both the WiFi and InfluxDB configuration files.
If these are found reporting continues as normal.
If the WiFi connectivity is lost, the Sensor board will periodically try to
reconnect to the WiFi network on its configuration file.

The sensor board may be reconfigured at any moment.
Ideally, we would require the user to press a button, but currently low-airtime
beaconing is permanently active (the user may need to scan for available BLE
devices twice after the sensor board has been configured once).
Upon connection, the procedure is exactly the same as if connected for the first
time.

## Security considerations

### The sensor board transfers data over HTTP to the InfluxDB database

A potential attacker with the capacity of intercepting traffic over the network
may be able to steal the sensor board credentials and inject deceitful
information without any participants noticing; as a workaround we only allow
connecting the sensor board to password-protected networks and our TCB assumes
it is acceptable for any user in the same local network to impersonate the
sensor board.
It is not possible to retrieve reported measurements using the sensor board
account.

While the InfluxDB database bucket name is a function of the sensor board WiFi
adapter mac address, the generated password is random and transfered over an
encrypted BLE link.
An attacker in possesion of a batch of sensor boards cannot register them to
secure access at a later time, since the credentials distributed will no longer
be valid when the sensor board is reconfigured.

A possible remediation is to use HTTPS instead of HTTP.
The main downside is the increased space footprint of shipping a CA chain or
custom root CA; along with the increased latency and energy usage of running
asymmetric cryptography on a platform with no cryptographic accelerator.

Another possible remediation would be to protect the HTTP connection through an
encrypted link (e.g. VPN using a registration-time generated keypair).
While this may be more space-efficient, it has the additional complexity of
generating asymmetric on the fly, testing such code and similar latency and
energetic costs as the ones previously described.

### The sensor board firmware is publicly available

A potential attacker in possession of a batch of sensor boards may download,
modify and compile a modified version of the firmware.

The scope of any damage to user devices is restricted by the json-rpc protocol
and the BLE specification.
The _Greenzz Android application_ will abort any abnormal conversations
notifying the user the sensor board may require a firmware update.
Abnormally large payloads or stalling the BLE link will trigger a link reset
in the BLE stack, notifying the user with a similar error message.

The scope of any damage to the InfluxDB database is restricted by the security
clearance of the sensor board.
The security clearance of the sensor board only allows writing data to one
time-series bucket; the max payload size is limited server-side.
High reporting rates will be limited by the database reverse proxy.
Any other attacks are not any different than attacks performed by any external
entity on the open internet and their scope and countermeasures are outside this
document.

A possible remediation would be to sign firmware and flash manufacturer keys
on the hardware.
Note that this is not preferable since it goes against the principle of open
source software and is incompatible with the license of this code base.

A better solution would be to package the sensor board in a DIY kit where end
users get to either flash a provided firmware (for example, the one generated
by the continuous integration on the project forge) or to compile and flash
the firmware themselves.

## Board revision history

### Inkplate6

First development board used to develop the JSON-RPC over BTLE protocol while
the set of sensors to integrate was still being decided and a more adequate
batch of prototyping boards and sensors were ordered.

### nodemcu-32s (rev A)

Quick and dirty prototype based on [nodemcu-32s][nodemcu-32s] development
boards.
These prototype boards were soon reworked into rev B by scrapping them down and
reusing their components.

[nodemcu-32s]: https://wiki.geekworm.com/NodeMCU-32S

![nodemcu-32s rev A prototype](./img/10-nodemcu-32s-A-1.jpg)

### nodemcu-32s (rev B)

Rework of the previous prototype with mechanical stability, debugging port
placement and sturdiness and versatility in mind.
Do note that the brown signal wire in the images was later relocated due to
software limitations.

![nodemcu-32s rev B prototype. The humidity and temperature sensor can be
seen.](./img/10-nodemcu-32s-B-1.jpg)

![nodemcu-32s rev B prototype. The board is placed in a plant pot on its
intended position. The light sensor is visible.](./img/10-nodemcu-32s-B-2.jpg)

![Top view of two nodemcu-32s rev B prototypes.](./img/10-nodemcu-32s-B-3.jpg)
