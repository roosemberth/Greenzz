# Getting started with your sensor board

This guide applies to the sensor board based on the nodemcu-32s rev B.

## Before you begin

Please inspect the sensor board for physical damage that may have occurred
during transport.
In particular, check all the wires are connected, none of them are ripped off
and the wires soldered to pin sockets are connected to the right terminals.
Compare your sensor board wiring to the image here bellow.
Mind that the cable colors may not exactly match; this is not an issue, but
merely a manufacturing artifact.

![Sensor board wiring](./img/20-wiring.jpg)

For reference, the pin sockets and the pins they are connected to are given
in the following table.
The side assumes the sensor board is oriented with the micro-usb-b port north
(on top), no wires visible and the screws threads oriented towards the user.

| Color (reference)  | Pin name (side)    | Another pin it connects to   |
|--------------------|--------------------|------------------------------|
| Brown (Signal)     | D33 (Right)        | SIG in the humidity sensor   |
| Yellow (SCL)       | D22 (Left)         | SCL on temperature sensor    |
| Blue (SDA)         | D21 (Left)         | SDA on temperature sensor    |
| Black (Ground)     | GND (Left)         | GND on temperature sensor    |
| Red (Power)        | 3V3 (Left)         | VDD on temperature sensor    |

## How to use your sensor board

Using the sensor board is as easy as providing power through a USB port and
inserting it alongside the plant you would like to monitor gently and as deep
as possible in the soil.

You may now go to the [Greenzz Android application][app], create a profile for
your plant if you have not done so already, tap on the menu on the top right
(three dots) and select _Add sensor board_.

[app]: ../../client/

Follow the instructions on screen and your sensor board should be streaming data
in no time.
Mind that the sensor board requires continuous WiFi connectivity to operate.

If your sensor board gets disconnected or out of WiFi range, you can simply plug
it back and it will resume normal operations.

## How to take good care of your sensor board

### Watering your plant

While it is possible to directly water your plants with the sensor installed,
we recommend removing it prior watering to avoid sprinkling water on top of the
delicate electronics.
After watering your plant, the sensor board may be directly inserted back at
it previous position.

### Maintenance of the sensor board

The sensor board requires little to no maintenance on normal working conditions.
It is recommended to clean the _soil humidity probe leads_ every other week with
a moist sponge to remove any traces of limestone, chalk or other residues that
may interfere with the probe's measurements.
While it is not necessary to unplug the sensor board for cleaning, we recommend
doing so to avoid sprinkling water into the delicate electronics.
