# Greenzz sensor board

This directory contains the firmware project files of the Greenzz sensor board
(**SB**).

If you're looking for an introduction or user documentation, make sure to
check [the `docs`](./docs/) folder.

The project is built using [PlatformIO][platformio].
Please refer to the platformio documentation for more details.
A reference workflow is provided in this document for convenience.
The reference workflow assumes you have platformio installed in your system.

[platformio]: https://platformio.org/

## Reference workflow

Make sure the necessary libs are installed by running `pio lib install`.
This step is not necessary, but will allow you to catch unavailable or
otherwise incompatible libraries with your system or platform.

The application may be built, flashed and executed in the device using the
following command:

```console
platformio run -j8 --target upload --target monitor
```

Mind that the target platform must be connected and your user should have the
necessary permissions to interact with the device. Please consult the
[installation FAQ][install-faq] if you're having any issues.

[install-faq]: https://docs.platformio.org/en/latest/faq.html#installation

### Running the unit tests

The unit tests may be run in one or more platformio environments.
Running in the host machine is provided as a development convenience for
test-driven development since it's usually much faster to run tests in the host
architecture than the embedded device.
Note however that tests MUST be run in the target device when possible for final
validation.

Machine (platform)-independent tests should be placed in the `all-machs-tests.h`
file so they may be sourced from all environments.

#### Running tests in the host

Requires a host compiler (e.g. `g++`).

```console
platformio test -e native
```

Native tests will be run by the CI.

#### Running tests on the target

Note this command runs the tests on all default environments, which at the
moment of writing happen to be only the target platform.

```console
platformio test
```

## Implementation comments

The SB implements the [bluart][bluart] services and characteristics over BTLE.
We currently know nothing about the legality of reusing these UUIDs, but it
seems to be common practice among amateur projects (including the Arduino
examples).
As such, we provide a simple terminal for debug and testing of the platform
under `tools/bt-terminal`.
This tool is supposed to be cross-platform, but has only been tested on
GNU/Linux 5.11.2 with BlueZ 5.55.
Mind that since we're using a somewhat standard transport ([bluart][bluart]),
there are many applications out there which should be able to talk to the sensor
board.
In particular, the android application [nRF UART 2.0][bluart-app] has been
confirmed working.

[bluart]: https://developer.nordicsemi.com/nRF_Connect_SDK/doc/latest/nrf/samples/bluetooth/central_uart/README.html
[bluart-app]: https://play.google.com/store/apps/details?id=com.nordicsemi.nrfUARTv2

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
