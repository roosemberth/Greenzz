#ifdef UNIT_TEST

#include "../all-machs-tests.h"
#include <unity.h>

int main(int argc, char **argv) {
  UNITY_BEGIN();

  run_machine_independent_tests();

  UNITY_END();
  return 0;
}

#endif // UNIT_TEST
