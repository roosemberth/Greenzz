#if defined(ARDUINO) && defined(UNIT_TEST)

#include <Arduino.h>
#include <unity.h>

#include "BLE.hpp"
#include "BoardID.hpp"

void test_board_id_not_empty() {
  const char *addr = getBoardAddress();
  TEST_ASSERT_TRUE(strlen(addr) > 0);
}

void test_btmgr_service_init_deinit() {
  GreenzzBLEManager *btmgr = new GreenzzBLEManager();

  // Test the name of the device
  int idx = btmgr->name.rfind("Greenzz Sensor Board ", 0);
  TEST_ASSERT_EQUAL_INT(0, idx);

  btmgr->begin();
  delay(500); // Give the BTLE stack some time to settle
  btmgr->stop();

  delete btmgr;
}

/**
 * Runs all platform-depent tests.
 * Must be called after UNITY has been initialized.
 */
void run_platform_tests() {
  RUN_TEST(test_board_id_not_empty);
  RUN_TEST(test_btmgr_service_init_deinit);
}

#endif
