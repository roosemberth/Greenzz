#if defined(ARDUINO) && defined(UNIT_TEST)

#include <Arduino.h>
#include <unity.h>

#include "../all-machs-tests.h"
#include "platform.hpp"

void setup() {
  UNITY_BEGIN();

  run_machine_independent_tests();
  run_platform_tests();

  UNITY_END();
}

void loop() {
  // nothing to be done here.
}

#endif
