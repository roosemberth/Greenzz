/**
 * This file contains tests which are valid for all platforms.
 */
#ifndef ALL_MACHS_TESTS_H
#define ALL_MACHS_TESTS_H

#include <unity.h>

#include "Protocols.hpp"
#include "jsonrpc-tests.hpp"
#include "test-utils.hpp"

void test_echo_response_controller() {
  auto msg = prandom_string();
  ResponseController &rc(controller_echo_back);
  auto recv = rc.respond(msg);
  TEST_ASSERT_TRUE(msg == recv);
}

void test_double_inversion() { TEST_ASSERT_EQUAL(0, ~(~0)); }

void run_machine_independent_tests() {
  RUN_TEST(test_double_inversion);
  RUN_TEST(test_echo_response_controller);
  run_jsonrpc_tests();
}

#endif // ALL_MACHS_TESTS_H
